//
//  FeedbackViewController.swift
//  Temp
//
//  Created by Mike on 08/02/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController, UITextViewDelegate {
    
    // ================ IB OUTLETS =================
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var button: UIButton!
    
    // ================ IB ACTIONS =================
    
    @IBAction func buttonPressed(sender: AnyObject) {
        connectionManager.sendFeedback(textView.text, completion: {
            (success, error) in
            
            let title = success ? " Feedback Submitted!" : "Sending Failed!"
            
            UIAlertView(title: title, message: nil, delegate: nil, cancelButtonTitle: "OK").show()
            
            if success {
                self.textView.text = ""
                self.performSegueWithIdentifier("return", sender: nil)
            }
        })
    }
    
    @IBAction func scrollViewTapped(sender: AnyObject) {
        self.view.endEditing(true)
    }
    // ================ VIEW CONTROLLER =================

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.registerKeyboardNotifications()
        self.textView.roundToRadius(10)
        self.textView.clipsToBounds = true
    }
    
    override func viewDidAppear(animated: Bool) {
        self.textView.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // ================ KEYBOARD NOTIFICATIONS =================
    
    override func keyboardWasShown (notification: NSNotification) {
        let info = notification.userInfo!
        let key = String(UIKeyboardFrameBeginUserInfoKey)
        
        if let keyboardHeight = info[key]?.CGRectValue.height {
            let insets: UIEdgeInsets = UIEdgeInsetsMake(self.scrollView.contentInset.top, 0, keyboardHeight, 0)

            self.scrollView.contentInset = insets
            self.scrollView.scrollIndicatorInsets = insets
        }
    }
    
    override func keyboardWillBeHidden (notification: NSNotification) {
        self.scrollView.contentInset = UIEdgeInsetsZero
        self.scrollView.scrollIndicatorInsets = UIEdgeInsetsZero
    }

    // ================ TEXT VIEW DELEGATE =================
    


    // ================ NAVIGATION =================
    /*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
