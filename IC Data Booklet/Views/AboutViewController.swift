//
//  AboutViewController.swift
//  Test
//
//  Created by Mike on 18/12/2014.
//  Copyright (c) 2014 Mike. All rights reserved.
//

import UIKit
import CoreData
import Foundation

class AboutViewController: UIViewController {
    
    var graphController: GraphViewController!
    var i = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "About"
        self.registerKeyboardNotifications()
        // Do any additional setup after loading the view.
        
        /*if let graphs = databaseManager.getAllEntitiesOfType(kDataGraph, withPredicate: nil) as? [Graph] where graphs.count > 1 {
            
            let graph = databaseManager.getEntity(kDataGraph, withID: 9999, createIfMissing: true) as! Graph
            let xAxis = databaseManager.getEntity(kDataGraphAxis, withID: 9997, createIfMissing: true) as! GraphAxis
            let yAxis = databaseManager.getEntity(kDataGraphAxis, withID: 9998, createIfMissing: true) as! GraphAxis
            let secondYAxis = databaseManager.getEntity(kDataGraphAxis, withID: 9999, createIfMissing: true) as! GraphAxis
            
            let units = databaseManager.getAllEntitiesOfType(kDataUnit, withPredicate: nil) as! [Unit]
            
            xAxis.title = "X Axis"
            xAxis.min = 0
            xAxis.max = 100
            xAxis.majorTick = 10
            xAxis.minorTick = 5
            xAxis.logarithmic = false
            xAxis.type = AxisType.X.rawValue
            xAxis.unit = units[0]
            
            yAxis.title = "Y Axis"
            yAxis.min = 1
            yAxis.max = 1000
            yAxis.majorTick = 10
            yAxis.minorTick = 5
            yAxis.logarithmic = true
            yAxis.type = AxisType.Y.rawValue
            yAxis.unit = units[1]
            
            secondYAxis.title = "Secondary Y Axis"
            secondYAxis.min = 0
            secondYAxis.max = 100
            secondYAxis.majorTick = 2
            secondYAxis.minorTick = 1
            secondYAxis.logarithmic = false
            secondYAxis.type = AxisType.SecondaryY.rawValue
            secondYAxis.unit = units[2]
            
            graph.name = "Graph Test"
            graph.isTable = false
            graph.image = graphs[2].image
            graph.axes = [xAxis,yAxis,secondYAxis]
            
            databaseManager.saveContext()*/
        if let graph = databaseManager.getEntity(kDataGraph, withID: 10, createIfMissing: false) as? Graph {
            let axis = databaseManager.getEntity(kDataGraphAxis, withID: 23, createIfMissing: false) as! GraphAxis
            
            let unit = databaseManager.getEntity(kDataUnit, withID: 435, createIfMissing: false) as! Unit
            axis.unit = unit
            
            databaseManager.saveContext()
            self.graphController = GraphViewController(graph: graph)
            self.view.addSubview(graphController.view)
            self.addChildViewController(graphController)
            graphController.view.frame = self.view.bounds
            graphController.view.backgroundColor = UIColor.lightGrayColor()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //containerFadeLayer.frame = scrollViewContainer.bounds
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
