//
//  UnitPickerView.swift
//  Temp
//
//  Created by Mike on 31/03/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit
import CoreData

protocol UnitPickerViewDelegate {
    func unitPicker(unitPicker: UnitPickerView, didSelectUnit unit: Unit)
}

@objc
protocol UnitPickerViewDataSource {
    func unitPicker(unitCategoryForPicker unitPicker: UnitPickerView) -> UnitCategory!
    optional func unitPicker(inputUnitForPicker unitPicker: UnitPickerView) -> Unit?
    optional func unitPicker(inputValueForPicker unitPicker: UnitPickerView) -> Double
}

class UnitPickerView: UIPickerView, UIPickerViewDataSource, UIPickerViewDelegate, NSFetchedResultsControllerDelegate {
    
    // ================= INITIALIZERS =================

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.delegate = self
        self.dataSource = self
    }
    
    // ================= VARIABLES =================
    
    var category: UnitCategory! 
    var inputUnit: Unit?
    var inputValue: Double = 0
    
    var compoundEnabled = true

    var unitDelegate: UnitPickerViewDelegate?
    var unitDataSource: UnitPickerViewDataSource?
    
    var allNormalUnits: [Unit] = []
    var allCompoundUnits: [Unit] = []
    var fetchedResultsController: NSFetchedResultsController!
    
    private var showsHeaders = false
    private var compoundsHeaderRow = 0
    private var separatorRow = 0
    private var normalHeaderRow = 0
    private var compoundStartRow = 0
    private var normalStartRow = 0
    
    // ================= METHODS =================
    
    func configureResults() {
        print(fetchedResultsController.sections?.count)
        /*if let units = fetchedResultsController.fetchedObjects as? [Unit] {
        self.allNormalUnits = units.filter({ $0.isCompound == false })
        self.allCompoundUnits = units.filter({ $0.isCompound == true })
        } else {
        self.allNormalUnits = []
        self.allCompoundUnits = []
        }*/
        print((fetchedResultsController.sections?[¿0]?.name,fetchedResultsController.sections?[¿1]?.name))
        self.allNormalUnits = fetchedResultsController.sections?[¿1]?.objects as? [Unit] ?? []
        self.allCompoundUnits = fetchedResultsController.sections?[¿0]?.objects as? [Unit] ?? []
        
    }
    
    func configureController() {
        
        // configure units
        let categoryPK = category?.pk ?? -1
        let categoryPred = NSPredicate(format: "category.pk = \(categoryPK)", argumentArray: nil)
        var predicates: [NSPredicate] = [categoryPred]
        if !compoundEnabled {
            let compoundPred = NSPredicate(format: "isCompound = false", argumentArray: nil)
            predicates += [compoundPred]
        }
        let pred = NSCompoundPredicate(type: .AndPredicateType, subpredicates: predicates)
        let nameSort = NSSortDescriptor(key: "name", ascending: true)
        
        self.fetchedResultsController =  databaseManager.getFetchedResultsController(kDataUnit, withPredicate: pred, withSortDescriptors: [nameSort], sectionNameKeyPath: "isCompound", delegate: self)
    }
    
    override func reloadAllComponents() {
        print("RELOAD")
        
        self.category = unitDataSource?.unitPicker(unitCategoryForPicker: self)
        self.inputUnit = unitDataSource?.unitPicker?(inputUnitForPicker: self)
        self.inputValue = unitDataSource?.unitPicker?(inputValueForPicker: self) ?? 0
        
        if category != nil {
            self.allNormalUnits = category.getUnits(false)
            let nameSort = NSSortDescriptor(key: "name", ascending: true)
            let pred = NSPredicate(format: "isCompound = true", argumentArray: nil)
            self.allCompoundUnits = category.getUnits(sortedBy: [nameSort], withPredicate: pred)
        } else {
            allNormalUnits = []
            allCompoundUnits = []
        }
        
        // save header/unit rows for convenience
        if allNormalUnits.count == 0 || allCompoundUnits.count == 0 {
            self.showsHeaders = false
            compoundsHeaderRow = 0
            separatorRow = 0
            normalHeaderRow = 0
            compoundStartRow = 0
            normalStartRow = allCompoundUnits.count
        } else {
            self.showsHeaders = true
            compoundsHeaderRow = 0
            separatorRow = allCompoundUnits.count + 1
            normalHeaderRow = separatorRow + 1
            compoundStartRow = compoundsHeaderRow + 1
            normalStartRow = normalHeaderRow + 1
        }
        
        // reload pickerView
        super.reloadAllComponents()
    }
    
    func selectUnit(unit: Unit!) {
        if unit != nil {
            if let index = Unit.findIndex(allNormalUnits, unit: unit) {
                self.selectRow(index + normalStartRow, inComponent: 0, animated: true)
                return
            } else if let index = Unit.findIndex(allCompoundUnits, unit: unit) {
                self.selectRow(index + compoundStartRow, inComponent: 0, animated: true)
                return
            } else if let defaultUnit = unit.category.getBaseUnit() {
                if let index = Unit.findIndex(allNormalUnits, unit: defaultUnit) {
                    self.selectRow(index + normalStartRow, inComponent: 0, animated: true)
                    return
                }
            }
        }
        // else
        print("CANT SELECT UNIT")
        let firstRow = normalStartRow
        self.selectRow(firstRow, inComponent: 0, animated: true)
    }

    func currentUnit() -> Unit? {
        return getUnitForRow(self.selectedRowInComponent(0))
    }

    func getUnitForRow(row: Int) -> Unit? {
        if row < normalStartRow {
            return allCompoundUnits[¿row - compoundStartRow]
        } else {
            return allNormalUnits[¿row - normalStartRow]
        }
    }
    
    // ================= PICKER VIEW DATA SOURCE =================
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let n = allNormalUnits.count + allCompoundUnits.count
        if showsHeaders {
            // will use 2 section titles and 1 blank space row
            return n + 3
        } else {
            return n
        }
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        var tView:UILabel
        if view == nil { tView = UILabel() } else { tView = view as! UILabel }
        
        tView.minimumScaleFactor = 0.5
        tView.textAlignment = NSTextAlignment.Center
        
        // configure for section headers as necessary
        if showsHeaders {
            if row == compoundsHeaderRow || row == normalHeaderRow {
                // shrink header fontsize and maintain the gray pickerview color to simulate unselected cells
                tView.textColor = UIColor.grayColor()
                tView.font = UIFont(name: tView.font.fontName, size: tView.font.pointSize*0.8)
            }
            
            if row == compoundsHeaderRow {
                let text = NSMutableAttributedString(string: "Created Units", font: tView.font)
                text.underline(NSUnderlineStyle.StyleSingle)
                tView.attributedText = text
                return tView
            } else if row == separatorRow {
                tView.text = ""
                return tView
            } else if row == normalHeaderRow {
                let text = NSMutableAttributedString(string: "Default Units", font: tView.font)
                text.underline(NSUnderlineStyle.StyleSingle)
                tView.attributedText = text
                return tView
            }
        }
        
        // else configure for unit
        tView.textColor = UIColor.blackColor()
        
        if let unitForRow = getUnitForRow(row) {
            // get value string as necessary
            var plural = true
            var valueString: NSMutableAttributedString
            if inputUnit == nil {
                valueString = NSMutableAttributedString()
            } else {
                let val = inputUnit!.convertValue(inputValue, toUnit: unitForRow).roundTo(significantFigures: 5)
                valueString = val.attributedStringWithExponentBase10(significantFigures: 5, forFont: tView.font) + " "
                plural = val != 1
            }
            // append unit display attributed string to the value string
            tView.attributedText = valueString + unitForRow.getDisplayString(forFont: tView.font, appendFullName: true, plural: plural)
        } else {
            // should never be called unless there is a development error
            tView.text = "???"
        }
        
        return tView
    }
    
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 25
    }
    
    // ================= PICKER VIEW DELEGATE =================
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if allNormalUnits.count + allCompoundUnits.count > 0 {
            if let unit = getUnitForRow(row) {
                // else, send the selected unit to the delegate
                self.unitDelegate?.unitPicker(self, didSelectUnit: unit)
            } else if showsHeaders {
                // if it is a header row, select the nearest unit row
                if row == compoundsHeaderRow || row == normalHeaderRow {
                    let newRow = row + 1
                    self.selectRow(newRow, inComponent: 0, animated: true)
                    if let unit = getUnitForRow(newRow) {
                        self.unitDelegate?.unitPicker(self, didSelectUnit: unit)
                    }
                } else if row == separatorRow {
                    let newRow = row - 1
                    self.selectRow(newRow, inComponent: 0, animated: true)
                    if let unit = getUnitForRow(newRow) {
                        self.unitDelegate?.unitPicker(self, didSelectUnit: unit)
                    }
                }
            }
        }
    }
    
    // ================= NS FETCHED RESULTS CONTROLLER DELEGATE =================

    /*func controllerDidChangeContent(controller: NSFetchedResultsController) {
        print("CONTROLLER DID CHANGE")
    }

    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        print(((anObject as! Unit).name,type.rawValue,indexPath?.section,indexPath?.row))
    }*/
    
}













/*
// Attempted to set up the view for use with a fetchedresultscontroller but ran into issues with the fetch request not loading any data. Temporarily reverted.

// section access keys
private let kNormalUnitSection = OptionalIndex(i: 1)
private let kCompoundUnitSection = OptionalIndex(i: 0)

class UnitPickerView: UIPickerView, UIPickerViewDataSource, UIPickerViewDelegate, NSFetchedResultsControllerDelegate {
    
    // ================= INITIALIZERS =================
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.delegate = self
        self.dataSource = self
    }
    
    // ================= VARIABLES =================
    
    var category: UnitCategory! {
        didSet {
            if self._fetchedResultsController != nil {
                // update FRC predicate if it was already made, otherwise do nothing as it will be made on startup
                fetchedResultsController.setNewPredicate(predicateForFetchedResultsController())
            }
        }
    }
    var inputUnit: Unit?
    var inputValue: Double = 0
    
    var compoundEnabled = true
    
    var unitDelegate: UnitPickerViewDelegate?
    var unitDataSource: UnitPickerViewDataSource?
    
    private var _fetchedResultsController: NSFetchedResultsController!
    var fetchedResultsController: NSFetchedResultsController {
        if _fetchedResultsController == nil {
            // set up new FRC
            let pred = predicateForFetchedResultsController()
            let nameSort = NSSortDescriptor(key: "name", ascending: true)
            
            self._fetchedResultsController = databaseManager.getFetchedResultsController(kDataUnit, withPredicate: pred, withSortDescriptors: [nameSort], sectionNameKeyPath: "isCompound", delegate: self)
            
            println("created fetched results controller")
            println(self._fetchedResultsController.fetchedObjects?.count)
            let test = self._fetchedResultsController.sections
            println(test)
            println(test?[kNormalUnitSection])
            println(test?[kNormalUnitSection]?.numberOfObjects)
        }
        return _fetchedResultsController
    }
    
    var numberOfNormalUnits: Int {
        return fetchedResultsController.sections?[kNormalUnitSection]?.numberOfObjects ?? 0
    }
    var numberOfCompoundUnits: Int {
        return fetchedResultsController.sections?[kCompoundUnitSection]?.numberOfObjects ?? 0
    }
    var allNormalUnits: [Unit] {
        return fetchedResultsController.sections?[kNormalUnitSection]?.allObjects as? [Unit] ?? []
    }
    var allCompoundUnits: [Unit] {
        return fetchedResultsController.sections?[kCompoundUnitSection]?.allObjects as? [Unit] ?? []
    }
    
    private var showsHeaders = true
    private var compoundsHeaderRow = 0
    private var separatorRow = 0
    private var normalHeaderRow = 0
    private var compoundStartRow = 0
    private var normalStartRow = 0
    
    // ================= METHODS =================
    
    override func awakeFromNib() {
        loadData()
    }
    
    func predicateForFetchedResultsController() -> NSPredicate {
        // return a predicate to retrieve only units of the same category that are not in the process of being created.
        let categoryPK = category?.pk ?? -1
        let categoryPred = NSPredicate(format: "category.pk = \(categoryPK)", argumentArray: nil)
        let creationPred = NSPredicate(format: "isBeingCreated = false", argumentArray: nil)
        let pred = NSCompoundPredicate(type: .AndPredicateType, subpredicates: [categoryPred,creationPred])
        return pred
    }
    
    private func loadData() {
        // reload input values
        self.category = unitDataSource?.unitPicker(unitCategoryForPicker: self)
        self.inputUnit = unitDataSource?.unitPicker?(inputUnitForPicker: self)
        self.inputValue = unitDataSource?.unitPicker?(inputValueForPicker: self) ?? 0
        
        // update and save header/unit rows for convenience
        if numberOfNormalUnits == 0 || numberOfCompoundUnits == 0 {
            self.showsHeaders = false
            compoundsHeaderRow = -1
            separatorRow = -1
            normalHeaderRow = -1
            compoundStartRow = -1
            normalStartRow = numberOfCompoundUnits
        } else {
            self.showsHeaders = true
            compoundsHeaderRow = 0
            separatorRow = allCompoundUnits.count + 1
            normalHeaderRow = separatorRow + 1
            compoundStartRow = compoundsHeaderRow + 1
            normalStartRow = normalHeaderRow + 1
        }
    }
    
    override func reloadAllComponents() {
        // reload all data
        loadData()
        
        // reload pickerView
        super.reloadAllComponents()
    }
    
    func selectUnit(unit: Unit!) {
        if unit != nil {
            if let index = Unit.findIndex(allNormalUnits, unit: unit) {
                self.selectRow(index + normalStartRow, inComponent: 0, animated: true)
                return
            } else if let index = Unit.findIndex(allCompoundUnits, unit: unit) {
                self.selectRow(index + compoundStartRow, inComponent: 0, animated: true)
                return
            } else if let defaultUnit = unit.category.getBaseUnit() {
                if let index = Unit.findIndex(allNormalUnits, unit: defaultUnit) {
                    self.selectRow(index + normalStartRow, inComponent: 0, animated: true)
                    return
                }
            }
        }
        // else
        println("CANT SELECT UNIT")
        let firstRow = normalStartRow
        self.selectRow(firstRow, inComponent: 0, animated: true)
    }
    
    func currentUnit() -> Unit? {
        return getUnitForRow(self.selectedRowInComponent(0))
    }
    
    func getUnitForRow(row: Int) -> Unit? {
        if row < normalStartRow {
            return fetchedResultsController.sections?[kCompoundUnitSection]?.objectAtIndex(row - compoundStartRow) as? Unit
        } else {
            return fetchedResultsController.sections?[kNormalUnitSection]?.objectAtIndex(row - normalStartRow) as? Unit
        }
    }
    
    // ================= PICKER VIEW DATA SOURCE =================
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let n = numberOfNormalUnits + numberOfCompoundUnits
        if showsHeaders {
            // will use 2 section titles and 1 blank space row
            return n + 3
        } else {
            return n
        }
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView {
        var tView:UILabel
        if view == nil {
            tView = UILabel()
        } else {
            tView = view as! UILabel
        }
        
        tView.minimumScaleFactor = 0.5
        tView.textAlignment = NSTextAlignment.Center
        
        // configure for section headers as necessary
        if showsHeaders {
            if row == compoundsHeaderRow || row == normalHeaderRow {
                // shrink header fontsize and maintain the gray pickerview color to simulate unselected cells
                tView.textColor = UIColor.grayColor()
                tView.font = UIFont(name: tView.font.fontName, size: tView.font.pointSize*0.8)
            }
            
            if row == compoundsHeaderRow {
                let text = NSMutableAttributedString(string: "Created Units", font: tView.font)
                text.underline(NSUnderlineStyle.StyleSingle)
                tView.attributedText = text
                return tView
            } else if row == separatorRow {
                tView.text = ""
                return tView
            } else if row == normalHeaderRow {
                let text = NSMutableAttributedString(string: "Default Units", font: tView.font)
                text.underline(NSUnderlineStyle.StyleSingle)
                tView.attributedText = text
                return tView
            }
        }
        
        // else configure for unit
        tView.textColor = UIColor.blackColor()
        
        if let unitForRow = getUnitForRow(row) {
            // get value string as necessary
            var plural = true
            var valueString: NSMutableAttributedString
            if inputUnit == nil {
                valueString = NSMutableAttributedString()
            } else {
                let val = inputUnit!.convertValue(inputValue, toUnit: unitForRow).roundTo(significantFigures: 5)
                valueString = val.attributedStringWithExponentBase10(significantFigures: 5, forFont: tView.font) + " "
                plural = val != 1
            }
            // append unit display attributed string to the value string
            tView.attributedText = valueString + unitForRow.getDisplayString(forFont: tView.font, appendFullName: true, plural: plural)
        } else {
            // should never be called unless there is a development error
            tView.text = "???"
            NSLog("Unit picker view \(self) could not func unit for row \(row)")
        }
        
        return tView
    }
    
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 25
    }
    
    // ================= PICKER VIEW DELEGATE =================
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if numberOfNormalUnits + numberOfCompoundUnits > 0 {
            if let unit = getUnitForRow(row) {
                // else, send the selected unit to the delegate
                self.unitDelegate?.unitPicker(self, didSelectUnit: unit)
            } else if showsHeaders {
                // if it is a header row, select the nearest unit row
                if row == compoundsHeaderRow || row == normalHeaderRow {
                    let newRow = row + 1
                    self.selectRow(newRow, inComponent: 0, animated: true)
                    if let unit = getUnitForRow(newRow) {
                        self.unitDelegate?.unitPicker(self, didSelectUnit: unit)
                    }
                } else if row == separatorRow {
                    let newRow = row - 1
                    self.selectRow(newRow, inComponent: 0, animated: true)
                    if let unit = getUnitForRow(newRow) {
                        self.unitDelegate?.unitPicker(self, didSelectUnit: unit)
                    }
                }
            }
        }
    }
    
    // ================= NS FETCHED RESULTS CONTROLLER DELEGATE =================
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        self.reloadAllComponents()
    }
    
}*/