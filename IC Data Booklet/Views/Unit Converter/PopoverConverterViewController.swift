//
//  PopoverConverterViewController.swift
//  Temp
//
//  Created by Mike on 06/01/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit

class PopoverConverterViewController: UIViewController, UnitPickerViewDataSource, UnitPickerViewDelegate {

    // ================= VARIABLES =================
    
    var category: UnitCategory!
    
    var allUnits: [Unit] = []
    var inputUnit: Unit! {
        didSet {
            configureForInputUnit()
        }
    }
    var inputValue: Double = 0
    var outputUnit: Unit! {
        didSet {
            if outputUnit != nil {
                let row = unitPicker.selectedRowInComponent(0)
                self.delegate?.unitConverterPage(self, didSelectUnit: self.outputUnit, selectedRow: row)
            }
        }
    }

    var delegate: UnitConverterPageDelegate? = nil
    var shouldHideToLabel = false
    
    // ================= IBOUTLETS =================
    
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var unitPicker: UnitPickerView!
    @IBOutlet weak var toLabelHeightConstraint: NSLayoutConstraint!
    
    // ================= VIEW CONTROLLER =================
    
    func selectUnit(unit: Unit!) {
        unitPicker.selectUnit(unit)
    }
    
    func configureForInputUnit() {
        if inputUnit != nil {
            if outputUnit == nil || outputUnit.category != inputUnit.category || allUnits.count == 0 {
                allUnits = inputUnit.category.getUnits(false)
                unitPicker.reloadAllComponents()
                outputUnit = inputUnit
                selectUnit(outputUnit)
            } else {
                unitPicker.reloadAllComponents()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if shouldHideToLabel {
            hideToLabel()
        }
        unitPicker.unitDataSource = self
        unitPicker.unitDelegate = self
        unitPicker.reloadAllComponents()
        self.configureForInputUnit()
        
        self.preferredContentSize = CGSize(width: unitPicker.frame.width, height: unitPicker.frame.maxY + 30)
        self.delegate?.unitConverterPage(self, didSetPreferredContentSize: preferredContentSize)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func hideToLabel() {
        if toLabel != nil {
            toLabel.hidden = true
            toLabelHeightConstraint.constant = 0
            toLabel.clipsToBounds = true
            shouldHideToLabel = false
        } else {
            shouldHideToLabel = true
        }
    }
    
    // ================ PICKER VIEW DATA SOURCE =================
    
    func unitPicker(unitCategoryForPicker unitPicker: UnitPickerView) -> UnitCategory! {
        return self.category
    }
    
    func unitPicker(inputUnitForPicker unitPicker: UnitPickerView) -> Unit? {
        return self.inputUnit
    }
    
    func unitPicker(inputValueForPicker unitPicker: UnitPickerView) -> Double {
        return self.inputValue
    }
    
    // ================ PICKER VIEW DELEGATE =================
    
    func unitPicker(unitPicker: UnitPickerView, didSelectUnit unit: Unit) {
        self.outputUnit = unit
    }
    
}
