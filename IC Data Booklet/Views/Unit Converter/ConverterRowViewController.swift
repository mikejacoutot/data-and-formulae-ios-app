//
//  ConverterRowViewController.swift
//  Temp
//
//  Created by Mike on 06/01/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit

class ConverterData {
    let name: String
    let symbol: String
    
    init(name: String, symbol: String) {
        self.name = name
        self.symbol = symbol
    }
}

class ConverterRowViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    var rowTitle: UILabel!
    var subIndicator: UILabel!
    var unitPicker: UIPickerView!
    
    var pickerData: [ConverterData]? = nil
    
    override init() {
        super.init()
    }

    override init(nibName: String?, bundle: NSBundle?) {
        super.init(nibName: nibName, bundle: bundle)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.clipsToBounds = true
        
        self.view.backgroundColor = UIColor.grayColor()
        
        self.createViewObjects()
        
        if pickerData != nil {
            rowTitle.text = pickerData![0].name
            
            if pickerData!.count > 1 {
                subIndicator.text = ">"
            } else {
                subIndicator.text = ""
            }
        }
    }
    
    func createViewObjects() {
        let bounds = self.view.frame
        
        // initialize view objects
        self.unitPicker = UIPickerView(frame: CGRect(x: 0, y: 0, width: 60, height: bounds.height))
        self.subIndicator = UILabel(frame: CGRect(x: 0, y: 0, width: 10, height: bounds.height))
        self.rowTitle = UILabel(frame: CGRect(x: 0, y: 0, width: bounds.width - 80, height: bounds.height))
        
        unitPicker.delegate = self
        unitPicker.dataSource = self
        
        
        // add as subviews
        self.view.addSubview(unitPicker)
        self.view.addSubview(subIndicator)
        self.view.addSubview(rowTitle)
        
        // unitPicker constraints
        unitPicker.setTranslatesAutoresizingMaskIntoConstraints(false)
        view.addConstraint(NSLayoutConstraint(item: unitPicker, attribute: .CenterY, relatedBy: .Equal, toItem: self.view, attribute: .CenterY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: unitPicker, attribute: .Trailing, relatedBy: .Equal, toItem: self.view, attribute: .Trailing, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: unitPicker, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 60))
        
        // subIndicator constraints
        subIndicator.setTranslatesAutoresizingMaskIntoConstraints(false)
        view.addConstraint(NSLayoutConstraint(item: subIndicator, attribute: .CenterY, relatedBy: .Equal, toItem: self.view, attribute: .CenterY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: subIndicator, attribute: .Trailing, relatedBy: .Equal, toItem: unitPicker, attribute: .Leading, multiplier: 1, constant: -5))
        view.addConstraint(NSLayoutConstraint(item: subIndicator, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 10))
        
        // rowTitle constraints
        rowTitle.setTranslatesAutoresizingMaskIntoConstraints(false)
        view.addConstraint(NSLayoutConstraint(item: rowTitle, attribute: .CenterY, relatedBy: .Equal, toItem: self.view, attribute: .CenterY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: rowTitle, attribute: .Trailing, relatedBy: .Equal, toItem: subIndicator, attribute: .Leading, multiplier: 1, constant: -5))
        view.addConstraint(NSLayoutConstraint(item: rowTitle, attribute: .Leading, relatedBy: .Equal, toItem: self.view, attribute: .Leading, multiplier: 1, constant: 5))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK - PICKER VIEW DELEGATE
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        println(pickerData)
        if let rows = pickerData?.count {
            return rows
        }
        return 0
    }
    
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> Float {
        return 20
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        
        var tView:UILabel
        
        if view == nil {
            tView = UILabel()
        } else {
            tView = view as UILabel
        }
        
        tView.minimumScaleFactor = 0.5
        tView.textAlignment = NSTextAlignment.Center
        tView.textColor = UIColor.blackColor()
        tView.text = {
            if self.pickerData != nil {
                return self.pickerData![row].symbol
            }
            return "???"
        }()
        
        return tView
    }


}
