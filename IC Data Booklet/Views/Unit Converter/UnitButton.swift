//
//  UnitButton.swift
//  IC Data Booklet
//
//  Created by Mike on 28/09/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit

protocol UnitButtonDelegate {
    func unitButton(unitButton: UnitButton, didChangeToUnit unit: Unit, withValue: Double)
}

class UnitButton: UIButton, UnitConverterDataSource, UnitConverterDelegate {
    
    // ================ INITIALIZERS =================
    
    init(unit: Unit) {
        super.init(frame: CGRectZero)
        self.unit = unit
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialConfiguration()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialConfiguration()
    }
    
    private func initialConfiguration() {
        // add target to call the popup unit converter on init
        self.addTarget(self, action: "callUnitConverter:", forControlEvents: .TouchUpInside)
    }
    
    // ================ VARIABLES =================

    var value: Double = 1
    
    var unit: Unit! 
    
    func configureForUnit(unit: Unit!, value: Double) {
        // if unit == nil OR the category is dimensionless, hide the button, otherwise configure it
        if unit.category.isDimensionless.boolValue ?? true {
            self.hidden = true
        } else {
            let title = unit.getDisplayString(forFont: self.titleLabel?.font, appendFullName: false, plural: value != 1)
            self.setAttributedTitle(title, forState: .Normal)
        }
        
        self.unit = unit
        self.value = value
    }
    
    func convertToUnit(unit: Unit!) {
        if self.unit != nil && unit != nil {
        configureForUnit(unit, value: self.unit.convertValue(self.value, toUnit: unit))
        } else {
            self.unit = nil
            self.value = 1
        }
    }
    
    var delegate: UnitButtonDelegate?
    
    // ================ GENERAL FUNCTIONS =================
    
    func callUnitConverter(sender: UnitButton) {
        let previousUnit = unit
        let unitConverter = PopoverController.unitConverter(self, dataSource: self, popoverAnchor: self, cancelHandler: {
            alert in
            self.unit = previousUnit
        }, saveHandler: nil)
        // don't include a saveHandler as the unit conversion currently already takes place through the delegate methods

        unitConverter.show()
    }
    
    // ================ UNIT CONVERTER DATA SOURCE =================
    
    func unitConverter(inputUnitForConverter converter: UnitConverterViewController) -> Unit? {
        return self.unit
    }
    
    func unitConverter(inputValueForConverter converter: UnitConverterViewController) -> Double {
        return self.value
    }
    
    // ================ UNIT CONVERTER DELEGATE =================
    
    func unitConverter(converter: UnitConverterViewController, didSelectUnit unit: Unit, value: Double, selectedRow row: Int) {
        configureForUnit(unit, value: value)
        delegate?.unitButton(self, didChangeToUnit: unit, withValue: value)
    }
    
    func unitConverter(converter: UnitConverterViewController, didSwapToUnit unit: Unit, value: Double, selectedRow row: Int) {
        configureForUnit(unit, value: value)
        delegate?.unitButton(self, didChangeToUnit: unit, withValue: value)
    }
}
