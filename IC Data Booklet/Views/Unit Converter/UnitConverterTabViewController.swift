//
//  UnitConverterTabViewController.swift
//  Temp
//
//  Created by Mike on 29/03/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit

@objc
protocol UnitConverterDelegate {
    optional func unitConverter(converter: UnitConverterViewController, didSelectUnit unit: Unit, value: Double, selectedRow row: Int)
    optional func unitConverter(converter: UnitConverterViewController, didSwapToUnit unit: Unit, value: Double, selectedRow row: Int)
    optional func unitConverter(converter: UnitConverterViewController, didLayoutSegmentedControl hidden: Bool, containerHeight: CGFloat)
    optional func unitConverter(converter: UnitConverterViewController, didForceSegmentedControlToIndex index: Int)
    optional func unitConverter(converter: UnitConverterViewController, animatingToPreferredContentSize contentSize: CGSize, duration: NSTimeInterval)
}

protocol UnitConverterDataSource {
    func unitConverter(inputValueForConverter converter: UnitConverterViewController) -> Double
    func unitConverter(inputUnitForConverter converter: UnitConverterViewController) -> Unit?
}

protocol UnitConverterPageDelegate {
    func unitConverterPage(page: AnyObject, didSelectUnit unit: Unit, selectedRow: Int)
    func unitConverterPage(page: AnyObject, didSetPreferredContentSize contentSize: CGSize)
}

class UnitConverterViewController: UIViewController, UnitConverterPageDelegate {
    
    // ================= VARIABLES =================
    
    var category: UnitCategory! {
        didSet {
            configureForCategory()
            self.compoundConverter.category = category
            self.unitConverter.category = category
        }
    }

    var inputUnit: Unit! {
        didSet {
            if category != inputUnit.category {
                category = inputUnit.category
            }
            
            self.unitConverter?.inputUnit = inputUnit
            self.compoundConverter?.inputUnit = inputUnit
        }
    }
    var inputValue: Double = 0 {
        didSet {
            self.unitConverter?.inputValue = inputValue
            self.compoundConverter?.inputValue = inputValue
        }
    }
    
    var outputUnit: Unit! {
        didSet {
            if outputUnit != nil {
                self.outputValue = inputUnit?.convertValue(inputValue, toUnit: outputUnit) ?? 0
                delegate?.unitConverter?(self, didSelectUnit: outputUnit, value: outputValue, selectedRow: selectedRow)
                updateRatioLabel()
            }
        }
    }
    var outputValue: Double = 0
    var selectedRow: Int = 0 
    
    var isHalf = false
    var embedded = false
    var hidesSwapButton = false
    var scrollViewLeadingConstraint: NSLayoutConstraint!
    
    var delegate: UnitConverterDelegate? = nil
    var dataSource: UnitConverterDataSource? = nil
    
    var unitConverter: PopoverConverterViewController!
    var compoundConverter: CompoundConverterViewController!
    
    var pageContentSizes: [CGSize] = [CGSizeZero,CGSizeZero]
    
    // ================= IB OUTLETS =================
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var segmentedControlContainer: UIView!
    @IBOutlet weak var segmentedControlContainerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var converterContainer: UIView!
    @IBOutlet weak var compoundContainer: UIView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var ratioLabel: UILabel!
    @IBOutlet weak var swapButton: UIButton!
    
    // ================= IB ACTIONS =================
    
    @IBAction func segmentPressed(sender: UISegmentedControl!) {
        if sender != segmentedControl {
            segmentedControl.selectedSegmentIndex = sender.selectedSegmentIndex
        }
        scrollView.contentOffset = CGPointZero
        switch sender.selectedSegmentIndex {
        case 0:
            converterContainer.hidden = false
            compoundContainer.hidden = true
            if let unit = unitConverter?.outputUnit {
                scrollView.contentSize = CGSize(width: 0, height: unitConverter!.unitPicker.frame.maxY)
                self.outputUnit = unit
            }
            break
        case 1:
            converterContainer.hidden = true
            compoundContainer.hidden = false
            if let unit = compoundConverter?.outputUnit {
                scrollView.contentSize = CGSize(width: 0, height: compoundConverter!.converterTableView.frame.maxY)
                self.outputUnit = unit
            }
            break
        default: ()
        }
    }
    
    @IBAction func swapPressed(sender: AnyObject) {
        if outputUnit != nil {
            delegate?.unitConverter?(self, didSwapToUnit: outputUnit, value: outputValue, selectedRow: selectedRow)
        }
    }
    
    // ================= VIEW CONTROL =================

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if hidesSwapButton {
            swapButton.hidden = true
            swapButton.enabled = false
        }
        reloadData()

        self.scrollView.translatesAutoresizingMaskIntoConstraints = false
        self.updateWidth(isHalf)
        //PREFERRED CONTENT SIZE
        
        updatePreferredContentSize()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateWidth(half: Bool) {
        if scrollView != nil {
            if scrollViewLeadingConstraint != nil {
                view.removeConstraint(scrollViewLeadingConstraint)
            }
            if half {
                scrollViewLeadingConstraint = NSLayoutConstraint(item: scrollView, attribute: .Leading, relatedBy: .Equal, toItem: self.view, attribute: .CenterX, multiplier: 1, constant: 0)
            } else {
                scrollViewLeadingConstraint = NSLayoutConstraint(item: scrollView, attribute: .Leading, relatedBy: .Equal, toItem: self.view, attribute: .Leading, multiplier: 1, constant: 0)
            }
            self.view.addConstraint(scrollViewLeadingConstraint)
            isHalf = half
        }
    }
    
    // ================= METHODS =================
    
    private func configureForCategory() {
        if category != nil && segmentedControl != nil {
            if category.getCompoundCategories().count == 0 {
                segmentedControl.selectedSegmentIndex = 0
                delegate?.unitConverter?(self, didForceSegmentedControlToIndex: 0)
                hideControl()
            } else {
                showControl()
            }
            segmentPressed(segmentedControl)
        }
    }
    
    func hideControl() {
        self.segmentedControlContainer?.hidden = true
        self.segmentedControlContainerHeightConstraint?.constant = 0
        delegate?.unitConverter?(self, didLayoutSegmentedControl: true, containerHeight: 0)
    }
    
    func showControl() {
        self.segmentedControlContainer?.hidden = embedded ? true : false
        self.segmentedControlContainerHeightConstraint?.constant = embedded ? 0 : 50
        delegate?.unitConverter?(self, didLayoutSegmentedControl: false, containerHeight: 50)
    }
    
    func updateRatioLabel() {
        if inputUnit != nil  && outputUnit != nil {
            let ratio = inputUnit.convertValue(1, toUnit: outputUnit)
            let ratioAttString = ratio.attributedStringWithExponentBase10(significantFigures: 4, forFont: ratioLabel.font)
            let plural = ratio != 1
            ratioLabel.attributedText = NSMutableAttributedString(string: "1 \(inputUnit.name) = ") + ratioAttString + " \(outputUnit.getName(plural))"
        } else {
            ratioLabel.text = "1 ??? = ???"
        }
    }

    func reloadData() {
        if let fetchedValue = dataSource?.unitConverter(inputValueForConverter: self) {
                inputValue = fetchedValue
        }
        if let fetchedUnit = dataSource?.unitConverter(inputUnitForConverter: self) {
                inputUnit = fetchedUnit
        } else {
            print("CONVERTER HAS NO UNIT!!")
        }
        updateRatioLabel()
    }
    
    // ================= UNIT CONVERTER PAGE DELEGATE =================
    
    func unitConverterPage(page: AnyObject, didSelectUnit unit: Unit, selectedRow: Int) {
        // only update stored properties if the current page was sent, ignore duplicate calls due to global input unit being set
        var isCurrentPage = false
        let currentPage = self.segmentedControl.selectedSegmentIndex
        if currentPage == 0 {
            if let _ = page as? PopoverConverterViewController {
                isCurrentPage = true
            }
        } else if currentPage == 1 {
            if let _ = page as? CompoundConverterViewController {
                isCurrentPage = true
            }
        }
    
        if isCurrentPage {
            self.selectedRow = selectedRow
            self.outputUnit = unit
        } 
    }
    
    func unitConverterPage(page: AnyObject, didSetPreferredContentSize contentSize: CGSize) {
        if let _ = page as? PopoverConverterViewController {
            pageContentSizes[0] = contentSize
        } else if let _ = page as? CompoundConverterViewController {
            pageContentSizes[1] = contentSize
        }
        updatePreferredContentSize()
    }
    
    func updatePreferredContentSize() {
        var largestSize = CGSizeZero
        for size in pageContentSizes {
            if size.width > largestSize.width {
                largestSize.width = size.width
            }
            if size.height > largestSize.height {
                largestSize.height = size.height
            }
        }
        
        let heightAbovePage = segmentedControlContainerHeightConstraint.constant
        let heightBelowPage = swapButton.frame.maxY + 20 - scrollView.frame.maxY
        scrollViewHeightConstraint.constant = largestSize.height
        self.preferredContentSize = CGSize(width: largestSize.width, height: largestSize.height + heightAbovePage + heightBelowPage)
        let duration: NSTimeInterval = 0.3
        delegate?.unitConverter?(self, animatingToPreferredContentSize: preferredContentSize, duration: duration)
        
        UIView.animateWithDuration(duration, animations: {
            self.scrollView.updateConstraints()
            self.view.layoutIfNeeded()
        })
    }
    
    // ================= NAVIGATION =================
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "embedConverter" {
            self.unitConverter = segue.destinationViewController as! PopoverConverterViewController
            unitConverter.delegate = self
            if !embedded {
                unitConverter.hideToLabel()
            }
        } else if segue.identifier == "embedCompound" {
            self.compoundConverter = segue.destinationViewController as! CompoundConverterViewController
            compoundConverter.delegate = self
        }
    }

}
