//
//  MasterViewController.swift
//  MasterDetailTest
//
//  Created by Mike on 18/08/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit
import CoreData

protocol CompoundMasterViewControllerDelegate {
    func compoundMaster(didSelectUnit unit: Unit)
    func compoundMaster(didCreateUnit newUnit: Unit)
}

class CompoundMasterViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    
    // ================ VARIABLES =================

    var detailViewController: CompoundDetailViewController? = nil
    var delegate: CompoundMasterViewControllerDelegate?
    
    lazy var fetchedResultsController: NSFetchedResultsController = {
        let predicate = NSPredicate(format: "isCompound=true", argumentArray: nil)
        let sortName = NSSortDescriptor(key: "name", ascending: true)
        return databaseManager.getFetchedResultsController(kDataUnit, withPredicate: predicate, withSortDescriptors: [sortName], sectionNameKeyPath: "category.name", delegate: self)
    }()
    
    // ================ VIEW CONTROL =================

    override func awakeFromNib() {
        super.awakeFromNib()
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            self.clearsSelectionOnViewWillAppear = false
            self.preferredContentSize = CGSize(width: 320.0, height: 600.0)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationItem.leftBarButtonItem = self.editButtonItem()

        let addButton = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: "insertNewObject:")
        self.navigationItem.rightBarButtonItem = addButton
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = controllers[controllers.count-1] as? CompoundDetailViewController
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func insertNewObject(sender: AnyObject) {
        let nextPK = settingsManager.nextCompoundUnitPK
        let newUnit = databaseManager.getEntity(kDataUnit, withID: nextPK, createIfMissing: true) as! Unit
        newUnit.name = "New Unit"
        
        delegate?.compoundMaster(didCreateUnit: newUnit)
    }
    
    /*/ ================ NAVIGATION =================

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow() {
            let unit = self.fetchedResultsController.objectAtIndexPath(indexPath) as! Unit
                let controller = (segue.destinationViewController as! UINavigationController).topViewController as! CompoundDetailViewController
                controller.unit = unit
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }*/

    // ================ TABLE VIEW DATA SOURCE =================

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.fetchedResultsController.sections?.count ?? 0
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = self.fetchedResultsController.sections![section] 
        return sectionInfo.numberOfObjects
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) 
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }

    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            
            let unit = self.fetchedResultsController.objectAtIndexPath(indexPath) as! Unit
            databaseManager.deleteObject(unit)
            databaseManager.saveContext()
        }
    }

    func configureCell(cell: UITableViewCell, atIndexPath indexPath: NSIndexPath) {
            let unit = self.fetchedResultsController.objectAtIndexPath(indexPath) as! Unit
        cell.textLabel!.text = unit.name
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let unit = self.fetchedResultsController.objectAtIndexPath(indexPath) as! Unit
        self.delegate?.compoundMaster(didSelectUnit: unit)
        
        if detailViewController != nil {
            splitViewController?.showDetailViewController(detailViewController!.navigationController!, sender: nil)
        }
    }

    // ================ FETCHED RESULTS CONTROLLER DELEGATE =================

    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        self.tableView.beginUpdates()
    }

    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        switch type {
            case .Insert:
                self.tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
            case .Delete:
                self.tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
            default:
                return
        }
    }

    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        switch type {
            case .Insert:
                tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
            case .Delete:
                tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
            case .Update:
                self.configureCell(tableView.cellForRowAtIndexPath(indexPath!)!, atIndexPath: indexPath!)
            case .Move:
                tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
                tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
        }
    }

    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        self.tableView.endUpdates()
    }

}

