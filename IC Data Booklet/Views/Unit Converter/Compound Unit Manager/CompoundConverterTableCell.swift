//
//  CompoundConverterTableCell.swift
//  Temp
//
//  Created by Mike on 09/03/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit

@objc
protocol CompoundConverterTableCellDelegate {
    optional func compoundConverterCell(cell: CompoundConverterTableCell, didSelectUnit unit: Unit)
}

class CompoundConverterTableCell: UITableViewCell, UnitPickerViewDelegate, UnitPickerViewDataSource {
    
    // ================ VARIABLES =================
    
    var indexPath: NSIndexPath!
    var category: UnitCategory!
    
    var delegate: CompoundConverterTableCellDelegate?
    
    // ================ IB OUTLETS =================
    
    @IBOutlet weak var unitPicker: UnitPickerView! {
        didSet {
            unitPicker.unitDelegate = self
            unitPicker.unitDataSource = self
        }
    }
    
    // ================ UNIT PICKER DATA SOURCE =================
    
    func unitPicker(unitCategoryForPicker unitPicker: UnitPickerView) -> UnitCategory! {
        return category
    }
    
    // ================ UNIT PICKER DELEGATE =================
    
    func unitPicker(unitPicker: UnitPickerView, didSelectUnit unit: Unit) {
        delegate?.compoundConverterCell?(self, didSelectUnit: unit)
    }
    
}


