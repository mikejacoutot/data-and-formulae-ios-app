//
//  DetailViewController.swift
//  MasterDetailTest
//
//  Created by Mike on 18/08/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit

class CompoundDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CompoundConverterTableCellDelegate, UIScrollViewDelegate, TitleScrollViewDelegate, CompoundMasterViewControllerDelegate {
    
    // ================ VARIABLES =================
    var inputUnit: Unit! {
        didSet {
            if inputUnit != nil {
                if category != inputUnit.category {
                    self.category = inputUnit.category
                    outputUnit = inputUnit
                } else if outputUnit == nil {
                    outputUnit = inputUnit
                }
            } else {
                category = nil
            }
        }
    }
    var category: UnitCategory! {
        didSet {
            self.configureForCategory()
        }
    }
    var compoundCategories: [CompoundCategory] = []
    var currentSection = 0
    
    var compoundUnits: [Unit] = []
    var outputUnit: Unit! {
        didSet {
            if outputUnit != nil {
                self.delegate?.unitConverterPage(self, didSelectUnit: self.outputUnit, selectedRow: 0)
                saveButton.enabled = true
            } else {
                saveButton.enabled = false
            }
        }
    }
    
    var buttons: [UIButton?] = []
    var pressedButtonIndex = -1
    var isNewUnit: Bool = false
    
    var delegate: UnitConverterPageDelegate?
    
    // ================ IB OUTLETS =================
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    
    @IBOutlet weak var categoryPicker: UIPickerView!
    @IBOutlet weak var buttonContainer: UIView!
    @IBOutlet weak var converterTableView: UITableView!
    
    @IBOutlet weak var titleScrollView: TitleScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    // ================ IB ACTIONS =================
    
    @IBAction func viewTapped(sender: AnyObject) {
        // clear selected button
        buttonPressed(nil)
    }
    
    @IBAction func cancelPressed(sender: AnyObject) {
        if isNewUnit {
            databaseManager.deleteObject(inputUnit)
        }
        self.inputUnit = nil
        self.outputUnit = nil
        buttonPressed(nil)
        // RETURN TO MASTER
    }
    
    @IBAction func savePressed(sender: AnyObject) {
        if outputUnit != nil {
            databaseManager.saveContext()
        }
        // RETURN TO MASTER
    }
    
    // ================ VIEW CONTROL =================
    
    override func loadView() {
        let frame = UIScreen.mainScreen().applicationFrame
        let scrollView = UIScrollView(frame: frame)
        scrollView.delegate = self
        self.view = scrollView
        self.view.autoresizingMask = [.FlexibleHeight, .FlexibleWidth]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageControl.hidesForSinglePage = true
        titleScrollView.pageControl = pageControl
        titleScrollView.delegate = self
        titleScrollView.titleDelegate = self
        configureForCategory()
        
        converterTableView.scrollEnabled = false
        
        self.preferredContentSize = CGSize(width: converterTableView.frame.width, height: converterTableView.frame.minY + 20)
        self.delegate?.unitConverterPage(self, didSetPreferredContentSize: preferredContentSize)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        titleScrollView.selectTitle(titleScrollView.currentPage, animated: false)
    }
    
    func configureForCategory() {
        if category != nil {
            self.compoundCategories = category.getCompoundCategories()
            self.compoundUnits = []
            for i in 0..<compoundCategories.count {
                let pk = self.pkForIndex(i)
                let compoundUnit = databaseManager.getEntity(kDataUnit, withID: pk, createIfMissing: true) as! Unit
                compoundUnit.makeCompoundUnit(fromCompound: self.compoundCategories[i], forCategory: self.category)
                compoundUnits += [compoundUnit]
                
                if let compound = compoundCategories[¿0] {
                    self.configureButtonContainerForCompound(compoundCategories[0], forceRefresh: true)
                    var titles: [NSMutableAttributedString] = []
                    let font = UILabel().font
                    for compound in compoundCategories {
                        titles += [compound.getTitles(font)]
                    }
                    titleScrollView.attributedTitles = titles
                }
            }
        }
        converterTableView.reloadData()
        buttonPressed(nil)
    }
    
    // ================ TITLE SCROLL VIEW =================
    
    func titleScrollView(titleScrollView: TitleScrollView, shouldScrollToSelectedTitle title: String, atIndex index: Int) -> Bool {
        if index != self.currentSection {
            if let unit = self.compoundUnits[¿index] {
                self.outputUnit = unit
            }
            UIView.animateWithDuration(0.3, animations: {
                self.currentSection = index
                self.buttonPressed(nil)
                self.configureButtonContainerForCompound(self.compoundCategories[index], forceRefresh: true)
                self.converterTableView.beginUpdates()
                self.converterTableView.endUpdates()
            })
        }
        return true
    }
    
    // ================ BUTTON CONTAINER =================
    
    func configureButtonContainerForCompound(var compound: CompoundCategory, forceRefresh: Bool = false) {
        
        if forceRefresh {
            // wipe created buttons to be remade
            for button in buttons {
                button?.removeFromSuperview()
            }
            self.buttons = []
        } else if self.buttons.count != 0 {
            return // buttons already initialised so do nothing
        }
        
        var i = 0
        for category in compound.components {
            if category.exponent != 0 {
                var unit: Unit!
                if category.unit != nil {
                    unit = category.unit
                } else if let baseUnit = category.category.getBaseUnit() {
                    unit = baseUnit
                }
                if unit != nil {
                    
                    let button = UIButton(type: UIButtonType.System)
                    
                    button.setTitleColor(UIColor.lightGrayColor(), forState: [.Disabled, .Highlighted, .Selected]) // doesnt work
                    
                    // format title label
                    button.titleLabel?.font = UIFont.systemFontOfSize(22)
                    setTitleForButton(button, forUnit: unit, andExponent: category.exponent)
                    
                    button.addTarget(self, action: "buttonPressed:", forControlEvents: .TouchUpInside)
                    button.tag = i
                    
                    // add and constrain button to the container, and add its reference it to the array
                    buttonContainer.addSubview(button)
                    button.translatesAutoresizingMaskIntoConstraints = false
                    buttonContainer.addConstraint(NSLayoutConstraint(item: button, attribute: .CenterY, relatedBy: .Equal, toItem: buttonContainer, attribute: .CenterY, multiplier: 1, constant: 0))
                    if i == 0 {
                        buttonContainer.addConstraint(NSLayoutConstraint(item: button, attribute: .Leading, relatedBy: .Equal, toItem: buttonContainer, attribute: .Leading, multiplier: 1, constant: 0))
                    } else {
                        let prevButton = buttons[i-1]
                        buttonContainer.addConstraint(NSLayoutConstraint(item: button, attribute: .Leading, relatedBy: .Equal, toItem: prevButton, attribute: .Trailing, multiplier: 1, constant: 5))
                    }
                    // constrain last button trailing to the container trailing to center it within the superview
                    if i == compound.components.count - 1 {
                        buttonContainer.addConstraint(NSLayoutConstraint(item: button, attribute: .Trailing, relatedBy: .Equal, toItem: buttonContainer, attribute: .Trailing, multiplier: 1, constant: 0))
                    }
                    buttons.insert(button, atIndex: i)
                    button.enabled = true
                    compound.components[i].unit = unit
                } else {
                    buttons += [nil] //.insert(nil, atIndex: i)
                    compound.components[i].unit = inputUnit
                }
            } else {
                buttons += [nil]//.insert(nil, atIndex: i)
                compound.components[i].unit = inputUnit
            }
            i++
        }
    }
    
    func setTitleForButton(button: UIButton, forUnit unit: Unit, andExponent exponent: Int) {
        let font = button.titleLabel?.font
        var title = unit.getDisplayString(forFont: font, appendFullName: false)
        
        // add exponent string if exponent != 1. Otherwise add blank superscript character to keep the button center inline
        let exponentString = exponent != 1 && exponent != 0 ? "\(exponent)" : " "
        
        // for clarity, if the unit already has superscript charaters (e.g. an exponent) and an exponent will be added, put it in brackets
        if !exponentString.isWhiteSpace() && unit.displayHasSuperscriptCharacters() {
            title = NSMutableAttributedString(string: "(") + title + ")"
        }
        title += exponentString.getSuperscript(font)
        button.setAttributedTitle(title, forState: .Normal)
        button.sizeToFit()
    }
    
    func buttonPressed(sender: AnyObject?) {
        let prevButtonIndex = pressedButtonIndex
        if let prevButton = buttons[¿prevButtonIndex] {
            // reenable previous button & hide tableview cell
            prevButton?.enabled = true
            prevButton?.alpha = 1
        }
        
        // disable pressed button & show respective cell
        if let button = sender as? UIButton {
            pressedButtonIndex = button.tag
            button.enabled = false
            button.alpha = 0.4
            
            self.preferredContentSize = CGSize(width: converterTableView.frame.width, height: converterTableView.frame.minY + 220)
            self.delegate?.unitConverterPage(self, didSetPreferredContentSize: preferredContentSize)
            
        } else {
            // hide all cells
            pressedButtonIndex = -1
            
            self.preferredContentSize = CGSize(width: converterTableView.frame.width, height: converterTableView.frame.minY + 20)
            self.delegate?.unitConverterPage(self, didSetPreferredContentSize: preferredContentSize)
        }
        converterTableView.beginUpdates()
        converterTableView.endUpdates()
    }
    
    // ================ MASTER COMPOUND VIEW CONTROLLER DELEGATE =================
    
    func compoundMaster(didCreateUnit newUnit: Unit) {
        self.inputUnit = newUnit
        self.isNewUnit = true
    }
    
    func compoundMaster(didSelectUnit unit: Unit) {
        self.inputUnit = unit
        self.isNewUnit = false
    }
    
    // ================ TABLE VIEW DATA SOURCE =================
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.compoundCategories.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = self.compoundCategories[¿section]?.components.count {
            return count
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("SelectorCell") as! CompoundConverterTableCell
        
        if let componentAtIndex = self.compoundCategories[¿indexPath.section]?.components[¿indexPath.row] {
            let categoryAtIndex = componentAtIndex.category
            if cell.category != categoryAtIndex {
                cell.category = categoryAtIndex
                cell.unitPicker.reloadAllComponents()
            }
            
            if let selectedUnit = componentAtIndex.unit {
                cell.unitPicker.selectUnit(selectedUnit)
            }
            cell.clipsToBounds = true
            cell.indexPath = indexPath
            cell.delegate = self
        }
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == currentSection && indexPath.row == pressedButtonIndex {
            return 200
        }
        return 0
    }
    
    
    
    // ================ TABLE VIEW DELEGATE =================
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    }
    
    // ================ CONVERTER TABLE CELL DELEGATE =================
    
    func compoundConverterCell(cell: CompoundConverterTableCell, didSelectUnit unit: Unit) {
        let indexPath = cell.indexPath
        
        if let b = buttons[¿indexPath.row] {
            if let button = b { // optional chaining is failing to test the second optional as of swift 1.2
                if let exponent = compoundCategories[¿indexPath.section]?.components[¿indexPath.row]?.exponent {
                    setTitleForButton(button, forUnit: unit, andExponent: exponent)
                    compoundCategories[indexPath.section].components[indexPath.row].unit = unit
                    
                    let compoundUnit = compoundUnits[indexPath.section]
                    compoundUnit.makeCompoundUnit(fromCompound: self.compoundCategories[indexPath.section], forCategory: self.category)
                    self.outputUnit = compoundUnit
                    
                    // CURRENTLY SAVE THE NEW UNIT
                    databaseManager.saveContext()
                }
            }
        }
    }
    
    func pkForIndex(index: Int) -> Int {
        return settingsManager.nextCompoundUnitPK
    }
    
    
    /*// ================ NAVIGATION =================
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    }*/
    
}

