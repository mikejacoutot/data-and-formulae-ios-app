//
//  ConverterViewController.swift
//  Test
//
//  Created by Mike on 18/12/2014.
//  Copyright (c) 2014 Mike. All rights reserved.
//

import UIKit
import CoreData


class ConverterViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UnitPickerViewDataSource, UnitPickerViewDelegate, UnitConverterDelegate, UnitConverterDataSource {
    
    // ================ VARIABLES =================
    
    var unitConverter: UnitConverterViewController!
    
    var prevValue: String = ""
    var allCategories: [UnitCategory] = []
    
    // ================ IBOUTLETS =================
    
    @IBOutlet weak var categoryPicker: UIPickerView!
    @IBOutlet weak var unitPicker: UnitPickerView!
    @IBOutlet weak var inputField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var containerViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var segmentedControlContainer: UIView!
    @IBOutlet weak var segmentedControlContainerHeightConstraint: NSLayoutConstraint!
    
    // ================ IBACTIONS =================
    
    @IBAction func textFieldDidChange(sender: AnyObject) {
        unitPicker.reloadAllComponents()
        unitConverter?.reloadData()
    }
    
    @IBAction func scrollViewTapped(sender: AnyObject) {
        // end editing to hide keyboard if the scrollview is tapped
        self.view.endEditing(true)
        self.unitConverter.compoundConverter.viewTapped(sender)
    }
    
    @IBAction func segmentPressed(sender: UISegmentedControl!) {
        unitConverter?.segmentPressed(sender)
    }
    
    // ================ VIEW CONTROLLER =================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Unit Conversion"
        self.registerKeyboardNotifications()
        
        unitPicker.unitDataSource = self
        unitPicker.unitDelegate = self
    
        // get categories
        let countPred = NSPredicate(format: "units.@count >= 1", argumentArray: nil)
        let hiddenPred = NSPredicate(format: "hidden = false", argumentArray: nil)
        let compoundPred = NSCompoundPredicate(type: .AndPredicateType, subpredicates: [countPred,hiddenPred])
        allCategories = (databaseManager.getAllEntitiesOfType(kDataUnitCategory, withPredicate: compoundPred) as! [UnitCategory]).sort({ $0.name < $1.name })

        unitPicker.reloadAllComponents()
        unitConverter.reloadData()
        
        if allCategories.count != 0 {
            let defaultUnit = allCategories[0].getBaseUnit()
            unitPicker.selectUnit(defaultUnit)
        } 

        // set content size for scrollview with a margin to allow for the keyboard
        scrollView.scrollEnabled = true
        
        inputField.text = "0.0"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // ================ KEYBOARD NOTIFICATIONS =================
    
    override func keyboardWasShown (notification: NSNotification) {
        let info = notification.userInfo!
        let key = String(UIKeyboardFrameBeginUserInfoKey)
        
        if let keyboardHeight = info[key]?.CGRectValue.height {
            
            let insets: UIEdgeInsets = UIEdgeInsetsMake(self.scrollView.contentInset.top, 0, keyboardHeight, 0)
            
            self.scrollView.contentInset = insets
            self.scrollView.scrollIndicatorInsets = insets
        }
    }
    
    override func keyboardWillBeHidden (notification: NSNotification) {
        self.scrollView.contentInset = UIEdgeInsetsZero
        self.scrollView.scrollIndicatorInsets = UIEdgeInsetsZero
    }
    
    // ================= TEXT FIELD DELEGATE =================
    
    func textFieldShouldReturn(textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    // ================ UNIT PICKER DATA SOURCE =================
    
    func unitPicker(unitCategoryForPicker unitPicker: UnitPickerView) -> UnitCategory! {
        if let category  = allCategories[¿categoryPicker.selectedRowInComponent(0)] {
            return category
        }
        return allCategories[¿0]
    }
    
    
    // ================ UNIT PICKER DELEGATE =================
    
    func unitPicker(unitPicker: UnitPickerView, didSelectUnit unit: Unit) {
        // always update the unit converter, for both category and unit change
        unitConverter?.reloadData()
    }
    
    // ================= PICKER VIEW DATA SOURCE =================
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        if pickerView == categoryPicker {
            return 1
        }
        return 0
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == categoryPicker {
            return allCategories.count
        }
        return 0
    }

    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        var tView:UILabel
        if view == nil { tView = UILabel() } else { tView = view as! UILabel }
        
        tView.minimumScaleFactor = 0.5
        tView.textAlignment = NSTextAlignment.Center
        tView.textColor = UIColor.blackColor(   )
        
        if pickerView == categoryPicker {
            tView.text = allCategories[row].name
        }
        return tView
    }
    
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 25
    }
    
    // ================= PICKER VIEW DELEGATE =================
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == categoryPicker {
            unitPicker.reloadAllComponents()
            
            // when the category changes, attempt to select the base unit. Otherwise select the first row
            let defaultUnit = allCategories[row].getBaseUnit()
            unitPicker.selectUnit(defaultUnit)
        }
        // always update the unit converter, for both category and unit change
        unitConverter.reloadData()
    }

    
    // ================= POPOVER CONVERTER DELEGATE ========s=========
    
    func unitConverter(converter: UnitConverterViewController, didSwapToUnit unit: Unit, value: Double, selectedRow: Int) {

        inputField.text = value.roundTo(significantFigures: 5).stringValue()

        let inputUnit = unitConverter.inputUnit
        
        unitPicker.selectUnit(unit)
        
        unitConverter?.reloadData()
        unitConverter.unitConverter.unitPicker.selectUnit(inputUnit)
        unitConverter.unitConverter.outputUnit = inputUnit        
        
        self.view.endEditing(true)
    }
    
    func unitConverter(converter: UnitConverterViewController, didLayoutSegmentedControl hidden: Bool, containerHeight: CGFloat) {
        UIView.animateWithDuration(0.3, delay: 0, options: UIViewAnimationOptions.CurveLinear, animations: {
            self.segmentedControlContainerHeightConstraint.constant = containerHeight
            self.segmentedControlContainer.layoutIfNeeded()
            self.view.layoutSubviews()
            }, completion: { _ in
                self.segmentedControlContainer.hidden = false
        })
    }
    
    func unitConverter(converter: UnitConverterViewController, didForceSegmentedControlToIndex index: Int) {
        self.segmentedControl.selectedSegmentIndex = index
    }
    
    func unitConverter(converter: UnitConverterViewController, animatingToPreferredContentSize contentSize: CGSize, duration: NSTimeInterval) {
        self.containerViewHeightConstraint.constant = contentSize.height
        UIView.animateWithDuration(duration, animations: {
            self.containerView.updateConstraints()
            self.view.layoutIfNeeded()
        })
    }
    
    func unitConverter(converter: UnitConverterViewController, didSetPreferredContentSize contentSize: CGSize) {
        self.containerViewHeightConstraint.constant = contentSize.height
    }
    
    // ================= POPOVER CONVERTER DATA SOURCE =================
    
    func unitConverter(inputUnitForConverter converter: UnitConverterViewController) -> Unit? {
        if let currentUnit = unitPicker.currentUnit() {
            return currentUnit
        } else if let category  = allCategories[¿categoryPicker.selectedRowInComponent(0)] {
            return category.getBaseUnit()
        }
        return nil
    }
    
    func unitConverter(inputValueForConverter converter: UnitConverterViewController) -> Double {
        return NSString(string: inputField.text ?? "").doubleValue
    }
    
    // ================= NAVIGATION =================
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        /*if segue.identifier == "embedPickerRow" {
            let viewController = segue.destinationViewController as ConverterRowViewController
            viewController.pickerData = [ConverterData(name: "Pascals", symbol: "Pa"),ConverterData(name: "Kilopascals", symbol: "kPa"),ConverterData(name: "Megapascals", symbol: "MPa"),ConverterData(name: "Gigapascals", symbol: "GPa")]
        
        } else*/
        if segue.identifier == "showConverter" || segue.identifier == "embedConverter" {
            let vc = segue.destinationViewController as! UnitConverterViewController
            
            vc.dataSource = self
            vc.delegate = self
            
            if segue.identifier == "embedConverter" {
                unitConverter = vc
                unitConverter.embedded = true
                if IS_IPAD {
                    unitConverter.isHalf = true
                }
            }
        }
    }
    
    // ================= GESTURE RECOGNIZERS =================
    
    /*override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        super.touchesBegan(touches, withEvent: event)
        // Resign keyboards when screen clicked
        self.view.endEditing(true)
    }*/
    
}
