//
//  ResistorViewController.swift
//  Test
//
//  Created by Mike on 11/12/2014.
//  Copyright (c) 2014 Mike. All rights reserved.
//

import UIKit
import Foundation

private let kBlack = UIColor.blackColor()
private let kBrown = UIColor.brownColor()
private let kRed = UIColor.redColor()
private let kOrange = UIColor.orangeColor()
private let kYellow = UIColor.yellowColor()
private let kGreen = UIColor.greenColor()
private let kBlue = UIColor.blueColor()
private let kViolet = UIColor.violetColor()
private let kGray = UIColor.grayColor()
private let kWhite = UIColor.whiteColor()
private let kGold = UIColor.goldColor()
private let kSilver = UIColor.silverColor()
private let kNone = UIColor.beigeColor()


class ResistorViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    // ================ VARIABLES =================
    var imagesDict: [String:[String:UIImage?]] = [:]
    var is4Band = true
    
    struct bands{
        //band colors
        static let numeric = [kBlack,kBrown,kRed,kOrange,kYellow,kGreen,kBlue,kViolet,kGray,kWhite]
        static let optional = [kBlack,kBrown,kRed,kOrange,kYellow,kGreen,kBlue,kViolet,kGray,kWhite,kGold,kSilver]
        static let multiplier = [kNone,kBlack,kBrown,kRed,kOrange,kYellow,kGreen,kBlue,kViolet,kGold,kSilver]
        static let tolerance = [kNone,kSilver,kGold,kRed,kBrown,kGreen,kBlue,kViolet,kGray]
        
        // tolerance associated with respective colours of tolerance band
        static let toleranceVal = [0.2,0.1,0.05,0.02,0.01,0.005,0.0025,0.001,0.0005]
        
        //resistor component order
        static let components = [numeric,numeric,optional,multiplier,tolerance]
    }
    
    // ================ IB OUTLETS =================
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var resistanceString: UILabel!

    @IBOutlet weak var resistorWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var resistorHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var resistorView: UIView!
    
    // ================ VIEW CONTROLLER =================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.calculateUpdate()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.updateResistorSize()
        //pickerView.reloadAllComponents()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        updateResistorSize()
    }
    
    // ================ PICKER VIEW DATA SOURCE =================

    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return bands.components.count
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 4 && is4Band{ return 5 }
        
        return bands.components[component].count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return ["left","middle","middle","middle","right"][component] 
    }
    
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return resistorHeightConstraint.constant
    }
    
    // create views for pickerView items from base images
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        // load image from assets
        let imageName = ["left","middle","middle","middle","right"][component] // index component image name from array
        
        let image: UIImage? = UIImage(named: imageName)
        
        let imageView = UIImageView(image: image)
        
        if imageName == "left" || imageName == "right" {
            imageView.roundToRadius(image!.size.height / 4.5)
            imageView.clipsToBounds = true
        }
        imageView.backgroundColor = bands.components[component][row]
        
        return imageView
    }
    
    // ================ PICKER VIEW DELEGATE =================
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.calculateUpdate()
    }
    
    // ================ METHODS =================
    
    
    func updateResistorSize() {
        // pickerview can only take specific heights, so must use a transform to scale it for universal sizes
        
        // get the unscaled pickerView width from the IB constraint, as well as the view width
        let pickerWidth = resistorWidthConstraint.constant
        let viewWidth = self.view.frame.width
        
        // scale pickerview to 80% of the view width, maintaining the aspect ratio
        let scaleFactor = viewWidth / pickerWidth
        resistorView.transform = CGAffineTransformMakeScale(scaleFactor, scaleFactor)
    }
    
    
    func calculateUpdate() {
        //find resisistance
        var resistance: Double
        
        let is4BandCalc = pickerView.selectedRowInComponent(3) == 0
        if is4BandCalc != self.is4Band {
            self.is4Band = is4BandCalc
            
            let prevRow = pickerView.selectedRowInComponent(4)
            pickerView.reloadComponent(4)
            
            if is4Band {
                if prevRow > 4 {
                    pickerView.selectRow(0, inComponent: 4, animated: true)
                }
            }
            
        }
        
        if is4BandCalc {
            //Four band resistor -> two digit value
            resistance = Double(pickerView.selectedRowInComponent(0)*10 + pickerView.selectedRowInComponent(1))/10 * pow(10,Double(pickerView.selectedRowInComponent(2)))
        } else {
            //Five band resistor -> three digit value
            resistance = Double(pickerView.selectedRowInComponent(0)*100 + pickerView.selectedRowInComponent(1)*10 + pickerView.selectedRowInComponent(2))/100 * pow(10,Double(pickerView.selectedRowInComponent(3)-1))
        }
        
        let tolerance = resistance * bands.toleranceVal[pickerView.selectedRowInComponent(4)]
        
        self.resistanceString.text = getStringForResistanceAndTolerance(resistance, tolerance: tolerance)
    }
    
    func getStringForResistanceAndTolerance(var resistance: Double, var tolerance: Double) -> String {
        
        var unit = ""
        
        if resistance > 0 { //
            
            let unitDict = [-1:"m", 0:"", 1:"k", 2:"M", 3:"G"]//unit prefix dictionary
            var power = log10(resistance)
        
            if power < -3 {
                power = -3
            }
            //find most appropriate unit (1-3sf before dp)
            let unitBand = floor(power/3)
            unit = unitDict[Int(unitBand)]!
            
            //scale values to appropriate unit
            resistance /= pow(10,unitBand*3)
            tolerance /= pow(10,unitBand*3)
            
            //round tolerance to 3sf
            let tolPower = (floor(log10(tolerance)))
            let tolFact = pow(10,tolPower-2)
            tolerance = round(tolerance/tolFact)*tolFact
        }
        
        // compile and return resistance string
        return "\(resistance) \(unit)Ω ± \(tolerance) \(unit)Ω"
    }
}


