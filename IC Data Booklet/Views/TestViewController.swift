//
//  TestViewController.swift
//  Temp
//
//  Created by Mike on 30/12/2014.
//  Copyright (c) 2014 Michael Jacoutot. All rights reserved.
//

import UIKit

class TestViewController: UIViewController {

    var testString = "Default"
    var steamData: [SteamData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelString.text = testString
        // Do any additional setup after loading the view.
        println(steamData)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var labelString: UILabel!

}
