//
//  SteamViewController.swift
//  Temp
//
//  Created by Mike on 12/11/14.
//  Copyright (c) 2014 Michael Jacoutot. All rights reserved.
//

import UIKit
import Alamofire
import Foundation


class SteamViewController: UIViewController, SteamTableDelegate {
    
    // ================ INITIALIZERS =================
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        let tablesData = SteamTables.sharedInstance()
        self.calculations = tablesData.getCalculationsArray()
        self.parameters = tablesData.getParametersDict()
        let defaultResult = self.compileBlankSteamResult()
        self.defaultSteamResult = defaultResult
        self.lastSteamResult = defaultResult
    }
    
    // ================ VARIABLES =================

    var steamTableController = SteamTableViewController()
    var steamGraphController = SteamGraphViewController()
    
    var defaultSteamResult: [Entity] = []
    var lastSteamResult: [Entity] = []
    var calculations: [[String]] = []
    var parameters: [String:Unit] = [:]
    
    var currentCalculationKeys: [String] {
        return calculations[pickerView.selectedRowInComponent(0)]
    }
    
    // ================ IB OUTLETS =================
    
    //@IBOutlet weak var solutionLabel: UILabel!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var getValuesButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var graphContainer: UIView!
    @IBOutlet weak var tableContainer: UIView!
    @IBOutlet weak var plotButton: UIButton!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    @IBOutlet weak var tableContainerHeightConstraint: NSLayoutConstraint!
    
    // ================ IB ACTIONS =================
    
    @IBAction func scrollViewTapped(sender: AnyObject) {
        // end editing to hide keyboard if the scrollview is tapped
        self.view.endEditing(true)
    }
    
    @IBAction func getValuesPressed(sender: AnyObject) {
        self.view.endEditing(true)
        configureButtons(true, calculating: true)
        let steamVars = getSteamVariables()
        
        //asynchronous network request
        connectionManager.getSteamResults(steamVars) { (success, steamData, error) in
            
            if success {
                self.lastSteamResult = steamData!
                self.steamTableController.onlyShowCellsForEntityKeys = nil // show all
                self.steamTableController.entities = self.lastSteamResult
                self.steamTableController.updateCellsForData(self.lastSteamResult)
                self.configureButtons(false, calculating: false)
            } else {
                if error != nil {
                    UIAlertView(title: "Could not contact the server", message: "You must be connected to the Imperial College wireless network or VPN service to access online capabilities", delegate: nil, cancelButtonTitle: "OK").show()
                } else {
                    UIAlertView(title: "Could not calculate steam parameters: Check input values.", message: nil, delegate: nil, cancelButtonTitle: "OK").show()
                }
                self.lastSteamResult = self.defaultSteamResult
                self.configureButtons(true, calculating: false)
            }
        }
    }
    
    @IBAction func savePress(sender: AnyObject) {
        self.view.endEditing(true)
        if lastSteamResult != defaultSteamResult {
            steamGraphController.graphController.savedDataPoints += [lastSteamResult]
        }
    }
    
    @IBAction func clearPress(sender: AnyObject) {
        self.view.endEditing(true)
        steamTableController.onlyShowCellsForEntityKeys = currentCalculationKeys
        configureButtons(true, calculating: false)
    }
    
    // ================ VIEW CONTROL =================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //solutionLabel.text = ""
        scrollView.scrollEnabled = true
        
        self.registerKeyboardNotifications()
        configureButtons(true, calculating: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // ================ KEYBOARD NOTIFICATIONS =================
    
    override func keyboardWasShown (notification: NSNotification) {
        let info : NSDictionary = notification.userInfo!
        let key = String(UIKeyboardFrameBeginUserInfoKey)
        
        if let keyboardHeight = info[key]?.CGRectValue.height {
            
            let insets: UIEdgeInsets = UIEdgeInsetsMake(self.scrollView.contentInset.top, 0, keyboardHeight, 0)
            
            self.scrollView.contentInset = insets
            self.scrollView.scrollIndicatorInsets = insets
        }
    }
    
    override func keyboardWillBeHidden (notification: NSNotification) {
        self.scrollView.contentInset = UIEdgeInsetsZero
        self.scrollView.scrollIndicatorInsets = UIEdgeInsetsZero
    }

    
    // ================ PICKER VIEW DATA SOURCE =================
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.calculations.count
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        
        var tView:UILabel
        if view == nil { tView = UILabel() } else { tView = view as! UILabel }
        
        tView.minimumScaleFactor = 0.5
        tView.textAlignment = NSTextAlignment.Center
        tView.textColor = UIColor.blackColor()
        
        // get row title from combining the unit category names of the parameters for the given calculation
        let calculation = self.calculations[row]
        var title = ""
        for param in calculation {
            if let unit = self.parameters[param] {
                if title != "" { title += " & " }
                title += unit.category.name
            }
        }
        
        tView.text = title
        
        return tView
    }
    
    // ================ PICKER VIEW DELEGATE =================
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        steamTableController.onlyShowCellsForEntityKeys = calculations[row]
    }
    
    // collects data from pickerview and input fields and returns data parameters formatted for the server request
    func getSteamVariables() -> [String:Double] {
        // get data types from active pickerView selection
        let row = pickerView.selectedRowInComponent(0) //read active row
        
        let calculationVars = calculations[row]
        
        var steamVars: [String:Double] = [:]
        for calcVar in calculationVars {
            if let cell = steamTableController.cellForSteamEntityKey(calcVar) {
                
                if let cellUnit = cell.entity?.unit {
                    let rawVal = (cell.valueField.text! as NSString).doubleValue
                    let convertedVal = cellUnit.convertValue(rawVal, toUnit: self.parameters[calcVar]!)
                    steamVars[calcVar] = convertedVal
                }
            }
        }
        return steamVars
    }
    
    // ================ STEAM TABLE DELEGATE =================
    
    func steamTable(steamTable: SteamTableViewController, willResize size: CGSize) {
        UIView.animateWithDuration(0.3, animations: {
            self.tableContainerHeightConstraint.constant = size.height
        })
    }
    
    //  ================ METHODS =================
    
    func compileBlankSteamResult() -> [Entity] {
        var blankResult: [Entity] = []
        for key in self.parameters.keys {
            let entity = Entity(unit: parameters[key]!, value: 0)
            entity.shouldDisplayValueAsNil = true
            entity.key = key
            blankResult += [entity]
        }
        return blankResult
    }
    
    func configureButtons(forInput: Bool, calculating: Bool) {
        getValuesButton.hidden = !forInput
        plotButton.hidden = forInput
        clearButton.hidden = forInput
        
        let title = calculating ? "Calculating":"Calculate"
        getValuesButton.setTitle(title, forState: .Normal)
        indicator.hidden = !calculating
        
        if calculating {
            indicator.startAnimating()
        } else if indicator.isAnimating() {
            indicator.stopAnimating()
        }
        
        steamTableController.updateCellsForEditting(forInput)
    }
    
    
    
    // ================ NAVIGATION =================
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "embedSteamTable" {
            steamTableController = segue.destinationViewController as! SteamTableViewController
            steamTableController.entities = lastSteamResult
            steamTableController.tableView.scrollEnabled = false
            
            steamTableController.onlyShowCellsForEntityKeys = calculations[¿0] ?? []
            
            steamTableController.delegate = self
        } else if segue.identifier == "embedSteamCharts" {
            steamGraphController = segue.destinationViewController as! SteamGraphViewController
        }
    }
    
    // ================ GESTURE RECOGNIZERS =================
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        self.view.endEditing(true)
    }
    
}

/*
// test steam value retrieval without internet connection
var blankResult: [Entity] = []
for key in self.parameters.keys {
var value: Double = 100
if key == "T" {
value = 500
} else if key == "s" {
value = 5
}
let entity = Entity(unit: parameters[key]!, value: value)
entity.shouldDisplayValueAsNil = true
entity.key = key
blankResult += [entity]
}

self.lastSteamResult = blankResult
self.steamTableController.hideCells.enabled = false
self.steamTableController.entities = self.lastSteamResult
self.steamTableController.updateCellsForData(self.lastSteamResult)
self.configureButtons(false, calculating: false)
*/
