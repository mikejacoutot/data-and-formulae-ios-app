//
//  TableViewCell.swift
//  Temp
//
//  Created by Mike on 07/02/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit

class SteamTableDataCell: UITableViewCell, UnitButtonDelegate {
    
    override func awakeFromNib() {
        if !IS_IPAD {
            let font = UIFont(name: nameLabel.font.fontName, size: 13)
            self.nameLabel.font = font
            self.valueField.font = font
            self.unitButton.titleLabel?.font = font
        }
    }
    
    // ================ VARIABLES =================
    
    //var canEditCell = true
    var entity: Entity! {
        didSet {
            if entity != nil {
                unitButton.configureForUnit(entity.unit, value: entity.value)
                
                if entity.shouldDisplayValueAsNil {
                    self.configureDataValue(forValue: nil)
                } else {
                    self.configureDataValue(forValue: entity.value)
                }
                
            }
        }
    }
    
    // ================ IB OUTLETS =================
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var valueField: UITextField!
    @IBOutlet weak var unitButton: UnitButton!
    
    // ================ CONFIGURATIONS =================
    
    func configureForEntity(entity: Entity) {
        self.entity = entity
        unitButton.delegate = self
        self.nameLabel.text = entity.unit.category.name
        
        self.clipsToBounds = true
    }
    
    func configureDataValue(forValue value: Double?) {
        if value != nil {
            self.valueField.text = "\(value!.roundTo(significantFigures: 5))"
        } else {
            self.valueField.text = ""
        }
    }
    
    func configureForEditting(canEdit: Bool) {
        valueField.userInteractionEnabled = canEdit
        valueField.borderStyle = canEdit ? .RoundedRect : .None
        valueField.textAlignment = canEdit ? .Left : .Right
        let pointSize = canEdit ? 14 : nameLabel.font.pointSize
        valueField.font = UIFont(name: valueField.font!.fontName, size: pointSize)
    }
    
    // ================ VARIABLES =================
    
    func unitButton(unitButton: UnitButton, didChangeToUnit unit: Unit, withValue: Double) {
        self.entity.convertToUnit(unit)
        self.configureDataValue(forValue: entity.value)
    }
}
