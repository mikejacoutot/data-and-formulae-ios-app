//
//  SteamGraphViewController.swift
//  Temp
//
//  Created by Mike on 03/01/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit
import Foundation

class SteamGraphViewController: UIViewController, SteamTableDelegate {
    
    // ================ VARIABLES =================
    
    var popOutTransitionOperator = PopOutTransitionOperator()
    var graphController: GraphPageViewController!

    // ================ VIEW CONTROL =================
    
    override func viewDidLoad() {
        self.graphController = GraphPageViewController(graphs: SteamTables.sharedInstance().graphs.allObjects as! [Graph])
        self.graphController.view.frame = self.view.bounds
        self.graphController.didMoveToParentViewController(self)
        self.addChildViewController(self.graphController)
        self.view.addSubview(self.graphController.view)
    }

}
