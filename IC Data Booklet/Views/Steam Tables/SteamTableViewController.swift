//
//  SteamTableViewController.swift
//  Temp
//
//  Created by Mike on 30/12/2014.
//  Copyright (c) 2014 Michael Jacoutot. All rights reserved.
//

import UIKit

@objc
protocol SteamTableDelegate {
    optional func steamTable(steamTable: SteamTableViewController, willResize size: CGSize)
}

// defined keys
private let kDataCellIdentifier = "dataCell"

enum SteamTableError: ErrorType {
    case IndexOutOfRange
    case NoEntityForKey(key: String)
}

class SteamTableViewController: UITableViewController, UnitConverterDataSource, UnitConverterDelegate {
    
    // ================ Variables =================
    
    let rowHeight: CGFloat = 40
    
    var entities: [Entity] = []
    var index = 0
    var delegate: SteamTableDelegate?
    var canEditCells = true
    
    var cellBeingConverted: SteamTableDataCell? = nil

    let popOutTransitionOperator = PopOutTransitionOperator()
    
    var onlyShowCellsForEntityKeys: [String]? {
        didSet {
            self.refreshTable()
            oldShownEntityKeys = onlyShowCellsForEntityKeys
        }
    }
    private var oldShownEntityKeys: [String]?
    
    // ================ VIEW CONTROLLER =================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.refreshTable()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refreshTable() {
        let n = tableView(tableView, numberOfRowsInSection: 0)
        self.preferredContentSize = CGSize(width: tableView.frame.width, height: CGFloat(n)*rowHeight)
        delegate?.steamTable?(self, willResize: self.preferredContentSize)
        
        self.tableView.beginUpdates()
        
        let entityKeysArray = entityKeys()
        let oldKeysArray = oldShownEntityKeys ?? entityKeysArray
        let newKeysArray = onlyShowCellsForEntityKeys ?? entityKeysArray
        
        let oldKeys = Set(oldKeysArray)
        let newKeys = Set(newKeysArray)
        
        let keysToAdd = newKeys - oldKeys
        let keysToRemove = oldKeys - newKeys
        
        var rowsToAdd: [NSIndexPath] = []
        var rowsToRemove: [NSIndexPath] = []
        //var rowsToMove: [(from: NSIndexPath,to: NSIndexPath)] = []*/
        
        var oldRowsManaged = 0 // number of rows that have been
        var newRowsManaged = 0
        
        for key in entityKeysArray {
            
            if keysToRemove.contains(key) {
                // remove an existing row
                let fromIndexPath = NSIndexPath(forRow: oldRowsManaged, inSection: 0)
                //tableView.deleteRowsAtIndexPaths([fromIndexPath], withRowAnimation: .Bottom)
                rowsToRemove += [fromIndexPath]
                oldRowsManaged++
            } else if keysToAdd.contains(key) {
                // add a new row
                let toIndexPath = NSIndexPath(forRow: newRowsManaged, inSection: 0)
                rowsToAdd += [toIndexPath] //tableView.insertRowsAtIndexPaths([toIndexPath], withRowAnimation: .Top)
                newRowsManaged++
            } else if newKeys.contains(key) {
                // move or keep an existing row
                
                /*if oldRowsManaged != newRowsManaged {
                    // row has changed so cell needs to move
                    let fromIndexPath = NSIndexPath(forRow: oldRowsManaged, inSection: 0)
                    let toIndexPath = NSIndexPath(forRow: newRowsManaged, inSection: 0)
                    rowsToMove += [(fromIndexPath,toIndexPath)]
                }*/
                oldRowsManaged++
                newRowsManaged++
            }
        }
        
        tableView.deleteRowsAtIndexPaths(rowsToRemove, withRowAnimation: .Bottom)
        tableView.insertRowsAtIndexPaths(rowsToAdd, withRowAnimation: .Top)
        /*for (fromPath,toPath) in rowsToMove {
            tableView.moveRowAtIndexPath(fromPath, toIndexPath: toPath)
        }*/
        
        
        
        
        self.tableView.endUpdates()
    }
    
    
    // ================ TABLE VIEW DATA SOURCE =================
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return onlyShowCellsForEntityKeys?.count ?? entities.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // guard against indexPath out of range (should never happen)
        let maxRow = onlyShowCellsForEntityKeys?.count ?? entities.count
        guard indexPath.row < maxRow else {
            NSLog("SteamTableViewController tableView indexPath out of range: \(indexPath.row)")
            return tableView.dequeueReusableCellWithIdentifier(kDataCellIdentifier, forIndexPath: indexPath)
        }
        
        //
        let cell = tableView.dequeueReusableCellWithIdentifier(kDataCellIdentifier, forIndexPath: indexPath) as! SteamTableDataCell
        let tag = indexPath.row
        cell.tag = tag
        cell.unitButton.tag = tag
        cell.valueField.tag = tag
        
        
        cell.configureForEditting(canEditCells)
        
        var entity: Entity!
        if let key = onlyShowCellsForEntityKeys?[¿tag] {
            entity = entityForKey(key)
            if entity == nil {
                NSLog("SteamTableViewController has no entity for the key: \(key)!")
                return cell
            }
        } else {
            entity = entities[tag]
        }
        cell.configureForEntity(entity)
        
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return rowHeight
    }
    
    func cellIsHidden(key: String) -> Bool {
        if !(onlyShowCellsForEntityKeys?.contains(key) ?? true) {
            return true
        }
        return false
    }
    
    func entityForKey(key: String) -> Entity? {
        if let i = entities.indexOf({ $0.key == key }) {
            return entities[i]
        }
        return nil
    }
    
    // ================ SCROLL VIEW DELEGATE =================
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        // a fix to undo the automatic scrolling when the keyboard appears. Needed where the table is embedded within a container in the SteamViewController
        if self.tableView.scrollEnabled == false {
            self.tableView.contentOffset = CGPointZero
        }
    }
    
    // ================ POPOVER CONVERTER DELEGATE =================
    
    func unitConverter(converter: UnitConverterViewController, didSelectUnit unit: Unit, value: Double, selectedRow row: Int) {
        if self.cellBeingConverted != nil {
            let entity = entities[cellBeingConverted!.tag]
            entity.convertToUnit(unit)
            cellBeingConverted!.configureForEntity(entity)
            
        }
    }
    
    func unitConverter(converter: UnitConverterViewController, didSwapToUnit unit: Unit, value: Double, selectedRow row: Int) {
        if cellBeingConverted != nil {
            self.entities[cellBeingConverted!.tag] = cellBeingConverted!.entity
            converter.dismissViewControllerAnimated(true, completion: { /*finished in self.popoverConverter = nil*/ } )
            self.cellBeingConverted = nil
        }
    }
    
    // ================ POPOVER CONVERTER DATA SOURCE =================
    
    func unitConverter(inputUnitForConverter converter: UnitConverterViewController) -> Unit? {
        return cellBeingConverted?.entity?.unit
    }
    
    func unitConverter(inputValueForConverter converter: UnitConverterViewController) -> Double {
        if let text = cellBeingConverted?.valueField.text {
            return NSString(string: text).doubleValue
        }
        return 0
    }
    
    // ================ METHODS =================
    
    func cellForSteamEntityKey(key: String) -> SteamTableDataCell? {
        var keys: [String] = []
        for entity in entities {
            keys += [entity.key]
        }
        
        if let cellRow = keys.indexOf(key) {
            if let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: cellRow, inSection: 0)) as? SteamTableDataCell {
                return cell
            }
        }
        return nil
    }
    
    func updateCellsForEditting(canEdit: Bool) {
        let n = tableView(tableView, numberOfRowsInSection: 0)
        for i in 0..<n {
            if let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0)) as? SteamTableDataCell {
                cell.configureForEditting(canEdit)
            }
        }
        if self.canEditCells != canEdit {
            canEditCells = canEdit
        }
    }
    
    func updateCellsForData(dataArray: [Entity]) {
        for entity in dataArray {
            if let cell = cellForSteamEntityKey(entity.key) {
                if let cellEnt = cell.entity {
                    entity.convertToUnit(cellEnt.unit)
                }
                entity.shouldDisplayValueAsNil = false
                cell.configureForEntity(entity)
            }
        }
    }
    
    func entityKeys() -> [String] {
        var keys: [String] = []
        for entity in entities {
            keys += [entity.key]
        }
        return keys
    }
    
    // ================ TEXT FIELD DELEGATE =================
    
    func textFieldShouldReturn(textField: UITextField) {
        textField.resignFirstResponder()
        self.moveToNextActiveTextField(textField, atIndex: textField.tag)
        
    }
    
    func moveToNextActiveTextField(sender: UITextField, atIndex senderIndex: Int){
        if senderIndex + 1 <= tableView.numberOfRowsInSection(0) {
            let nextCell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: senderIndex + 1, inSection: 0)) as! SteamTableDataCell
            nextCell.valueField.becomeFirstResponder()
        } else {
            sender.resignFirstResponder()
        }
    }
    
}