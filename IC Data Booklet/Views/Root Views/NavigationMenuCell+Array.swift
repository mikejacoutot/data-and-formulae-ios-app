//
//  NavigationMenuCell+Array.swift
//  Temp
//
//  Created by Mike on 04/03/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import UIKit


class NavigationTableCell: UITableViewCell {
    
    func configureNavTableCell(navTableData: NavigationTableArray) {
        self.imageView?.image = navTableData.icon //?.colorImage(UIColor.whiteColor())
        self.textLabel?.text = navTableData.label
    }
}


class NavigationTableArray {
    //Array object class - allows the array to be referenced with class methods
    let label: String
    var icon: UIImage?
    let segueId: String
    
    init(label: String, icon: UIImage?, segueId: String){
        self.label = label
        self.segueId = segueId
        self.setIcon(icon)
    }
    
    private func setIcon(icon: UIImage?) {
        if let img = icon {
            self.icon = img.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        }
    }
    
    class func allRows() -> [NavigationTableArray] {
        return [NavigationTableArray(label: "Formulae", icon: UIImage(named: "formulae.png"), segueId: "toFormulae"),
            NavigationTableArray(label: "Converter", icon: UIImage(named: "converter.png"), segueId: "toConverter"),
            NavigationTableArray(label: "Steam Tables", icon: UIImage(named: "tables.png"), segueId: "toTables"),
            NavigationTableArray(label: "Resistor", icon: UIImage(named: "resistor.png"), segueId: "toResistor"),
            NavigationTableArray(label: "Feedback", icon: UIImage(named: "feedback.png"), segueId: "toFeedback"),
            NavigationTableArray(label: "About", icon: UIImage(named: "about.png"), segueId: "toAbout")]
    }
}
