//
//  MenuCollectionViewController.swift
//  IC Data Booklet
//
//  Created by Mike on 04/10/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import UIKit

class MenuCollectionViewCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
}

class MenuCollectionViewController: UICollectionViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let minCellDimension: CGFloat = 100
    
    var navArray = NavTableArray.allRows()
    var currentSegueID = ""
    
    // ================ COLLECTION VIEW DATA SOURCE =================
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return navArray.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCellWithReuseIdentifier("menuCell", forIndexPath: indexPath) as? MenuCollectionViewCell, let navData = navArray[¿indexPath.row] {
            cell.titleLabel.text = navData.label
            cell.imageView.image = navData.icon
            cell.imageView.contentMode = .ScaleAspectFit
            return cell
        } else {
            NSLog("Unable to create collection view cell for the navigation menu at indexPath: \(indexPath)")
            return UICollectionViewCell()
        }
    }

    // ================ COLLECTION VIEW DELEGATE =================
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        println(navArray[indexPath.row].segueId)
    }
    
    // ================ COLLECTION VIEW FLOW LAYOUT DELEGATE =================
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    /*func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let totalWidth = self.view.frame.width
    }*/
    
}
