//
//  SignInViewController.swift
//  Test
//
//  Created by Mike on 17/12/2014.
//  Copyright (c) 2014 Mike. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController, UITextFieldDelegate {
    
    // ================= VARIABLES =================

    var isSigningIn = false
    
    // ================= IB OUTLETS =================
    
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var usernameView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signinActivity: UIActivityIndicatorView!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var savePasswordButton: UIButton!
    
    // ================= IB ACTIONS =================
    
    @IBAction func savePasswordPressed(sender: AnyObject) {
        // toggle save password
        configureForSavingPassword(!settingsManager.shouldSaveVPNPassword)
    }
    
    @IBAction func signInPressed(sender: AnyObject) {
        if isSigningIn {
            // TODO: CANCEL SIGN IN
        } else {
            attemptSignIn(nil)
        }
    }
    
    // ================= VIEW CONTROLLER =================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = backgroundColor
        
        self.usernameView.roundFullHeight()
        self.passwordView.roundFullHeight()
        self.buttonView.roundFullHeight()
        self.buttonView.backgroundColor = detailColor
        self.signinActivity.hidden = true
        
        // if the user has opted not to save the vpn password, remove the reference from the keychain
        if !settingsManager.shouldSaveVPNPassword {
            keychainManager.vpnPassword = nil
        }
        // configure buttons appropriately
        configureForSavingPassword(settingsManager.shouldSaveVPNPassword)
        configureForSigningIn(false)
        
        // attempt to load previous username and password
        var hasUsername = false
        var hasPassword = false
        if let username = keychainManager.vpnUsername {
            usernameTextField.text = username
            hasUsername = true
        }
        if let password = keychainManager.vpnPassword {
            passwordTextField.text = password
            hasPassword = true
        }
        
        // if both a username and password were already saved, attempt the sign-in automatically
        if hasUsername && hasPassword {
            attemptSignIn(nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureForSavingPassword(save: Bool) {
        // Function to configure the view for saving or not saving the password. Note that the password must always be temporarily saved in the keychain as the VPN framework requires a persistent reference.
        // If the user opts not to save the password, the data is removed before the next sign-in attempt or when the application is terminated
        
        // ensure variable is properly set
        settingsManager.shouldSaveVPNPassword = save
        
        // set button title
        let tickboxCharacter = save ? "✅":"❎" //"✔☑✅":"✖☒❎⬛"
        savePasswordButton.setTitle("Save password " + tickboxCharacter, forState: .Normal)
    }
    
    func configureForSigningIn(signingIn: Bool) {
        // ensure variable is properly set
        isSigningIn = signingIn
        
        // configure view items
        if signingIn {
            signinActivity.startAnimating()
            signinActivity.hidden = false
            signInButton.titleLabel?.text = "CANCEL"
        } else {
            signinActivity.stopAnimating()
            signinActivity.hidden = true
            signInButton.titleLabel?.text = "SIGN IN"
        }
    }
    
    // ================= SERVER COMMUNICATION =================
    
    func attemptSignIn(completion: (() -> Bool)?) {
        
        configureForSigningIn(true)
        
        // save username and password to be read from keychain
        keychainManager.vpnUsername = usernameTextField.text
        keychainManager.vpnPassword = passwordTextField.text
        
        connectionManager.configureVPN({
            (success,_,error) in
            if success {
                
                // NEED TO EXIT THE VIEW
                
            } else {
                // UIAlert called by the ConnectionManager, depending on the type of error. Should probably just return an error and call the UIAlert here.
                if !settingsManager.shouldSaveVPNPassword {
                    // NEED TO MAKE THIS ONLY WIPE (wether saved or not) IF LOGIN FAILED, NOT FOR ALL CONNECTION ISSUES
                    self.passwordTextField.text = ""
                }
                self.configureForSigningIn(false)
            }
        })
    }
    
    // ================= TEXT FIELD DELEGATE =================
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        // Hide keyboard when return button is pressed
        textField.resignFirstResponder()
    
        if textField == usernameTextField && passwordTextField.text!.isEmpty {
            self.passwordTextField.becomeFirstResponder()
        }
        
        if !(usernameTextField.text!.isEmpty || passwordTextField.text!.isEmpty) {
            // if both fields populated try to log in
            self.attemptSignIn(nil)
        }
        
        return true
    }

    
    // ================= NAVIGATION =================
    /*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // ================= GESTURE RECOGNIZERS =================
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        // Resign keyboards when screen clicked
        self.view.endEditing(true)
    }
}
