//
//  RootViewContainer.swift
//  IC Data Booklet
//
//  Created by Mike on 09/10/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation

class RootViewContainer: UIViewController, NavigationMenuViewControllerDelegate {
    
    // ================ VARIABLES =================
    
    var navController: UINavigationController?
    var menuController: NavigationMenuViewController?
    var currentNavigationItemIndexPath: NSIndexPath = NSIndexPath(forRow: 0, inSection: 0) // indexPath for the inital segue (toFormula)
    
    var slideOutTransitionManager = SlideOutTransitionManager(direction: .FromLeft, mode: IS_IPAD ? .Resize : .MoveOffscreen, destinationViewWidth: 275)
    var menuSegueEnterPanGesture: UIScreenEdgePanGestureRecognizer!
    var startup = true
    
    // ================ VIEW CONTROL =================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        menuSegueEnterPanGesture = UIScreenEdgePanGestureRecognizer(target: self, action: "handleEnterPan:")
        menuSegueEnterPanGesture.edges = .Left
        self.view.addGestureRecognizer(menuSegueEnterPanGesture)
    }
    
    override func viewDidAppear(animated: Bool) {
        if startup {
            self.showMenu()
            startup = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // ================ GENERAL FUNCTIONS =================
    
    func showNavigationMenu() {
        if menuController == nil {
            performSegueWithIdentifier("toMenu", sender: nil)
        } else {
            dismissNavigationMenu()
        }
    }
    
    func dismissNavigationMenu() {
        self.menuController?.dismissViewControllerAnimated(true, completion: nil)
        self.menuController = nil
    }
    
    // ================ MENU CONTROLLER DELEGATE =================
    
    func menuControllerWillAppear(menuController: NavigationMenuViewController) {
        menuController.tableView.selectRowAtIndexPath(currentNavigationItemIndexPath, animated: false, scrollPosition: UITableViewScrollPosition.None)
    }
    
    func menuControllerWillDismiss(menuController: NavigationMenuViewController) {
        self.menuController = nil
    }
    
    func menuController(menuController: NavigationMenuViewController, navigationItemSelectedAtIndexPath indexPath: NSIndexPath, forSegue segueIdentifier: String) {
        if indexPath != currentNavigationItemIndexPath {
            self.navController?.performSegueWithIdentifier(segueIdentifier, sender: nil)
            currentNavigationItemIndexPath = indexPath
            
            if !IS_IPAD {
                dismissNavigationMenu()
            }
        }
    }
    
    // ================ GESTURE RECOGNIZERS =================
    
    func handleEnterPan(gesture: UIPanGestureRecognizer) {
        
        switch (gesture.state) {
        case .Began:
            slideOutTransitionManager.interactive = true
            self.performSegueWithIdentifier("toMenu", sender: nil)
            break
            
        default:
            slideOutTransitionManager.handleEnterPan(gesture)
        }
    }
    
    // ================ NAVIGATION =================
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if let navVC = segue.destinationViewController as? UINavigationController {
            self.navController = navVC
        } else if let menuVC = segue.destinationViewController as? NavigationMenuViewController {
            menuVC.delegate = self
            self.menuController = menuVC
            
            if let slideOutSegue = segue as? SlideOutSegue {
                slideOutSegue.transitionManager = slideOutTransitionManager
            }
        }
    }
}

