//
//  NavigationMenuViewController.swift
//  Test
//
//  Created by Mike on 08/12/2014.
//  Copyright (c) 2014 Mike. All rights reserved.
//

import UIKit

protocol NavigationMenuViewControllerDelegate {
    func menuController(menuController: NavigationMenuViewController, navigationItemSelectedAtIndexPath indexPath: NSIndexPath, forSegue segueIdentifier: String)
    func menuControllerWillAppear(menuController: NavigationMenuViewController)
    func menuControllerWillDismiss(menuController: NavigationMenuViewController)
}

class NavigationMenuViewController: UITableViewController {
    
    // ================ VARIABLES =================
    
    var tableViewHeight: CGFloat!
    var delegate: NavigationMenuViewControllerDelegate?
    var navArray = NavigationTableArray.allRows()//from NavTableArray class at bottom
    
    override func viewWillAppear(animated: Bool) {
        delegate?.menuControllerWillAppear(self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.clipsToBounds = true
    }
    
    override func viewDidDisappear(animated: Bool) {
        if isBeingDismissed() {
            delegate?.menuControllerWillDismiss(self)
        }
    }
    
    // ================ TABLE VIEW DATA SOURCE =================
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return navArray.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("NavTableCell", forIndexPath: indexPath) as! NavigationTableCell
        cell.configureNavTableCell(navArray[indexPath.row])
        return cell
    }
    
    /*override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }*/
    
    // ================ TABLE VIEW DELEGATE =================
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor.clearColor()
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        delegate?.menuController(self, navigationItemSelectedAtIndexPath: indexPath, forSegue: navArray[indexPath.row].segueId)
    }
    
}
