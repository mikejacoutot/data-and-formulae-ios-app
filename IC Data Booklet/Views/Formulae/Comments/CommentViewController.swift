//
//  ViewController.swift
//  Forum
//
//  Created by Mike on 10/03/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit
import Foundation

var comments = [Comment(user: "First user", date: NSDate(timeIntervalSinceNow: NSTimeInterval(-300)), votes: 12, comment: "A short comment from one user"),
    Comment(user: "Second user", date: NSDate(timeIntervalSinceNow: NSTimeInterval(-30000)), votes: 6, comment: "This is another comment from another user, slightly longer, taking up more rows. It spans two lines, slightly more than before."),
    Comment(user: "Third user", date: NSDate(timeIntervalSinceNow: NSTimeInterval(-300000)), votes: 125, comment: "This is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space. This is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space. This is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space. This is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space.\n \nThis is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space. This is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space. This is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space. This is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space. This is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space. "),
    Comment(user: "Third user", date: NSDate(timeIntervalSinceNow: NSTimeInterval(-300000)), votes: 125, comment: "This is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space. This is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space. This is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space. This is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space.\n \nThis is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space. This is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space. This is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space. This is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space. This is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space. "),
    Comment(user: "Third user", date: NSDate(timeIntervalSinceNow: NSTimeInterval(-300000)), votes: 125, comment: "This is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space. This is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space. This is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space. This is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space.\n \nThis is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space. This is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space. This is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space. This is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space. This is a much longer comment, extremely long in fact. It spans multiple lines and needs to be limited so that it does not take up too much space. ")]

let userName = "My Username"

//var myAddedComments: [Comment] = []

class CommentViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, CommentCellDelegate {
    
    var test = false
    
    @IBOutlet weak var submitContainer: UIView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var commentView: UITextView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var commentViewHeight: NSLayoutConstraint!
    
    @IBAction func viewTapped(sender: AnyObject) {
        self.view.endEditing(true)
    }
    
    @IBAction func submitPressed(sender: AnyObject) {
        if commentView.text.countNonBlank() >= 10 {
            let comment = Comment(user: userName, date: NSDate(), votes: 0, comment: commentView.text)
            
            let indexPath = NSIndexPath(forRow: comments.count, inSection: 0)
            comments += [comment]
            tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Top)
            
            commentView.text = ""
            fitTextViewSize(commentView)
            self.view.endEditing(true)
            
        } else {
            UIAlertView(title: "Comment is too short", message: nil, delegate: nil, cancelButtonTitle: "OK").show()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.submitContainer.layer.cornerRadius = 8
        self.commentView.layer.cornerRadius = 8
        tableView.reloadData()
        
        let textHeight = commentView.font!.lineHeight * 2
        self.commentViewHeight.constant = textHeight
        self.submitContainer.frame.size.height = textHeight
        
        if test {
            UIAlertView(title: "test", message: "worked", delegate: nil, cancelButtonTitle: nil).show()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CommentCell") as! CommentCell
        cell.configureForComment(comments[indexPath.row])
        cell.contentView.layer
        cell.tag = indexPath.row
        cell.delegate = self
        return cell
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let comment = comments[indexPath.row]
        let h = CommentCell.estimatedHeightForComment(comment.comment)
        return h
    }

func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
let comment = comments[indexPath.row]
        let h = CommentCell.estimatedHeightForComment(comment.comment)
        return h
}
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    

    func textViewDidChange(textView: UITextView) {
        fitTextViewSize(textView)
    }
    
    func fitTextViewSize(textView: UITextView) {
        let h = textView.sizeThatFits(CGSize(width: textView.frame.width, height: CGFloat.max)).height
        
        UIView.animateWithDuration(0.2, animations: {
            self.commentViewHeight.constant = h
            print(textView.font!.lineHeight)
            self.view.layoutIfNeeded()
            self.commentView.contentOffset = CGPointZero
        })
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        fitTextViewSize(commentView)
    }
    
    
    func commentCell(commentcell: CommentCell, didChangeVote fromVote: Bool?, toVote: Bool?) {
        let index = commentcell.tag
        let comment = comments[index]
        
        let change: Int = {
            if fromVote == nil {
                if toVote == true {
                    return 1
                } else if toVote == false {
                    return -1
                }
            } else if fromVote == true {
                if toVote == nil {
                    return -1
                } else if toVote == false {
                    return -2
                }
            } else if fromVote == false {
                if toVote == nil {
                    return 1
                } else if toVote == true {
                    return 2
                }
            }
            return 0
            }()
        
        if change != 0 {
            comment.votes += change
            comments[index] = comment
        }
        
    }
}




