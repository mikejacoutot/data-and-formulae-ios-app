//
//  ComentTableViewCell.swift
//  Forum
//
//  Created by Mike on 11/03/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit

private let minStartHeight: CGFloat = 115
private let maxStartHeight: CGFloat = 400

protocol CommentCellDelegate {
    func commentCell(commentcell: CommentCell, didChangeVote fromVote: Bool?, toVote: Bool?)
}

class CommentCell: UITableViewCell {
    
    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var upvoteButton: UIButton!
    @IBOutlet weak var downvoteButton: UIButton!
    @IBOutlet weak var commentView: UITextView!
    @IBOutlet weak var showMoreButtonContainer: UIView!
    
    @IBOutlet weak var commentLabelHeightConstraint: NSLayoutConstraint!
    
    var initialMaxLabelHeight: CGFloat!
    @IBAction func showMorePressed(sender: AnyObject) {
        let numberOfTimesShown = round(commentLabelHeightConstraint.constant / initialMaxLabelHeight)
        let newMaxHeight = (numberOfTimesShown + 1) * initialMaxLabelHeight
        
        let expectedLabelHeight = sizeThatFits(CGSize(width: commentView.frame.width, height: CGFloat.max)).height
        UIView.animateWithDuration(0.3, animations: {
            self.commentLabelHeightConstraint.constant = newMaxHeight
            self.layoutIfNeeded()
            
            if expectedLabelHeight < newMaxHeight {
                self.showMoreButtonContainer.hidden = true
            }
        })
    }
    
    
    @IBAction func upvotePressed(sender: AnyObject) {
        upvoteButton.enabled = false
        downvoteButton.enabled = true
        upvoteButton.setTitleColor(UIColor.greenColor(), forState: UIControlState.Normal)
        downvoteButton.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Normal)
        
        if savedVote != true {
            delegate?.commentCell(self, didChangeVote: savedVote, toVote: true)
            vote(true)
        }
    }
    
    @IBAction func downvotePressed(sender: AnyObject) {
        downvoteButton.enabled = false
        upvoteButton.enabled = true
        upvoteButton.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Normal)
        downvoteButton.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
        
        
        if savedVote != false {
            delegate?.commentCell(self, didChangeVote: savedVote, toVote: false)
            vote(true)
        }
        vote(false)
    }
    
    var delegate: CommentCellDelegate?
    
    var comment: Comment!
    private var votes: Int = 0 {
        didSet {
            self.scoreLabel.text = "\(votes)"
        }
    }
    private var savedVote: Bool?
    
    func vote(up: Bool) {
        if let currentVote = savedVote {
            if up != currentVote {
                savedVote = up
                if up {
                    votes += 2
                } else {
                    votes -= 2
                }
            }
        } else {
            savedVote = up
            if up {
                votes++
            } else {
                votes--
            }
        }
    }
    
    func configureForComment(comment: Comment) {
        
        self.comment = comment
        self.commentView.text = comment.comment
        commentView.reloadInputViews()
        self.votes = comment.votes
        self.userLabel.text = comment.user
        self.dateLabel.text = comment.date.formatDateString()
        //formatter.dateFormat
        
        commentView.layoutIfNeeded()
        let expectedLabelHeight = commentView.frame.height //sizeThatFits(CGSize(width: commentLabel.frame.width, height: CGFloat.max)).height
        if expectedLabelHeight < commentLabelHeightConstraint.constant {
            self.showMoreButtonContainer.hidden = false //true
            //self.commentLabel.frame.size.height = expectedLabelHeight
        } else {
            self.showMoreButtonContainer.hidden = false
            //self.commentLabel.frame.size.height = commentLabelHeightConstraint.constant
        }
        
    }
    
    
    
    override func awakeFromNib() {
        let upTrans = CGAffineTransformMakeTranslation(3, 0)
        upvoteButton.transform = CGAffineTransformRotate(upTrans, CGFloat(-M_PI_2))
        let downTrans = CGAffineTransformMakeTranslation(0, 0)
        downvoteButton.transform = CGAffineTransformRotate(downTrans, CGFloat(M_PI_2))
        
        self.userLabel.font = UIFont.boldSystemFontOfSize(userLabel.font.pointSize)
        
        self.layer.borderWidth = 5
        self.layer.borderColor = UIColor.clearColor().CGColor
        
        self.initialMaxLabelHeight = self.commentLabelHeightConstraint.constant
    }
    
    class func estimatedHeightForComment(comment: String) -> CGFloat {
        
        if let width = UIApplication.sharedApplication().keyWindow?.frame.width {
            
            let labelWidth = width - 105
            let font = UIFont.systemFontOfSize(UIFont.systemFontSize())
            
            let textHeight = comment.getHeight(forWidth: labelWidth, usingFont: font)
            let minHeight = max(textHeight + 60, minStartHeight)
            return min(maxStartHeight, minHeight)
        }
        return minStartHeight
    }
}






extension String {
    func getHeight(forWidth width: CGFloat, usingFont font: UIFont) -> CGFloat {
        let size = CGSize(width: width, height: CGFloat.max)
        return NSString(string: self).boundingRectWithSize(size, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName:font], context: NSStringDrawingContext()).height
    }
    
    var length: Int { return self.characters.count }
    
    func countNonBlank() -> Int {
        let whitespace = NSCharacterSet.whitespaceAndNewlineCharacterSet()
        return self.stringByTrimmingCharactersInSet(whitespace).length
    }

}




class Comment: NSObject {
    
    init(user: String, date: NSDate, votes: Int, comment: String) {
        self.user = user
        self.date = date
        self.votes = votes
        self.comment = comment
    }
    
    let user: String
    let date: NSDate
    var votes: Int
    var comment: String
}


extension NSDate {
    func formatDateString() -> String {
        let date = self
        
        let duration = NSDate().timeIntervalSinceDate(date)
        let (days, hours, minutes) = (duration/86400,duration/3600,duration/60)
        
        if hours < 1 {
            let m = Int(floor(minutes))
            if m == 0 {
                return "<1m"
            } else {
                return "\(m)m"
            }
        } else if days < 1 {
            let h = Int(floor(hours))
            return "\(h)h"
        } else if days < 7 {
            let d = Int(floor(days))
            return "\(d)d"
        } else {
            let formatter = NSDateFormatter()
            formatter.dateStyle = NSDateFormatterStyle.ShortStyle
            formatter.dateFormat = "dd/MM/yy"
            return formatter.stringFromDate(date)
        }
    }
}










