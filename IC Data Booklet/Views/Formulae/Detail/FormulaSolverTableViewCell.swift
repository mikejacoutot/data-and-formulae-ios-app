//
//  FormulaSolverTableViewCell.swift
//  Temp
//
//  Created by Mike on 16/01/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit

class FormulaSolveCell: UITableViewCell {

    @IBOutlet weak var solveButton: UIButton!
    @IBOutlet weak var clearButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}

class FormulaVariableCell: UITableViewCell, UnitButtonDelegate {
    
    var entity: Entity!
    
    @IBOutlet weak var variableLabel: UILabel!
    @IBOutlet weak var realField: UITextField!
    @IBOutlet weak var imaginaryField: UITextField!
    @IBOutlet weak var plusLabel: UILabel!
    @IBOutlet weak var iLabel: UILabel!
    @IBOutlet weak var unitButton: UnitButton!
    
    @IBOutlet weak var setButton: UIButton!
    
    @IBOutlet weak var textFieldContainer: UIView!
    @IBOutlet weak var textFieldContainerTrailingConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configureForEntity(entity: Entity) {
        
        self.entity = entity
        unitButton.unit = entity.unit
        unitButton.value = entity.value
        unitButton.delegate = self
        
        variableLabel.attributedText = entity.getNameString(forFont: variableLabel.font)

        // if the cell only accepts real values, hide the redundant subviews and resize
        if !entity.isImaginary {
            imaginaryField.hidden = true
            iLabel.hidden = true
            plusLabel.hidden = true
            
            // reconstrain container trailing edge to the realField trailing edge to re-center the container for real values only
            textFieldContainer.removeConstraint(textFieldContainerTrailingConstraint)
            textFieldContainerTrailingConstraint = NSLayoutConstraint(item: realField, attribute: .Trailing, relatedBy: .Equal, toItem: textFieldContainer, attribute: .Trailing, multiplier: 1, constant: 0)
            textFieldContainer.addConstraint(textFieldContainerTrailingConstraint)
        }
        
        if entity.symbol?.isConstant.boolValue ?? false {
            setButton.hidden = true
            setButton.enabled = false
            imaginaryField.enabled = false
            realField.enabled = false
            realField.text = entity.symbol!.constant.doubleValue.stringValue()
        }
    }
    
    func unitButton(unitButton: UnitButton, didChangeToUnit unit: Unit, withValue: Double) {
        self.entity.convertToUnit(unit)
        self.configureForEntity(self.entity)
    }
}
