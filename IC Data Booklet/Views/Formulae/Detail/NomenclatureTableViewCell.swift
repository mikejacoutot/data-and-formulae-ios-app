//
//  NomenclatureTableViewCell.swift
//  Temp
//
//  Created by Mike on 01/04/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit

class NomenclatureTableViewCell: UITableViewCell {
    
    @IBOutlet weak var symbolLabel: UILabel!
    @IBOutlet weak var hyphenLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var equalsLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    func configureForSymbol(symbol: Symbol) {
        symbolLabel.attributedText = symbol.display.parseSuperAndSubscript(symbolLabel.font)
        
        if !symbol.name.isWhiteSpace() {
            nameLabel.text = symbol.name
            nameLabel.sizeToFit()
            nameLabel.hidden = false
            hyphenLabel.hidden = false
        } else {
            nameLabel.text = ""
            nameLabel.hidden = true
            nameLabel.frame.size.width = 0
            hyphenLabel.hidden = true
        }
        
        if symbol.isConstant as Bool {
            var valueString = symbol.constant.doubleValue.attributedStringWithExponentBase10(significantFigures: 5, forFont: valueLabel.font)
            if let unit = symbol.unit {
                valueString += " "
                valueString += unit.getDisplayString(forFont: valueLabel.font, appendFullName: false, plural: symbol.constant.doubleValue != 1)
            }
            equalsLabel.hidden = false
            valueLabel.hidden = false
        } else {
            equalsLabel.hidden = true
            valueLabel.hidden = true
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
