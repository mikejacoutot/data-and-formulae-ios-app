//
//  FormulaSolverTableViewController.swift
//  Test
//
//  Created by Mike on 16/12/2014.
//  Copyright (c) 2014 Mike. All rights reserved.
//

import UIKit

@objc
protocol FormulaSolverDelegate {
    optional func formulaSolverTableDidResize(size: CGSize)
}


class FormulaSolverTableViewController: UITableViewController, UITextFieldDelegate {
    
    // ================ VARIABLES =================
    
    var shouldScroll = true
    
    var currentSolveIndex = -1
    var entities: [Entity] = []
    var formula: Formula!
    var delegate: FormulaSolverDelegate?
    
    var cellBeingConverted: FormulaVariableCell?
    var solveButton: UIButton!
    let popOutTransitionOperator = PopOutTransitionOperator()
    
    // ================ IB ACTIONS =================
    
    @IBAction func textFieldDidChange(sender: UITextField!) {
        let even = sender.tag % 2 == 0
        let row = sender.tag / 2
        
        let value = NSString(string: sender.text!).doubleValue
        let entity = entities[row]
        
        if even {
            entity.value = value
        } else {
            entity.imaginaryValue = value
        }
        
        if currentSolveIndex < 0 {
            var blankIndices: [Int] = []
            for i in 0..<entities.count {
                let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0)) as! FormulaVariableCell
                
                let hasReal = cell.realField.hasText()
                let hasImag = cell.imaginaryField.hasText()
                
                if !hasReal && !hasImag {
                    blankIndices += [i]
                }
            }
            
            if blankIndices.count == 1 {
                setEntityToSolve(blankIndices[0])
            }
        }
    }
    
    @IBAction func solveClicked() {
        self.solveEquation()
    }
    
    @IBAction func clearClicked() {
        for i in 0..<entities.count {
            let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0)) as! FormulaVariableCell
            
            cell.realField.text = ""
            cell.imaginaryField.text = ""
        }
        setEntityToSolve(-1)
    }
    
    @IBAction func setUnknown(sender: UIButton!) {
        setEntityToSolve(sender.tag)
    }
    
    func setEntityToSolve(index: Int) {
        var solveFound = false
        
        for i in 0..<entities.count {
            let entity = entities[i]
            let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0)) as! FormulaVariableCell
            if i == index {
                cell.setButton.setTitle("", forState: .Normal)
                entity.shouldSolve = true
        
                var solveTitle = NSMutableAttributedString(string: "Solve for ")
                solveTitle += entity.getNameString(forFont: solveButton.titleLabel?.font, short: true)
                solveButton.setAttributedTitle(solveTitle, forState: .Normal)
                
                solveFound = true
            } else {
                cell.setButton.setTitle("Set Unknown", forState: .Normal)
                entity.shouldSolve = false
            }
        }
        currentSolveIndex = index
        if !solveFound {
            //solveButton.setTitle("Solve", forState: .Normal)
            solveButton.setAttributedTitle(NSAttributedString(string: "Solve"), forState: .Normal)
        }
    }

    
    // ================ VIEW CONTROLLER =================

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        delegate?.formulaSolverTableDidResize?(self.tableView.contentSize)
    }
    
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        delegate?.formulaSolverTableDidResize?(self.tableView.contentSize)
    }
    
    // ================ TABLE VIEW DATA SOURCE =================

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return entities.count + 1
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // configure variable cell
        let row = indexPath.row
        if row < entities.count {
            let cell = tableView.dequeueReusableCellWithIdentifier("VariableCell", forIndexPath: indexPath) as! FormulaVariableCell
            let entity = entities[row]
            cell.configureForEntity(entity)
            
            cell.realField.tag = row * 2
            cell.imaginaryField.tag = (row * 2) + 1
            cell.unitButton.tag = row
            cell.setButton.tag = row
            cell.tag = row
            
            return cell
            
        // configure footer
        } else if indexPath.row == entities.count {
            let cell = tableView.dequeueReusableCellWithIdentifier("SolveCell", forIndexPath: indexPath) as! FormulaSolveCell
            self.solveButton = cell.solveButton
            return cell
        }

        print("returning blank cell")
        return UITableViewCell()
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }

    // ================ TEXT FIELD DELEGATE =================
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        var result = true
        
        if string.characters.count > 0 {
            let disallowedCharacterSet = NSCharacterSet(charactersInString: "0123456789.eE-").invertedSet
            let replacementStringIsLegal = string.rangeOfCharacterFromSet(disallowedCharacterSet) == nil
            result = replacementStringIsLegal
        }
        return result
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {

        let nextField = self.getNextActiveTextField(textField.tag)
        shouldScroll = nextField == nil
        textField.resignFirstResponder()
        nextField?.becomeFirstResponder()
        shouldScroll = true
        
        return true
    }
    
    func getNextActiveTextField(senderIndex: Int) -> UITextField? {
        
        let nextIndex = senderIndex + 1
        let maxIndex = (entities.count * 2) - 1
        if nextIndex <= maxIndex {
            // get corresponding cell for textField index
            let tableRow = nextIndex/2
            let nextCell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: tableRow, inSection: 0)) as! FormulaVariableCell
            if nextIndex % 2 == 0 {
                return nextCell.realField
            } else {
                if nextCell.imaginaryField.hidden == false {
                    return nextCell.imaginaryField
                } else if nextIndex + 1 < maxIndex {
                    return (tableView.cellForRowAtIndexPath(NSIndexPath(forRow: tableRow + 1, inSection: 0)) as! FormulaVariableCell).realField
                }
            }
        }
        return nil
    }
    
    // ================ SERVER COMMUNICATION =================
    
    func solveEquation() {
        var variables: [String:AnyObject] = [:]
        var locked = false
        for entity in entities {
            if entity.shouldSolve {
                locked = true
                variables[entity.symbol!.sympy] = NSNull()
            } else {
                variables[entity.symbol!.sympy] = entity.sympyValue()
            }
        }
        
        if !locked {
            UIAlertView(title: "Select a value to solve for", message: nil, delegate: nil, cancelButtonTitle: "OK").show()
            return
        }
        
        variables["formulaID"] = formula.pk
        
        connectionManager.solveEquation(variables, completion: { (success, solutions, error) in
            
            if error != nil {
                UIAlertView(title: "Could not contact the server", message: "You must be connected to the Imperial College wireless network or VPN service to access online capabilities", delegate: nil, cancelButtonTitle: "OK").show()
                return
            } else if !success || solutions == nil || solutions?.count == 0 {
                UIAlertView(title: "No solutions found", message: nil, delegate: nil, cancelButtonTitle: "OK").show()
                return
            } else {
                if let first = solutions?.filter({ $0.index == 0 })[¿0] {
                    
                    for entity in self.entities {
                        if entity.symbol?.sympy == first.vars {
                            if entity.unit == nil {
                                entity.value = first.real
                                entity.imaginaryValue = first.imag
                            } else {
                                entity.value = entity.unit!.convertValue(valueFromBaseUnit: first.real)
                                entity.imaginaryValue = entity.unit!.convertValue(valueFromBaseUnit: first.imag)
                            }
                        }
                    }
                    
                    self.tableView.reloadData()
                    
                    // DO SOMETHING WITH THE OTHER SOLUTIONS
                    //let otherSolutions = solutions!.filter({ $0.index != 0 })
                } else {
                    UIAlertView(title: "No solutions found", message: nil, delegate: nil, cancelButtonTitle: "OK").show()
                    return
                }
            }
        })
    }

}
