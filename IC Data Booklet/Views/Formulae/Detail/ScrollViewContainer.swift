//
//  ScrollViewContainer.swift
//  Temp
//
//  Created by Mike on 07/03/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//
//  Container to forward events for touches outside of the scrollView frame but within the scope of the contentView
//

import UIKit


class ScrollViewContainer: UIView {
    
    var enabled = true
    
    @IBOutlet var scrollView: UIScrollView!
    
    override func hitTest(point: CGPoint, withEvent event: UIEvent!) -> UIView? {
        
        let view = super.hitTest(point, withEvent: event)
        
        if enabled {
            if let theView = view {
                if theView == self {
                    return scrollView
                }
            }
        }
        
        return view
    }
    
}
