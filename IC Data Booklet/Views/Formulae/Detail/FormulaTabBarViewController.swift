//
//  FormulaTabBarViewController.swift
//  Temp
//
//  Created by Mike on 19/03/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit

class FormulaTabBarViewController: UIViewController {

    // ================ VARIABLES =================
    
    var formula: Formula!
    var popTransitionManager = PopOutTransitionOperator()
    
    // ================ IB OUTLETS =================
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var segmentedControlContainer: UIView!
    @IBOutlet weak var segmentedControlContainerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var formulaContainer: UIView!
    @IBOutlet weak var graphContainer: UIView!
    
    @IBOutlet weak var legalButton: UIBarButtonItem!
    
    // ================ IB ACTIONS =================
    
    @IBAction func segmentPressed(sender: UISegmentedControl!) {
        switch sender.selectedSegmentIndex {
        case 0:
            formulaContainer.hidden = false
            graphContainer.hidden = true
            break
        case 1:
            formulaContainer.hidden = true
            graphContainer.hidden = false
            break
        default: ()
        }
    }
    
    @IBAction func legalPressed(sender: AnyObject) {
        if IS_IPAD {
            self.performSegueWithIdentifier("popoverLegal", sender: sender)
        } else {
            self.performSegueWithIdentifier("showLegal", sender: sender)
        }
    }

    // ================ VIEW CONTROL =================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if formula != nil {
            self.title = formula.name
            
            // configure legal button
            if self.formula.legal.isWhiteSpace() {
                self.legalButton.title = ""
                self.legalButton.enabled = false
            }
            
            let imageCount = formula.images.count
            let graphs = formula.graphs.allObjects as! [Graph]
            
            if graphs.count == 0 {
                hideControl()
                self.segmentedControl.selectedSegmentIndex = 0
            } else if imageCount == 0 {
                hideControl()
                self.segmentedControl.selectedSegmentIndex = 1
            } else {
                showControl()
                self.segmentedControl.selectedSegmentIndex = 0
            }
            
            segmentPressed(segmentedControl)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // ================ METHODS =================
    
    func hideControl() {
        self.segmentedControlContainer.hidden = true
        self.segmentedControlContainerHeightConstraint.constant = 0
    }
    
    func showControl() {
        self.segmentedControlContainer.hidden = false
        self.segmentedControlContainerHeightConstraint.constant = 50
    }
    
    // ================ NAVIGATION =================

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "embedFormulae" {
            let vc = segue.destinationViewController as! FormulaDetailViewController
            vc.formula = self.formula
        } else if segue.identifier == "embedGraphs" {
            let vc = segue.destinationViewController as! GraphPageViewController
            vc.graphs = formula?.graphs.allObjects as? [Graph] ?? []
        } else if segue.identifier == "popoverLegal" || segue.identifier == "showLegal" {
            let vc = segue.destinationViewController as! LegalPopoverViewController
            vc.legal = formula!.legal
            
            if segue.identifier == "showLegal" {
                vc.transitioningDelegate = popTransitionManager
            }
        }
    }
}
