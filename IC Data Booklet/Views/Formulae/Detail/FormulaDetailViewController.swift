//
//  FormulaDetailViewController.swift
//  Temp
//
//  Created by Mike on 11/01/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit

class FormulaDetailViewController: UIViewController, UIScrollViewDelegate, FormulaSolverDelegate, TitleScrollViewDelegate, DAFViewDelegate {
    
    // ================ VARIABLES =================
    
    var formula: Formula!
    var images: [Image] = []
    var pageViews: [UIImageView?] = []
    var titles: [NSMutableAttributedString] = []

    private var pageBeforeRotation = 0
    var currentPage: Int = 0
    
    private var shouldLayoutForRotation = false
    
    var equationPageViewController: UIPageViewController!
    var solverTableViewController: FormulaSolverTableViewController?
    
    var nomenclatureRowHeight: CGFloat = 20
    var hasSummary = true
    var hasNomenclature = true
    
    var defaultMinScrollViewOffset: CGFloat = -40
    var defaultMaxScrollViewOffset: CGFloat = -100
    var keyboardHeight: CGFloat = 0
    
    // ================ IB OUTLETS =================
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewContentView: UIView!
    
    @IBOutlet weak var titleScrollView: TitleScrollView!
    @IBOutlet weak var equationContainer: ScrollViewContainer!
    @IBOutlet weak var equationScrollView: UIScrollView!
    @IBOutlet weak var equationScrollViewOffsetConstraint: NSLayoutConstraint!
    @IBOutlet weak var equationPageControl: UIPageControl!
    
    @IBOutlet weak var showMoreButton: UIButton!
    
    @IBOutlet weak var summaryTitleLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var nomenclatureTitleLabel: UILabel!
    @IBOutlet weak var nomenclatureTableView: UITableView!
    @IBOutlet weak var nomenclatureTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var nomenclatureTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var solverContainer: UIView!
    @IBOutlet weak var solverContainerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var commentsContainer: UIView!
    
    
    // ================ IB ACTIONS =================
    
    @IBAction func scrollViewTapped(sender: AnyObject) {
        self.view.endEditing(true)
    }
    
    @IBAction func showMorePressed(sender: AnyObject) {
        UIView.animateWithDuration(0.3, animations: {
            if self.showMoreButton.titleLabel?.text == "+" {
                self.scrollView.contentOffset = CGPoint(x: 0, y: -self.scrollView.contentInset.top)
            } else if self.showMoreButton.titleLabel?.text == "-" {
                if self.hasSummary || self.hasNomenclature {
                    self.scrollView.contentOffset = CGPoint(x: 0, y: -40)
                }
            }
        })
    }
    
    // ================ VIEW CONTROL =================
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        if self.formula != nil {
            
            if let dafView = self.view as? DAFView {
                dafView.delegate = self
            } else {
                NSLog("FormulaDetailViewController's view is not a DAFView!")
            }
            
            commentsContainer.roundToRadius(5)
            
            self.title = formula.name
            
            // class extension function to set up notifications & methods for keyboard appearance and dismissal
            self.registerKeyboardNotifications()
            
            // get ordered array of Image class images from the formula.images relation
            self.images = (self.formula.images.allObjects as! [Image]).sort({
                $0.main.boolValue == true && $1.main.boolValue == false
            })
            self.equationPageControl.numberOfPages = images.count
            
            // Set up the array to hold the views for each page
            for _ in 0..<images.count {
                pageViews.append(nil)
            }
            
            // set up titleScrollView
            for image in images {
                self.titles += [image.name.parseSuperAndSubscript(UIFont.systemFont())]
            }
            self.titleScrollView.attributedTitles = self.titles
            titleScrollView.titleDelegate = self

            // configure nomenclature table
            self.nomenclatureTableViewHeightConstraint.constant = nomenclatureRowHeight * CGFloat(formula.nomenclatureSymbols.count)
            if formula.nomenclatureSymbols.count == 0 {
                nomenclatureTitleLabel.hidden = true
                hasNomenclature = false
            }
            
            // configure summary field
            if formula.summary.isWhiteSpace() {
                // reconstrain the nomenclature title to the view instead of the summary label, and hide the summary fields
                view.removeConstraint(nomenclatureTopConstraint)
                view.addConstraint(NSLayoutConstraint(item: nomenclatureTitleLabel, attribute: .Top, relatedBy: .Equal, toItem: view, attribute: NSLayoutAttribute.TopMargin, multiplier: 1, constant: 5))
                
                summaryLabel.hidden = true
                summaryTitleLabel.hidden = true
                hasSummary = false
            } else {
                // summary exists
                self.summaryLabel.text = formula.summary
                self.summaryLabel.sizeToFit()
            }
            
            if hasNomenclature || hasSummary {
                // set shadow - explicit shadow path set in viewDidLayoutSubviews() to improve performance
                scrollViewContentView.layer.shadowOpacity = 0.8
            } else {
                // hide the background layer - make no distinction between the content view and the scrollView
                scrollView.backgroundColor = scrollViewContentView.backgroundColor
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        // update content size of the vertical scrollview
        self.updateScrollViews()
        loadVisiblePages(false)
        // Set up the content size of the scroll view
        
        if shouldLayoutForRotation {
            equationScrollView.contentOffset = CGPoint(x: CGFloat(pageBeforeRotation) * equationScrollView.frame.width, y: 0)
        }
        
        // specify explicit shadow path to improve performance
        let shadowPath = UIBezierPath(rect: self.scrollViewContentView.layer.bounds).CGPath
        scrollViewContentView.layer.shadowPath = shadowPath
    }
    
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        shouldLayoutForRotation = true
        self.pageBeforeRotation = self.equationPageControl.currentPage
        
        // if turning to portrait the contentsize will increase. Need to increase it beforehand or it will cause jerky animations
        if toInterfaceOrientation == .LandscapeLeft || toInterfaceOrientation == .LandscapeRight {
            let windowSize = UIApplication.sharedApplication().keyWindow!.frame.size
            let ratio = max(windowSize.width / windowSize.height, windowSize.height / windowSize.width)
            let fromScrollSize = equationScrollView.contentSize
            equationScrollView.contentSize = CGSize(width: fromScrollSize.width * ratio, height: fromScrollSize.height)
        }
    }
    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        shouldLayoutForRotation = false
        equationScrollView.contentSize = CGSizeMake(equationScrollView.frame.width * CGFloat(images.count), equationScrollView.frame.height)
        loadVisiblePages(false)
        
    }
    
    // ================ DAF VIEW DELEGATE =================
    
    func dafView(view: DAFView, overrideHitTestForPoint point: CGPoint, withEvent event: UIEvent) -> UIView? {
        // check if the hit corresponds to the showMoreButton

        let p = scrollViewContentView.convertPoint(point, fromView: view)
        if !scrollViewContentView.pointInside(p, withEvent: event) {
            let pt = showMoreButton.convertPoint(point, fromView: view)
            if showMoreButton.pointInside(pt, withEvent: event) {
                return showMoreButton
            }
        }
        
        return nil
    }
    
    // ================ SCROLL VIEW DELEGATE =================
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView == self.equationScrollView {
            loadVisiblePages(false)
        } else if scrollView == self.scrollView {
            if hasSummary || hasNomenclature {
                let maxHeight = hasNomenclature ? nomenclatureTableView.frame.maxY : summaryLabel.frame.maxY
                let scrollHeight = -scrollView.contentOffset.y
                
                if scrollHeight > maxHeight + 5 {
                    if showMoreButton.titleLabel?.text != "-" {
                        showMoreButton.setTitle("-", forState: .Normal)
                    }
                } else {
                    if showMoreButton.titleLabel?.text != "+" {
                        showMoreButton.setTitle("+", forState: .Normal)
                    }
                }
            }
        }
    }
    
    // ======= Vertical ScrollView =======
    
    func updateScrollViews() {
        self.view.layoutIfNeeded()
        
        if images.count <= 1 {
            equationContainer.enabled = false
            equationScrollViewOffsetConstraint.constant = IS_IPAD ? 20 : 8
        } else {
            equationContainer.enabled = true
            equationScrollViewOffsetConstraint.constant = self.view.frame.width * 0.05
        }
        self.view.layoutIfNeeded()
        
        // recalculate correct content mode incase image size has changed past the critical limit
        for page in pageViews {
            if let iView = page {
                let imageSize = iView.image?.size ?? CGSizeZero
                let frameSize = iView.frame.size
                
                if imageSize.width < frameSize.width && imageSize.height < frameSize.height {
                    iView.contentMode = .Center
                } else {
                    iView.contentMode = .ScaleAspectFit
                }
            }
        }
        
        equationScrollView.contentSize = CGSizeMake(equationScrollView.frame.width * CGFloat(images.count), equationScrollView.frame.height)
        titleScrollView.selectTitle(equationPageControl.currentPage, animated: true)
        
        let offset: CGFloat = 15
        if !hasSummary && !hasNomenclature {
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
        } else if !hasNomenclature {
            scrollView.contentInset = UIEdgeInsets(top: summaryLabel.frame.maxY + offset, left: 0, bottom: keyboardHeight, right: 0)
        } else {
            scrollView.contentInset = UIEdgeInsets(top: nomenclatureTableView.frame.maxY + offset, left: 0, bottom: keyboardHeight, right: 0)
        }
        scrollView.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
        solverTableViewController?.tableView.contentOffset = CGPointZero
    
    }
    
    // ======= Titles ScrollView =======

    func titleScrollView(titleScrollView: TitleScrollView, shouldScrollToSelectedTitle title: String, atIndex index: Int) -> Bool {
         
        let duration: NSTimeInterval = IS_IPAD ? 0.5 : 0.3
        UIView.animateWithDuration(duration, animations: {
            self.equationScrollView.contentOffset = CGPoint(x: self.equationScrollView.frame.width*CGFloat(index), y: 0)
            _ = self.loadVisiblePages(false)
        })
        return true
    }
    
    // ======= Equation ScrollView =======
    
    func loadPage(page: Int, force: Bool) {
        if page < 0 || page >= images.count {
            return //outside of display range so do nothing
        }
        
        // Load an individual page, first checking if its already loaded
        if let _ = pageViews[page] {
            return // Do nothing. The view is already loaded.
        }
        
        var frame = equationScrollView.bounds
        frame.size.width = frame.size.width
        frame.origin.x = frame.size.width * CGFloat(page)
        frame.origin.y = 0.0
        
        let inset: CGFloat = {
            if self.images.count <= 1 {
                return 0
            } else {
                return IS_IPAD ? 15 : 5
            }
        }()
        frame = CGRectInset(frame, inset, 0.0)
        
        let image = images[page].getImage()?.colorImage(UIColor.whiteColor())        
        
        let newPageView = UIImageView(image: image)
        newPageView.contentMode = image?.size.width < frame.size.width ?  .Center : UIViewContentMode.ScaleAspectFit
        newPageView.frame = frame
        newPageView.backgroundColor = UIColor.freshGreen()
        newPageView.layer.borderWidth = -10
        newPageView.layer.borderColor = UIColor.blueColor().CGColor
        newPageView.roundToRadius(IS_IPAD ? 30:10)
        newPageView.clipsToBounds = false
        equationScrollView.addSubview(newPageView)
        pageViews[page] = newPageView
        
        // add constraints
        newPageView.translatesAutoresizingMaskIntoConstraints = false
        
        let scrollAspectRatio = (frame.width + 2*inset) / frame.height
        equationScrollView.addConstraint(NSLayoutConstraint(item: newPageView, attribute: .Width, relatedBy: .Equal, toItem: newPageView, attribute: .Height, multiplier: scrollAspectRatio, constant: -2*inset))
        equationScrollView.addConstraint(NSLayoutConstraint(item: newPageView, attribute: .Top, relatedBy: .Equal, toItem: equationScrollView, attribute: .Top, multiplier: 1, constant: 0))
        equationScrollView.addConstraint(NSLayoutConstraint(item: newPageView, attribute: .CenterY, relatedBy: .Equal, toItem: equationScrollView, attribute: .CenterY, multiplier: 1, constant: 0))
        if page == 0 {
            equationScrollView.addConstraint(NSLayoutConstraint(item: newPageView, attribute: .Leading, relatedBy: .Equal, toItem: equationScrollView, attribute: .Leading, multiplier: 1, constant: inset))
        } else {
            equationScrollView.addConstraint(NSLayoutConstraint(item: newPageView, attribute: .Leading, relatedBy: .Equal, toItem: pageViews[page-1], attribute: .Trailing, multiplier: 1, constant: inset*2))
        }
    }
    
    func purgePage(page: Int) {
        if page < 0 || page >= images.count {
            return //outside of display range so do nothing
        }
        
        // Remove a page from the scroll view and reset the container array
        if let pageView = pageViews[page] {
            pageView.removeFromSuperview()
            pageViews[page] = nil
        }
    }
    
    func loadVisiblePages(forceReload: Bool) -> Int {
        
        // Determine which page is currently visible
        let pageWidth = equationScrollView.frame.size.width// * pagedScale
        let page = Int(floor((equationScrollView.contentOffset.x * 2.0 + pageWidth) / (pageWidth * 2.0)))
 
        // Update the page control
        equationPageControl.currentPage = page
        titleScrollView.selectTitle(page, animated: true)
        
        // Work out which pages to load
        let firstPage = 0 //page - 1
        let lastPage = self.images.count-1 //page + 1
        
        // Currently keep anything before the first page. Does not need to be loaded but needed for layout constraints
        // could potentially unload the stored images but keep the imageViews, though not currently necessary as there are never more than 10 images per formula
    
        // Load pages in range
        for var index = firstPage; index <= lastPage; ++index {
            loadPage(index, force: forceReload)
        }
        
        // Purge anything after the last page
        /*for var index = lastPage+1; index < images.count; ++index {
            purgePage(index)
        }*/
        
        return page
    }
    
    // ================ KEYBOARD NOTIFICATIONS =================

    
    override func keyboardWillBeShown(notification: NSNotification) {
        let info = notification.userInfo!
        let frameKey = String(UIKeyboardFrameBeginUserInfoKey)
        
        if let keyboardHeight = info[frameKey]?.CGRectValue.height {
            
            self.keyboardHeight = keyboardHeight
            
            if solverTableViewController?.shouldScroll ?? true {
                scrollView.contentOffset.y += keyboardHeight
                updateScrollViews()
            }
            
        }
    }
    
    override func keyboardWillBeHidden(notification: NSNotification) {
        if solverTableViewController?.shouldScroll ?? true {
            self.keyboardHeight = 0
            scrollView.contentOffset.y -= keyboardHeight
            updateScrollViews()
        }
    }
    
    override func keyboardWasHidden(notification: NSNotification) {
    }
    
    override func keyboardWasShown(notification: NSNotification) {
    }
    
    // ================ FORMULA SOLVER TABLE DELEGATE =================
    
    func formulaSolverTableDidResize(size: CGSize) {
        self.solverContainerHeightConstraint.constant = size.height
        self.updateViewConstraints()
    }
    
    // ================ TABLE VIEW DATA SOURCE =================
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return formula?.nomenclatureSymbols.count ?? 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = nomenclatureTableView.dequeueReusableCellWithIdentifier("NomenclatureCell", forIndexPath: indexPath) as! NomenclatureTableViewCell
        if let symbol = formula?.nomenclatureSymbols.allObjects[¿indexPath.row] as? Symbol {
            cell.configureForSymbol(symbol)
        }
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return nomenclatureRowHeight
    }
    
    // ================ NAVIGATION =================

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "embedSolverTable" {
            let vc = segue.destinationViewController as! FormulaSolverTableViewController
            if let symbols = formula?.symbols.sortedArrayUsingDescriptors([NSSortDescriptor(key: "name", ascending: true)]) as? [Symbol] {
                vc.entities = Entity.initializeEntitiesFromSymbols(symbols)
                vc.formula = formula
                vc.delegate = self
                vc.tableView.scrollEnabled = false
                self.solverTableViewController = vc
            }
        } else if segue.identifier == "embedComments" {
            if let vc = segue.destinationViewController as? CommentViewController {
                print("success")
                vc.test = true
            } 
        }
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if self.formula == nil {
            return false
        } else if identifier == "embedSolverTable" {
            if self.formula?.symbols.count == 0 {
                self.solverContainer.hidden = true
                self.solverContainerHeightConstraint.constant = 0
                return false
            }
        }
        return true
    }
    
}
