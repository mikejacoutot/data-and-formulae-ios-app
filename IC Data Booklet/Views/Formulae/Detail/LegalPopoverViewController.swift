//
//  LegalPopoverViewController.swift
//  Temp
//
//  Created by Mike on 11/01/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit

class LegalPopoverViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    var legal: String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.label.text = legal
        self.label.sizeToFit()
        self.preferredContentSize = CGSize(width: label.frame.width, height: label.frame.height)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
