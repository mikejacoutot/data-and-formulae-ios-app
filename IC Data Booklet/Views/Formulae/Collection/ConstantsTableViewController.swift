//
//  ConstantsTableViewController.swift
//  Temp
//
//  Created by Mike on 10/01/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit

class ConstantsTableCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
    
    func configureCellForConstant(constant: Symbol) {
        var text = NSMutableAttributedString(string: constant.name, font: label.font)
        text += ", "
        text += constant.display.parseSuperAndSubscript(label.font)
        text += " = "
        text += constant.constant.doubleValue.attributedStringWithExponentBase10(significantFigures: 5, forFont: label.font)
        
        if let unit = constant.unitCategory.getBaseUnit() {
            text += " "
            text += unit.getDisplayString(forFont: label.font, appendFullName: false)
        } else { print("CANT GET UNIT: \(constant.unitCategory)") }
        label.attributedText = text
    }
}


class ConstantsTableViewController: UITableViewController, UIPopoverControllerDelegate {
    
    // ================ VARIABLES =================
    
    let rowHeight: CGFloat = 40
    
    var constants: [Symbol] = (databaseManager.getAllEntitiesOfType(kDataSymbol, withPredicate: NSPredicate(format: "isConstant == %@", true)) as! [Symbol]).sort({ $0.name < $1.name })
    
    // ================ INITIALIZERS =================
    
    /*init(constants: [Symbol]){
        super.init()
        self.constants = constants
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }*/
    
    // ================ VIEW CONTROLLER =================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if constants.count > 0 {
            self.preferredContentSize = CGSize(width: 360, height: CGFloat(constants.count)*rowHeight)
        } else {
            self.preferredContentSize = CGSize(width: 0, height: 0)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // ================ TABLE VIEW DATA SOURCE =================

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return constants.count
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return rowHeight
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("constantCell", forIndexPath: indexPath) as! ConstantsTableCell
        
        if constants.count >= indexPath.row {
            let constant = constants[indexPath.row]
            cell.configureCellForConstant(constant)
        }
        return cell
    }
}
