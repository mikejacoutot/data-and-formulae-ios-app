//
//  FormulaCollectionViewLayout.swift
//  Temp
//
//  Created by Mike on 08/01/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class FormulaCollectionViewLayout: UICollectionViewFlowLayout {
    
    
    override func layoutAttributesForElementsInRect(rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
       /* let attributes = super.layoutAttributesForElementsInRect(rect)!
        let newAttributes = NSMutableArray(capacity: attributes.count)
        
        for attribute in attributes {
            if attribute.frame.origin.x + attribute.frame.size.width <= self.collectionViewContentSize().width && attribute.frame.origin.y + attribute.frame.size.height <= collectionViewContentSize().height {
                newAttributes.addObject(attribute)
            }
        }
        
        return newAttributes*/
        
        
        
        var answer = super.layoutAttributesForElementsInRect(rect)!
        
        let missingSections = NSMutableIndexSet()
        for var idx = answer.count - 1; idx >= 0; idx-- {
            let layoutAttributes = answer[idx] 
            
            if layoutAttributes.representedElementCategory == .Cell {
                missingSections.addIndex(layoutAttributes.indexPath.section)
            }
            if layoutAttributes.representedElementKind != nil {
                if layoutAttributes.representedElementKind == UICollectionElementKindSectionHeader {
                    answer.removeAtIndex(idx)
                }
            }
        }
        
         // layout all headers needed for the rect using self code
        missingSections.enumerateIndexesUsingBlock({
            (idx, stop) in
            let indexPath = NSIndexPath(forItem: 1, inSection: idx)
            let layoutAttributes = self.layoutAttributesForSupplementaryViewOfKind(UICollectionElementKindSectionHeader, atIndexPath: indexPath)
            if layoutAttributes != nil {
                answer.append(layoutAttributes!)
            }
        })
        return answer
    }

    
    override func layoutAttributesForSupplementaryViewOfKind(elementKind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionViewLayoutAttributes? {
        if let attributes = super.layoutAttributesForSupplementaryViewOfKind(elementKind, atIndexPath: indexPath) {
            
            if elementKind == UICollectionElementKindSectionHeader {
                let cv = self.collectionView!
                let contentOffset = cv.contentOffset
                var nextHeaderOrigin = CGPointMake(CGFloat.infinity, CGFloat.infinity)
                
                if indexPath.section + 1 < cv.numberOfSections() {
                    if let nextHeaderAttributes = super.layoutAttributesForSupplementaryViewOfKind(elementKind, atIndexPath: NSIndexPath(forItem: 0, inSection: indexPath.section + 1)) {
                        nextHeaderOrigin = nextHeaderAttributes.frame.origin
                    }
                }
                
                var frame = attributes.frame
                if self.scrollDirection == .Vertical {
                    frame.origin.y = min(max(contentOffset.y, frame.origin.y), nextHeaderOrigin.y - frame.height)
                } else {
                    frame.origin.x = min(max(contentOffset.x, frame.origin.x), nextHeaderOrigin.y - frame.width)
                }
                attributes.zIndex = indexPath.section + 1
                attributes.frame = frame
                
            }
            return attributes
        }
        return nil
    }
  
    override func initialLayoutAttributesForAppearingSupplementaryElementOfKind(elementKind: String, atIndexPath elementIndexPath: NSIndexPath) -> UICollectionViewLayoutAttributes? {
        let attributes = self.layoutAttributesForSupplementaryViewOfKind(elementKind, atIndexPath: elementIndexPath)
        return attributes
    }
    
    override func finalLayoutAttributesForDisappearingSupplementaryElementOfKind(elementKind: String, atIndexPath elementIndexPath: NSIndexPath) -> UICollectionViewLayoutAttributes? {
        let attributes = self.layoutAttributesForSupplementaryViewOfKind(elementKind, atIndexPath: elementIndexPath)
        return attributes
    }
    
    override func shouldInvalidateLayoutForBoundsChange(newBounds: CGRect) -> Bool {
        return true
    }
    
    
    /*override func layoutAttributesForElementsInRect(rect: CGRect) -> [AnyObject]? {
        let answer: NSMutableArray = NSMutableArray(array: super.layoutAttributesForElementsInRect(rect)!)

        let missingSections = NSMutableIndexSet()
        
        for var idx = 0; idx < answer.count; idx++ {
            let layoutAttributes: UICollectionViewLayoutAttributes = answer[idx] as UICollectionViewLayoutAttributes
            if layoutAttributes.representedElementCategory == UICollectionElementCategory.Cell {
                missingSections.addIndex(layoutAttributes.indexPath.section)
            }
            if layoutAttributes.representedElementKind == UICollectionElementKindSectionHeader {
                answer.removeObjectAtIndex(idx)
                idx--
            }
 
            missingSections.enumerateIndexesUsingBlock(){ (idx: Int, stop) in
                let indexPath = NSIndexPath(forItem: 1, inSection: idx)
                let layoutAttributes = self.layoutAttributesForSupplementaryViewOfKind(UICollectionElementKindSectionHeader, atIndexPath: indexPath)
                if layoutAttributes != nil {
                    answer.addObject(layoutAttributes)
                }
            }
        }
        return answer
    }
    
    override func layoutAttributesForSupplementaryViewOfKind(elementKind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionViewLayoutAttributes! {
        
        let attributes = super.layoutAttributesForSupplementaryViewOfKind(elementKind, atIndexPath: indexPath)
        if elementKind == String(UICollectionElementKindSectionHeader) {
            let cv = self.collectionView
            let contentOffset = cv!.contentOffset
            var nextHeaderOrigin = CGPoint(x: Double.infinity, y: Double.infinity)
            
            if indexPath.section + 1 < cv?.numberOfSections() {
                let nextHeaderAttributes = super.layoutAttributesForSupplementaryViewOfKind(elementKind, atIndexPath:NSIndexPath(forItem: 0, inSection: indexPath.section + 1))
                nextHeaderOrigin = nextHeaderAttributes.frame.origin
            }
            
            var frame = attributes.frame
            if self.scrollDirection == UICollectionViewScrollDirection.Vertical {
                frame.origin.y = min(max(contentOffset.y, frame.origin.y), nextHeaderOrigin.y - CGRectGetHeight(frame))
            } else {
                frame.origin.x = min(max(contentOffset.x, frame.origin.x), nextHeaderOrigin.x - CGRectGetHeight(frame))
            }
            attributes.zIndex = indexPath.section + 1
            attributes.frame = frame
        }
        return attributes
    }
    
    override func initialLayoutAttributesForAppearingSupplementaryElementOfKind(elementKind: String, atIndexPath elementIndexPath: NSIndexPath) -> UICollectionViewLayoutAttributes? {
        let attributes = self.layoutAttributesForSupplementaryViewOfKind(elementKind, atIndexPath: elementIndexPath)
        return attributes
    }
    
    override func finalLayoutAttributesForDisappearingSupplementaryElementOfKind(elementKind: String, atIndexPath elementIndexPath: NSIndexPath) -> UICollectionViewLayoutAttributes? {
        let attributes = self.layoutAttributesForSupplementaryViewOfKind(elementKind, atIndexPath: elementIndexPath)
        return attributes
    }
    
    override func shouldInvalidateLayoutForBoundsChange(newBounds: CGRect) -> Bool {
        return true
    }*/
}