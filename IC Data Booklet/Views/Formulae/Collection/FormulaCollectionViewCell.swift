//
//  FormulaCollectionViewCell.swift
//  Temp
//
//  Created by Mike on 08/01/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit

class FormulaCollectionViewCell: UICollectionViewCell {
    
    var formula: Formula!
    
    // ================ IB OUTLETS =================
    
    @IBOutlet weak var equationImage: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    // ================ INITIALIZERS =================
    
    func configureCellForFormula(formula: Formula) {
        
        //self.backgroundView?.autoresizingMask = .FlexibleWidth | .FlexibleHeight
        
        //self.autoresizingMask = .FlexibleWidth | .FlexibleHeight
        
        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad {
            //self.label.font = UIFont.systemFontOfSize(42)
        } else {
            //self.label.font = UIFont.systemFontOfSize(20)
        }
        self.label.text = formula.name
        
        if let image = formula.getImage() {
            self.equationImage.image = image.imageWithRenderingMode(.AlwaysTemplate)
            self.equationImage.tintColor = .whiteColor()
            let size = FormulaCollectionViewCell.preferredSizeForImage(image)
            self.frame = CGRect(origin: self.frame.origin, size: size)
            self.contentView.frame = self.frame
        }
        
        self.clipsToBounds = true
        //self.equationImage.setTranslatesAutoresizingMaskIntoConstraints(false)
        self.backgroundColor = UIColor.freshGreen()
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.contentView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        self.contentView.translatesAutoresizingMaskIntoConstraints = true
        
        //self.backgroundView?.autoresizingMask = .FlexibleWidth | .FlexibleHeight
        /*self.backgroundView?.setTranslatesAutoresizingMaskIntoConstraints(false)
        self.addConstraint(NSLayoutConstraint(item: self, attribute: .Top, relatedBy: .Equal, toItem: self.backgroundView, attribute: .Top, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: self, attribute: .Bottom, relatedBy: .Equal, toItem: self.backgroundView, attribute: .Bottom, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: self, attribute: .Leading, relatedBy: .Equal, toItem: self.backgroundView, attribute: .Leading, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: self, attribute: .Trailing, relatedBy: .Equal, toItem: self.backgroundView, attribute: .Trailing, multiplier: 1, constant: 0))*/
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //self.backgroundColor = UIColor.freshGreen()
    }
    
    
    
    // ================ VIEW CONTROL =================

    override func prepareForReuse() {
        super.prepareForReuse()
        self.backgroundColor = UIColor.redColor()
    }
    
    
    
    override func drawRect(rect: CGRect) {
        let layer: CALayer = self.layer
        layer.masksToBounds = true
        layer.cornerRadius = 15
        layer.rasterizationScale = UIScreen.mainScreen().scale
        layer.shouldRasterize = true
        
        layer.shadowColor = UIColor.blackColor().CGColor
        layer.shadowOffset = CGSize(width: 2,height: 2)
        layer.shadowRadius = 1
        layer.shadowOpacity = 0.9
        
        layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: layer.cornerRadius).CGPath
        //self.contentView.frame = self.bounds
    }

    class func preferredSizeForImage(image: UIImage) -> CGSize {
        let baseSize = image.size
        let is_iPad = UIDevice.currentDevice().userInterfaceIdiom == .Pad
        
        let scale = is_iPad ? 0.5 : 0.3
        let buffer = CGSize(width: 40, height: 60)
        let insets = CGSize(width: 20, height: 20)
        
        let scaleSize = CGSize(width: scale, height: scale)
        let scaledImageSize = baseSize * scaleSize
        
        let winFrame = UIApplication.sharedApplication().keyWindow!.frame
        
        let maxWidth = min(winFrame.height, winFrame.width) - insets.width - buffer.width
        
        if scaledImageSize.width > maxWidth {
            let rescaledImageSize = CGSize(width: maxWidth, height: scaledImageSize.height * maxWidth / scaledImageSize.width)
            return rescaledImageSize + buffer
        }
        return scaledImageSize + buffer
    }
    
}
