//
//  FormulaCollectionViewController.swift
//  Temp
//
//  Created by Mike on 08/01/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit
import CoreData

protocol FormulaCollectionViewControllerDelegate {
    func formulaCollection(shouldHideKeyboard formulaCollection: FormulaCollectionViewController)
}

class FormulaCollectionViewController: UICollectionViewController, NSFetchedResultsControllerDelegate, UISearchBarDelegate, UICollectionViewDelegateFlowLayout {

    // ================ VARIABLES =================
    
    lazy var fetchedResultsController: NSFetchedResultsController = {
        
        let pred = NSPredicate(format: "hasMain==True", argumentArray: nil)
        
        // Edit the sort key as appropriate.
        let sortCategory = NSSortDescriptor(key: "category.name", ascending: true)
        let sortName = NSSortDescriptor(key: "name", ascending: true)
        let sortDescriptors = [sortCategory,sortName]
        
        return databaseManager.getFetchedResultsController(kDataFormula, withPredicate: pred, withSortDescriptors: sortDescriptors, sectionNameKeyPath: "category.name", delegate: self, batchSize: 20)
    }()
    
    
    var selectedFormula: Formula?
    var delegate: FormulaCollectionViewControllerDelegate?
    var mode: String!
    var forms = []
    var results = []
    let refresh = UIRefreshControl()
    var objectChanges: [[String:[NSIndexPath]]] = []
    var sectionChanges: [[String:Int]] = []
    
    // ================ VIEW CONTROLLER =================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // collection view
        self.collectionView?.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "othercell")
        self.collectionView?.registerClass(SectionHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "SectionHeader")

        self.collectionView?.addSubview(refresh)
        self.collectionView?.alwaysBounceVertical = true
        self.collectionView?.reloadData()
        self.collectionView?.backgroundColor = UIColor.whiteColor()
        
        // refresh control
        refresh.tintColor = UIColor.grayColor()
        refresh.addTarget(self, action: "syncWithDB", forControlEvents: .ValueChanged)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   

    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        self.collectionView?.performBatchUpdates(nil, completion: nil)
    }
    
    // ================ SERVER COMMUNICATION =================
    
    func syncWithDB() {
        connectionManager.syncWithDB(refresh)
    }
    
    // ================ COLLECTION VIEW DATA SOURCE =================
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        if let sections = self.fetchedResultsController.sections {
            return sections.count
        } else {
            print("CANT GET SECTION COUNT INFO")
            return 1
        }
    }

    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let sections = self.fetchedResultsController.sections {
            if sections.count > section {
                return sections[section].objects?.count ?? 0
            }
        }
        
        print("CANT GET SECTION INFO")
        return self.fetchedResultsController.fetchedObjects!.count
  
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("showFormula", sender: indexPath)
    }
    
    override func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        let sh = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "SectionHeader", forIndexPath: indexPath) as! SectionHeader
        if let sections = fetchedResultsController.sections {
            sh.configureForTitle(sections[indexPath.section].name)
        } else {
            sh.configureForTitle("???")
            print("CANT GET SECTION INFO")
        }
        return sh
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let formula = self.fetchedResultsController.objectAtIndexPath(indexPath) as! Formula
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("FormulaCell", forIndexPath: indexPath) as! FormulaCollectionViewCell
        
        cell.configureCellForFormula(formula)
        cell.contentView.frame = cell.bounds
        cell.contentView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        return cell
    }
    
    // ================ COLLECTION VIEW DELEGATE FLOW LAYOUT =================


    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            return CGSize(width: 768, height: 75)
        } else {
            return CGSize(width: 320, height: 45)
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return sizeForItemAtIndexPath(indexPath)
    }
    
    func sizeForItemAtIndexPath(indexPath: NSIndexPath) -> CGSize{
        if let sections = fetchedResultsController.sections {
            if indexPath.section == sections.count {
                if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
                    return CGSize(width: 700, height: 100)
                } else {
                    return CGSize(width: 700, height: 100)
                }
            }
        }
        let formula = self.fetchedResultsController.objectAtIndexPath(indexPath) as! Formula
        if let image = formula.getImage() {
            return FormulaCollectionViewCell.preferredSizeForImage(image)
        } else {
            print("CANT GET FORMULA")
        }
        
        print("DEFAULTSIZE")
        return CGSize(width: 300, height: 100)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    // ================ NS FETCHED RESULTS CONTROLLER DELEGATE =================
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        print("WILLCHANGE")
        self.collectionView?.reloadData()
        //self.collectionView?.performBatchUpdates(nil, completion: nil)
        }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        print("CONTROLLER DID CHANGE OBJECT")
        
        var change: [String:[NSIndexPath]] = [:]
        switch type {
        case .Insert:
            change["Insert"] = [newIndexPath!]
            break
            
        case .Delete:
            change["Delete"] = [indexPath!]
            break
            
        case .Update:
            change["Update"] = [indexPath!]
            break
            
        case .Move:
            change["Move"] = [indexPath!,newIndexPath!]
            break
        }
        objectChanges += [change]
    }
    
    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        print("CONTROLLER DID CHANGE SECTION")
        var change: [String:Int] = [:]
        switch type {
        case .Insert:
            change["Insert"] = sectionIndex
            break
            
        case .Delete:
            change["Delete"] = sectionIndex
            break
        default: ()
        }
        sectionChanges += [change]
    }
    
    func manageUpdates() {
        print("MANAGE")
        let delete = NSMutableIndexSet()
        let insert = NSMutableIndexSet()
        for sChange in sectionChanges {
            let key = Array(sChange.keys)[0]
            if key == "Delete" {
                delete.addIndex(sChange[key]!)
            } else if key == "Insert" {
                insert.addIndex(sChange[key]!)
            }
        }
        self.collectionView?.insertSections(insert)
        self.collectionView?.deleteSections(delete)
        self.sectionChanges = []
        
        for oChange in objectChanges {
            let key = Array(oChange.keys)[0]
            if key == "Delete" {
                
            } else if key == "Insert" {
                
            } else if key == "Update" {
                
            } else if key == "Move" {
                
            }
        }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
        print("DIDCHANGE")
        //self.manageUpdates()
        self.collectionView?.reloadData()//performBatchUpdates(nil, completion: nil)
    }
    
    // ================ SEARCH BAR DELEGATE =================
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String, andScopeIndex scopeIndex: Int, withCategoryName categoryName: String) {
        let searchProperties = ["category.name","name","summary","tags"] //"ANY images.name","ANY nomenclatureSymbols.name"]
        
        
        if !searchText.isWhiteSpace() || scopeIndex != 0 {
            let terms = searchText.componentsSeparatedByCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            
            var subpredicates: [NSPredicate] = []
            
            for term in terms {
                if term.characters.count > 0 {
                    var propertyPredicates: [NSPredicate] = []
                    for property in searchProperties {
                    let pred = NSPredicate(format: "%K contains [cd] %@", property, term)
                    propertyPredicates += [pred]
                        
                    }
                    let propertyFilter = NSCompoundPredicate(type: .OrPredicateType, subpredicates: propertyPredicates)
                    subpredicates += [propertyFilter]
                }
            }
            if scopeIndex != 0 {
                //if let title = searchBar.scopeButtonTitles![scopeIndex] as? String {
                    subpredicates += [NSPredicate(format: "category.name contains[cd] %@", categoryName)]
                //}
            }
            
            let filter = NSCompoundPredicate(type: .AndPredicateType, subpredicates: subpredicates)
            self.fetchedResultsController.fetchRequest.predicate = filter
            
        } else {
            self.fetchedResultsController.fetchRequest.predicate = nil
        }
        
        var error: NSError? = nil
        do {
            try self.fetchedResultsController.performFetch()
        } catch let error1 as NSError {
            error = error1
            print("error: \(error)")
            exit(-1)
        }
        self.collectionView?.reloadData()
       
        
        //self.manageUpdates()
        //self.collectionView?.performBatchUpdates(nil, completion: nil)
        //self.collectionView?.performBatchUpdates(nil, completion: nil)
    }

    
    func processUpdates() {
    }
    
    // ================ NAVIGATION =================
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if identifier == "showFormula" {
            if sender != nil {
                var indexPath: NSIndexPath!
                if let path = sender as? NSIndexPath {
                    indexPath = path
                } else {
                    indexPath = sender!.indexPath
                }
                if let _ = self.fetchedResultsController.objectAtIndexPath(indexPath) as? Formula {
                    return true
                }
            }
        }
        return false
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showFormula" {
            if sender != nil {
                var indexPath: NSIndexPath!
                if let path = sender as? NSIndexPath {
                    indexPath = path
                } else {
                    indexPath = sender!.indexPath
                }
                
                if let formula = self.fetchedResultsController.objectAtIndexPath(indexPath) as? Formula {
                    let tabVC = segue.destinationViewController as! FormulaTabBarViewController
                    tabVC.formula = formula
                }
            }
        }
    }
    
    // ================ GESTURE RECOGNIZERS =================
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        delegate?.formulaCollection(shouldHideKeyboard: self)
    }
    
    override func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        self.delegate?.formulaCollection(shouldHideKeyboard: self)
    }
}
