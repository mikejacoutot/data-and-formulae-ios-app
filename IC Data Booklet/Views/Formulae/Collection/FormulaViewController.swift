//
//  FormulaeViewController.swift
//  Test
//
//  Created by Mike on 16/12/2014.
//  Copyright (c) 2014 Mike. All rights reserved.
//

import UIKit
import CoreData

class FormulaViewController: UIViewController, UISearchBarDelegate, FormulaCollectionViewControllerDelegate {
    
    // ================ VARIABLES =================
    var formula: Formula?
    var categories: [FormulaCategory] = []
    var fetchedResultsController: NSFetchedResultsController?
    var mode: String?
    var popTransitionManager = PopOutTransitionOperator()
    var cvc: FormulaCollectionViewController!
    
    var _scopeBar: UISegmentedControl?
    var scopeBar: UISegmentedControl? {
        if self._scopeBar != nil {
            return self._scopeBar
        }
        
        if let result = scopeBarInViewHeirarchy(self.formulaSearchBar) {
            self._scopeBar = result
            return result
        }
        print("not found")
        return nil
    }
    
    // recursive search for a scopebar in the searchbar view heirarchy
    func scopeBarInViewHeirarchy(view: UIView) -> UISegmentedControl? {
        if view.isKindOfClass(UISegmentedControl.self) {
            return view as? UISegmentedControl
        }
        for subview in view.subviews {
            if let result = scopeBarInViewHeirarchy(subview) {
                return result
            }
        }
        return nil
    }
    
    var _scopeFont: UIFont?
    var scopeFont: UIFont? {
        if self._scopeFont != nil {
            return self._scopeFont
        }
        if self.scopeBar != nil {
            self._scopeFont = fontForScopeBar(self.scopeBar!)
            return self._scopeFont
        }
        return nil
    }
    
    func fontForScopeBar(scopeBar: UISegmentedControl) -> UIFont? {
        for subview in self.scopeBar!.subviews {
            for sv in subview.subviews {
                if let label = sv as? UILabel {
                    return label.font
                }
            }
        }
        return nil
    }
    
    // ================ IBOUTLETS =================
    
    @IBOutlet weak var formulaSearchBar: UISearchBar!
    
    // ================ IBACTIONS =================
    
    @IBAction func constantsPressed(sender: AnyObject) {
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            self.performSegueWithIdentifier("popoverConstants", sender: sender)
        } else {
            self.performSegueWithIdentifier("showConstants", sender: sender)
        }
    }
    
    // ================ VIEW CONTROLLER =================
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let pred = NSPredicate(format: "formulae.@count >= 1", argumentArray: nil)
        self.categories = databaseManager.getAllEntitiesOfType(kDataFormulaCategory, withPredicate: pred) as! [FormulaCategory]
        
        var titles: [String] = []
        for category in categories {
            titles += [category.name]
        }
        
        // format search bar
        self.formulaSearchBar.barTintColor = UIColor.grayColor()
        self.formulaSearchBar.tintColor = UIColor.whiteColor()
        self.formulaSearchBar.scopeButtonTitles = ["All"] + titles
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        getSearchBarScopeButtonLabelSize()
    }
    
    func getSearchBarScopeButtonLabelSize() {
        if self.scopeBar != nil {
            let scopeSize = scopeBar!.frame.size
            let font = self.scopeFont ?? UIFont.systemFont()
            
            for i in 0...self.categories.count {
                if i > 0 {
                    let category = self.categories[i-1]
                    let size = CGSize(width: scopeSize.width/CGFloat(self.categories.count + 1) - 2, height: scopeSize.height)
                    let title = category.nameForSize(size, font: font)
                    self.formulaSearchBar.scopeButtonTitles![i] = title
                }
            }
        }
    }
    
    // ================ FORMULA COLLECTION DELEGATE =================
    
    func formulaCollection(shouldHideKeyboard formulaCollection: FormulaCollectionViewController) {
        self.formulaSearchBar.resignFirstResponder()
    }
    
    // ================ SEARCH BAR DELEGATE =================

    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        let selectedScope = formulaSearchBar.selectedScopeButtonIndex
        let hasCategory = self.categories.count >= selectedScope && selectedScope > 0
        let catName = hasCategory ? categories[selectedScope-1].name : ""
        cvc!.searchBar(searchBar, textDidChange: searchText, andScopeIndex: selectedScope, withCategoryName: catName)
    }
    
    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        let hasCategory = self.categories.count >= selectedScope && selectedScope > 0
        let catName = hasCategory ? categories[selectedScope-1].name : ""
        cvc.searchBar(searchBar, textDidChange: searchBar.text ?? "", andScopeIndex: selectedScope, withCategoryName: catName)
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    // ================ NAVIGATION =================

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "showConstants" {
            let toViewController = segue.destinationViewController 
            toViewController.transitioningDelegate = popTransitionManager
        } else if segue.identifier == "embedCollection" {
            cvc = segue.destinationViewController as! FormulaCollectionViewController
            cvc.delegate = self
        }
    }

}
