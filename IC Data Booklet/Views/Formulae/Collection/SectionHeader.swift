//
//  SectionHeader.swift
//  Temp
//
//  Created by Mike on 10/01/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit

class SectionHeader: UICollectionReusableView {
    
    // ================ IB OUTLETS =================
    
    var sectionName: UILabel!
    
    // ================ INITIALIZERS =================
    
    
    override init(var frame: CGRect) {
        let isIpad = UIDevice.currentDevice().userInterfaceIdiom == .Pad
        if isIpad {
            frame = CGRect(x: 0, y: 0, width: 758, height: 75)
        } else {
            frame = CGRect(x: 0, y: 0, width: 320, height: 45)
        }
        super.init(frame: frame)
        
        self.sectionName = UILabel(frame: frame)
        self.sectionName.textAlignment = .Center
        self.addSubview(sectionName)
        self.sectionName.textColor = UIColor.whiteColor()
        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad {
            self.sectionName.font = UIFont.systemFontOfSize(60)
        } else {
            self.sectionName.font = UIFont.systemFontOfSize(36)
        }
        
        self.backgroundColor = UIColor.grayColor()
        
        self.sectionName.translatesAutoresizingMaskIntoConstraints = false
        self.addConstraint(NSLayoutConstraint(item: self, attribute: .Top, relatedBy: .Equal, toItem: sectionName, attribute: .Top, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: self, attribute: .Bottom, relatedBy: .Equal, toItem: sectionName, attribute: .Bottom, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: self, attribute: .Leading, relatedBy: .Equal, toItem: sectionName, attribute: .Leading, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: self, attribute: .Trailing, relatedBy: .Equal, toItem: sectionName, attribute: .Trailing, multiplier: 1, constant: 0))
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func configureForTitle(title: String) {
        self.sectionName.text = title
        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad {
            self.sectionName.font = UIFont.systemFontOfSize(60)
        } else {
            self.sectionName.font = UIFont.systemFontOfSize(36)
        }
    }
    

}
