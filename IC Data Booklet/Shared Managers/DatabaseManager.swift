//
//  DatabaseManager.swift
//  Temp
//
//  Created by Mike on 06/01/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit
import CoreData

// database class keys
let kDataGraph = "Graph"
let kDataGraphAxis = "GraphAxis"
let kDataSteam = "SteamTables"
let kDataUnit = "Unit"
let kDataUnitCategory = "UnitCategory"
let kDataFormula = "Formula"
let kDataFormulaCategory = "FormulaCategory"
let kDataImage = "Image"
let kDataSymbol = "Symbol"
let kDataPK = "pk"


public let DatabaseManagerErrorDomain = "com.databaseManager.error"

class DatabaseManager {
    
    // ================ VARIABLES =================
    
    lazy var appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    lazy var managedObjectContext: NSManagedObjectContext? = {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        if let managedObjectContext = appDelegate.managedObjectContext {
            return managedObjectContext
        } else {
            return nil
        }
        }()
    
    // ================ INITIALIZERS =================
    
    static let sharedManager = DatabaseManager()
    
    // ================ SETUP & MANAGEMENT =================
    
    func deleteObject(object: NSManagedObject) {
        managedObjectContext?.deleteObject(object)
    }
    
    func saveContext() {
        appDelegate.saveContext()
    }
    
    func getFetchedResultsController(type: String, withPredicate predicate: NSPredicate?, withSortDescriptors descriptors: [NSSortDescriptor]?, sectionNameKeyPath: String?, delegate: NSFetchedResultsControllerDelegate, batchSize: Int = 20) -> NSFetchedResultsController {
        let request = NSFetchRequest(entityName: type)
        
        request.sortDescriptors = descriptors

        request.predicate = predicate
        
        let resultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: self.managedObjectContext!, sectionNameKeyPath: sectionNameKeyPath, cacheName: "Master")
        
        resultsController.delegate = delegate
        
        var error: NSError? = nil
        do {
            try resultsController.performFetch()
        } catch let error1 as NSError {
            error = error1
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(error), \(error?.userInfo)")
            abort()
        }
        
        return resultsController
    }
    
    // ================ REQUESTS =================
    
    func getAllEntitiesOfType(type: String, withPredicate: NSPredicate?) -> [AnyObject]  {
        let request = NSFetchRequest(entityName: type)
        request.returnsObjectsAsFaults = false
        if withPredicate != nil {
            request.predicate = withPredicate
        }
        let fetchedResults = try? managedObjectContext!.executeFetchRequest(request)
        
        if fetchedResults != nil {
            return fetchedResults!
        }
        return []
    }
    
    
    func getEntity(type: String, withID pk: NSNumber, createIfMissing: Bool) -> AnyObject? {
        /*No profile exists so we have to create one*/
        
        let request = NSFetchRequest(entityName: type)
        request.fetchLimit = 1
        request.predicate = NSPredicate(format: "pk = %@", pk)
        
        if let result = try? managedObjectContext?.executeFetchRequest(request) {
            if result!.count == 1 {
                return result![0]
            } else if createIfMissing {
                let obj: Meta = NSEntityDescription.insertNewObjectForEntityForName(type, inManagedObjectContext: managedObjectContext!) as! Meta
                obj.lastModified = NSDate(timeIntervalSince1970: 0)
                obj.pk = pk
                
                return obj
            }
            
        }
        return nil
    }
    
    func getTimeStampsOfEntities(entities: [Meta]) -> [Int:NSDate] {
        var timestamps: [Int:NSDate] = [:]
        for e in entities {
            timestamps[e.pk.integerValue] = e.lastModified
        }
        return timestamps
    }
    
    // ================ SPECIFIC REQUESTS =================
    
    func getSteamTablesData() -> SteamTables {
        let steam = self.getAllEntitiesOfType(kDataSteam, withPredicate: nil) as! [SteamTables]
        if steam.count > 0 {
            return steam[0]
        }
        let steamData = (self.getEntity(kDataSteam, withID: 1, createIfMissing: true) as! SteamTables)
        steamData.calculations = []
        steamData.parameters = [:]
        steamData.graphs = Set<Graph>()
        return steamData
    }
    
    func deleteCreatedUnits() {
        let predicate = NSPredicate(format: "isCompound = true", argumentArray: nil)
        for unit in self.getAllEntitiesOfType(kDataUnit, withPredicate: predicate) as! [Unit] {
            self.deleteObject(unit)
        }
        self.saveContext()
    }
}