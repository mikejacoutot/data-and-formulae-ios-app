//
//  ResponseStructs.swift
//  Temp
//
//  Created by Mike on 03/04/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation

struct SolverSolution {
    var index: Int
    var complex: Bool
    var real: Double
    var imag: Double
    var vars: String
}