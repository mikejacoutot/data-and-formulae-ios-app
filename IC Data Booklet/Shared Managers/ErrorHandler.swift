//
//  ErrorHandler.swift
//  IC Data Booklet
//
//  Created by Mike on 18/08/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import UIKit

class ErrorHandler {
    
    // ================ INITIALIZERS =================
    
    static let sharedManager = ErrorHandler()
    
    // ================ GENERAL FUNCTIONS =================
    
    func openSafariURL(urlString: String) {
        if let url = NSURL(string: urlString) where UIApplication.sharedApplication().openURL(url) {
            // success
        } else {
            NSLog("%@ %@","failed to open url:",urlString)
        }
    }
    
    func openSettings() {
        if let url = NSURL(string: UIApplicationOpenSettingsURLString) where UIApplication.sharedApplication().openURL(url) {
                // success
        } else {
            NSLog("failed to open settings app")
        }
    }
    
    // ================ CONNECTION MANAGER =================
    
    func checkConnection() -> Bool {
        if Reachability.reachabilityForInternetConnection().currentReachabilityStatus().rawValue != 0 {
            // has internet connection
            
        } else {
            UIAlertView(title: "Connection Error", message: "No internet connection detected. Operation could not be completed.", delegate: nil, cancelButtonTitle: "OK").show()
        }
        return false
    }
    
    
}