//
//  KeychainManager.swift
//  IC Data Booklet
//
//  Created by Mike on 14/08/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation

public enum ReturnType: Int {
    case Data, PersistentRef
}

class KeychainManager {
    
    // ================ INITIALIZERS =================
    
    static let sharedManager = KeychainManager()
    
    // ================ VARIABLES =================
    
    var vpnUsername: String? {
        get {
            return loadData(forKey: kICVPNUsername, inService: kICVPNServiceName, returnType: .Data)?.stringValue
        }
        set {
            if let name = vpnUsername, let data = name.dataValue() {
                updateData(data, key: kICVPNUsername, inService: kICVPNServiceName)
            }
        }
    }
    
    var vpnPassword: String? {
        get {
            return loadData(forKey: kICVPNPassword, inService: kICVPNServiceName, returnType: .Data)?.stringValue
        }
        set {
            if let name = vpnPassword, let data = name.dataValue() {
                updateData(data, key: kICVPNPassword, inService: kICVPNServiceName)
            }
        }
    }
    
    var vpnPasswordReference: NSData? {
        get {
            return loadData(forKey: kICVPNPassword, inService: kICVPNServiceName, returnType: .PersistentRef) as? NSData
        }
    }
    
    private func initialiseDictionary(key: String, service: String) -> [String:AnyObject] {
        var dict: [String:AnyObject] = [:]
        dict[String(kSecClass)] = kSecClassGenericPassword
        let encodedKey = NSString(string: key).dataUsingEncoding(NSUTF8StringEncoding)
        dict[String(kSecAttrGeneric)] = encodedKey
        dict[String(kSecAttrAccount)] = encodedKey
        dict[String(kSecAttrService)] = service
        dict[String(kSecAttrAccessible)] = kSecAttrAccessibleAlwaysThisDeviceOnly
        
        return dict
    }
    
    func updateData(data: NSData, key: String, inService service: String) {
        deleteData(key, inService: service)
        saveData(data, key: key, inService: service)
    }
    
    func deleteData(key: String, inService service: String) {
        let dict = initialiseDictionary(key, service: service)
        SecItemDelete(dict)
    }
    
    func saveData(data: NSData, key: String, inService service: String) {
        var dict = initialiseDictionary(key, service: service)
        dict[String(kSecValueData)] = data
        
        let status = SecItemAdd(dict, nil)
        if status != errSecSuccess {
            NSLog("Unable add item with key =%@ error:%ld",key,status)
        }
    }
    
    func loadData(forKey key: String, inService service: String, returnType: ReturnType) -> AnyObject? {

        var dict = initialiseDictionary(key, service: service)
        dict[String(kSecMatchLimit)] = kSecMatchLimitOne
        
        switch returnType {
        case .Data:
            dict[String(kSecReturnData)] = kCFBooleanTrue
        case .PersistentRef:
            dict[String(kSecReturnPersistentRef)] = kCFBooleanTrue
        }
        
        var result: AnyObject?
        let status = withUnsafeMutablePointer(&result) { SecItemCopyMatching(dict, UnsafeMutablePointer($0)) }
        
        if status != errSecSuccess {
            NSLog("Unable to fetch item for key %@ with error:%ld",key,status);
            return nil;
        }
        
        return result
    }
}