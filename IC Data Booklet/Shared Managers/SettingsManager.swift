//
//  SettingsManager.swift
//  IC Data Booklet
//
//  Created by Mike on 30/06/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation

// access keys
private let kForceReload = "Force Reload"
private let kCompoundUnitPK = "Compound Unit PK"
private let kShouldSaveVPNPassword = "Should Save VPN Password"


public class SettingsManager {
    
    // ================ VARIABLES =================
    
    var settings = NSUserDefaults.standardUserDefaults()
    
    // ================ INITIALIZERS =================
    
    static let sharedManager = SettingsManager()
    
    // ================ SETTINGS =================
    
    var forceReload: Bool {
        set {
            settings.setBool(newValue, forKey: kForceReload)
        }
        get {
            return settings.boolForKey(kForceReload) ?? false
        }
    }
    
    var shouldSaveVPNPassword: Bool {
        set {
            settings.setBool(newValue, forKey: kShouldSaveVPNPassword)
        }
        get {
            print(settings.boolForKey(kShouldSaveVPNPassword))
            return settings.boolForKey(kShouldSaveVPNPassword) ?? true
        }
    }
    
    var nextCompoundUnitPK: Int {
        set {
            settings.setInteger(newValue, forKey: kCompoundUnitPK)
        }
        get {
            let pk = settings.integerForKey(kCompoundUnitPK)
            
            // pk of generated units must not be a possible pk of centrally-stored units to avoid overwrites, so negative values are used
            // in case of an unconfigured or incorrectly configured value, set and return the correct value or a default
            if pk > -1 {
                // load all compound units
                let predicate = NSPredicate(format: "isCompound=true", argumentArray: nil)
                let allCompounds = databaseManager.getAllEntitiesOfType(kDataUnit, withPredicate: predicate) as! [Unit]
                
                // find the current pk (most negative) from loaded units and step to the next pk
                // if no compound units exist use a default starter value of -1
                var newPK = -1
                if let minPK = allCompounds.sort({ $0.pk < $1.pk })[¿0]?.pk.integerValue {
                    newPK = minPK - 1
                }
                settings.setInteger(newPK, forKey: kCompoundUnitPK)
                return newPK
            }
            return pk
        }
    }
    
    // ================ FUNCTIONS =================
    
    func iterateToNextCompoundUnitPK() {
        nextCompoundUnitPK--
    }

}
