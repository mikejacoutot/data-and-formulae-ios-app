//
//  ConnectionManager.swift
//  Temp
//
//  Created by Mike on 29/12/2014.
//  Copyright (c) 2014 Michael Jacoutot. All rights reserved.
//

import Foundation
import Alamofire
import NetworkExtension
import CoreData
import UIKit
import MBProgressHUD


// define keys for the dictionaries returned by the server
private let kDictFormula = "Formulae"
private let kDictUnit = "Units"
private let kDictUnitCategory = "UnitCategories"
private let kDictImage = "Images"
private let kDictSymbol = "Symbols"
private let kDictGraph = "Graphs"
private let kDictGraphAxis = "GraphAxes"
private let kDictSteam = "Steam"
private let kDictFormulaCategory = "Categories"

private let kDictSuccess = "success"
private let kDictSolution = "solution"
private let kDictDelete = "delete"

// define server URLs
let kServerBaseURL = "http://192.168.0.3:8080/" //"http://155.198.43.125:8080/" //0.8:8080/"
private let kServerSync = "sync"
private let kServerSteam = "getSteamValues"
private let kServerFeedback = "feedback"
private let kServerEquation = "solveEquation"

public class ConnectionManager {
    
    // ================ VARIABLES =================
    
    var manager = NEVPNManager.sharedManager()
    
    // ================ INITIALIZERS =================
    
    static let sharedManager = ConnectionManager()
    
    // ================ DATABASE SYNC =================
    
    func syncWithDB(refresh: UIRefreshControl? = nil, force: Bool = false) {
        var params: [String:NSDictionary] = [:]
        
        if !force {
            let formulae = databaseManager.getAllEntitiesOfType(kDataFormula, withPredicate: nil) as! [Formula]
            let units = databaseManager.getAllEntitiesOfType(kDataUnit, withPredicate: NSPredicate(format: "isCompound=false", argumentArray: nil)) as! [Unit]
            let unitCategories = databaseManager.getAllEntitiesOfType(kDataUnitCategory, withPredicate: nil) as! [UnitCategory]
            let symbols = databaseManager.getAllEntitiesOfType(kDataSymbol, withPredicate: nil) as! [Symbol]
            let images = databaseManager.getAllEntitiesOfType(kDataImage, withPredicate: nil) as! [Image]
            let graphs = databaseManager.getAllEntitiesOfType(kDataGraph, withPredicate: nil) as! [Graph]
            let graphAxes = databaseManager.getAllEntitiesOfType(kDataGraphAxis, withPredicate: nil) as! [GraphAxis]
            let steam = databaseManager.getAllEntitiesOfType(kDataSteam, withPredicate: nil) as! [SteamTables]
            let formulaCategories = databaseManager.getAllEntitiesOfType(kDataFormulaCategory, withPredicate: nil) as! [FormulaCategory]
            let fTS = databaseManager.getTimeStampsOfEntities(formulae)
            let uTS = databaseManager.getTimeStampsOfEntities(units)
            let ucTS = databaseManager.getTimeStampsOfEntities(unitCategories)
            let sTS = databaseManager.getTimeStampsOfEntities(symbols)
            let iTS = databaseManager.getTimeStampsOfEntities(images)
            let gTS = databaseManager.getTimeStampsOfEntities(graphs)
            let gaTS = databaseManager.getTimeStampsOfEntities(graphAxes)
            let stTS = databaseManager.getTimeStampsOfEntities(steam)
            let fcTS = databaseManager.getTimeStampsOfEntities(formulaCategories)
            
            params = [kDictFormula:fTS, kDictUnit:uTS, kDictUnitCategory:ucTS, kDictSymbol:sTS, kDictImage:iTS, kDictGraph:gTS, kDictGraphAxis:gaTS, kDictSteam:stTS, kDictFormulaCategory:fcTS]
        }
        
    
        Alamofire.request(.POST, kServerBaseURL+kServerSync, parameters: params).responseJSON {
            [weak self] response in
            
            var hud: MBProgressHUD?
            if refresh == nil && response.result.error == nil {
                hud = self?.getHUD("Syncing Data")
            }
            
            if let returnedDictionary = response.result.value as? [String:AnyObject] {
                if returnedDictionary.count > 0 {
                    
                    self?.processUpdates(returnedDictionary, hud: hud)
                }
            }
            refresh?.endRefreshing()
        }
    }
    
    //checks if a server command was successful and solutions are present
    func checkSuccess(returnedData: [String:AnyObject?]?) -> (success: Bool, solution: [String:AnyObject]?) {
        if let unwrappedData = returnedData {
            if let success = unwrappedData[kDictSuccess] as? Bool where success == true {
                if let data = unwrappedData[kDictSolution] as? [String:AnyObject] {
                    return (true, data)
                }
            }
        }
        return (false, nil)
    }
    
    
    func processUpdates(returnedDict: [String:AnyObject], hud: MBProgressHUD?) {
        
        let formulae = returnedDict[kDictFormula] as? [[String:AnyObject]]
        let units = returnedDict[kDictUnit] as? [[String:AnyObject]]
        let symbols = returnedDict[kDictSymbol] as? [[String:AnyObject]]
        let unitCategories = returnedDict[kDictUnitCategory] as? [[String:AnyObject]]
        let images = returnedDict[kDictImage] as? [[String:AnyObject]]
        let graphs = returnedDict[kDictGraph] as? [[String:AnyObject]]
        let graphAxes = returnedDict[kDictGraphAxis] as? [[String:AnyObject]]
        let steam = returnedDict[kDictSteam] as? [[String:AnyObject]]
        let formulaCategories = returnedDict[kDictFormulaCategory] as? [[String:AnyObject]]
        
        if images != nil {
            for i in images!  {
                if let pk = i[kDataPK] as? NSNumber {
                    let editImage = databaseManager.getEntity(kDataImage, withID: pk, createIfMissing: true) as! Image
                    
                    let delete = i[kDictDelete]?.boolValue ?? false
                    if delete {
                        databaseManager.deleteObject(editImage)
                    } else {
                        editImage.lastModified = NSDate()
                        editImage.main = i["main"] as? NSNumber ?? 0
                        editImage.name = i["name"] as? String ?? ""
                        
                        if let identifier = i["image"] as? String {
                            downloadImage(identifier, image: editImage)
                        }
                    }
                }
            }
        }
        
        
        if graphAxes != nil {
            for ga in graphAxes! {
                if let pk = ga[kDataPK] as? NSNumber {
                    let editAxis = databaseManager.getEntity(kDataGraphAxis, withID: pk, createIfMissing: true) as! GraphAxis
                    
                    let delete = ga[kDictDelete]?.boolValue ?? false
                    if delete {
                        databaseManager.deleteObject(editAxis)
                    } else {
                        editAxis.lastModified = NSDate()
                        editAxis.title = ga["name"] as? String ?? ""
                        editAxis.type = ga["type"] as? NSNumber ?? AxisType.X.rawValue
                        editAxis.max = ga["max"] as? NSNumber ?? 1
                        editAxis.min = ga["min"] as? NSNumber ?? 0
                        editAxis.majorTick = ga["major"] as? NSNumber ?? 1
                        editAxis.minorTick = ga["minor"] as? NSNumber ?? 1
                        editAxis.logarithmic = ga["log"] as? Bool ?? false
                        
                        if let unit = ga["unit"] as? NSNumber {
                            editAxis.unit = databaseManager.getEntity(kDataUnit, withID: unit, createIfMissing: true) as! Unit
                        }
                    }
                }
            }
        }
        
        if graphs != nil {
            for g in graphs! {
                if let pk = g[kDataPK] as? NSNumber {
                    let editGraph = databaseManager.getEntity(kDataGraph, withID: pk, createIfMissing: true) as! Graph
                    
                    let delete = g[kDictDelete]?.boolValue ?? false
                    if delete {
                        databaseManager.deleteObject(editGraph)
                    } else {
                        editGraph.lastModified = NSDate()
                        editGraph.name = g["name"] as? String ?? ""
                        editGraph.isTable = g["isTable"] as? NSNumber ?? 0
                        
                        var axesSet = Set<GraphAxis>()
                        if let axes = g["axes"] as? [NSNumber] {
                            for a in axes {
                                axesSet += databaseManager.getEntity(kDataGraphAxis, withID: a, createIfMissing: true) as! GraphAxis
                            }
                        }
                        editGraph.axes = axesSet
                        
                        if let i = g["image"] as? NSNumber {
                            let image = databaseManager.getEntity(kDataImage, withID: i, createIfMissing: true) as! Image
                            editGraph.image = image
                        }
                    }
                }
            }
        }
        
        if formulaCategories != nil {
            for fc in formulaCategories! {
                if let pk = fc[kDataPK] as? NSNumber {
                    let editCategory = databaseManager.getEntity(kDataFormulaCategory, withID: pk, createIfMissing: true) as! FormulaCategory
                    
                    let delete = fc[kDictDelete]?.boolValue ?? false
                    if delete {
                        databaseManager.deleteObject(editCategory)
                    } else {
                        editCategory.lastModified = NSDate()
                        editCategory.name = fc["name"] as? String ?? ""
                        editCategory.aliases = fc["alias"] as? [String] ?? []
                    }
                }
            }
        }
        
        if formulae != nil {
            for f in formulae! {
                if let pk = f[kDataPK] as? NSNumber {
                    let editFormula = databaseManager.getEntity(kDataFormula, withID: pk, createIfMissing: true) as! Formula
                    
                    let delete = f[kDictDelete]?.boolValue ?? false
                    if delete {
                        databaseManager.deleteObject(editFormula)
                    } else {
                        editFormula.lastModified = NSDate()
                        editFormula.name = f["name"] as? String ?? ""
                        editFormula.summary = f["summary"] as? String ?? ""
                        editFormula.legal = f["legal"] as? String ?? ""
                        editFormula.tags = f["tags"] as? String ?? ""
                        
                        if let category = f["category"] as? NSNumber {
                            editFormula.category = databaseManager.getEntity(kDataFormulaCategory, withID: category, createIfMissing: true) as! FormulaCategory
                        }
                        
                        var symbolSet = Set<Symbol>()
                        if let symbols = f["symbols"] as? [NSNumber] {
                            for s in symbols {
                                symbolSet += databaseManager.getEntity(kDataSymbol, withID: s, createIfMissing: true) as! Symbol
                            }
                        }
                        editFormula.symbols = symbolSet
                        
                        var nomenclatureSet = Set<Symbol>()
                        if let nomenclature = f["nomenclature"] as? [NSNumber] {
                            for n in nomenclature {
                                nomenclatureSet += databaseManager.getEntity(kDataSymbol, withID: n, createIfMissing: true) as! Symbol
                            }
                        }
                        editFormula.nomenclatureSymbols = nomenclatureSet
                        
                        var graphSet = Set<Graph>()
                        if let graphs = f["graphs"] as? [NSNumber] {
                            for g in graphs {
                                graphSet += databaseManager.getEntity(kDataGraph, withID: g, createIfMissing: true) as! Graph
                            }
                        }
                        editFormula.graphs = graphSet
                        
                        var imageSet = Set<Image>()
                        if let images = f["images"] as? [NSNumber] {
                            for i in images {
                                imageSet += databaseManager.getEntity(kDataImage, withID: i, createIfMissing: true) as! Image
                            }
                        }
                        editFormula.images = imageSet
                        editFormula.hasMain = editFormula.hasImage()
                    }
                }
            }
        }
        
        if unitCategories != nil {
            for uc in unitCategories!  {
                if let pk = uc[kDataPK] as? NSNumber {
                    let editCategory = databaseManager.getEntity(kDataUnitCategory, withID: pk, createIfMissing: true) as! UnitCategory
                    
                    let delete = uc[kDictDelete]?.boolValue ?? false
                    if delete {
                        databaseManager.deleteObject(editCategory)
                        
                    } else {
                        editCategory.name = uc["name"] as? String ?? ""
                        editCategory.isDimensionless = uc["isDimensionless"] as? Bool ?? false
                        editCategory.lastModified = NSDate()
                        editCategory.hidden = uc["hidden"] as? Bool ?? false
                        
                        editCategory.compoundCategories = uc["compoundParams"] as? [[[Int]]] ?? []
                        
                        /*var symbolSet = Set<Symbol>()
                        if let symbols = uc["symbols"] as? [NSNumber] {
                            for s in symbols as [NSNumber] {
                                symbolSet += databaseManager.getEntity(kDataSymbol, withID: s, createIfMissing: true) as! Symbol
                            }
                        }
                        editCategory.symbols = symbolSet*/
                        
                        var unitSet = Set<Unit>()
                        if let units = uc["units"] as? [NSNumber] {
                            for u in units {
                                unitSet += databaseManager.getEntity(kDataUnit, withID: u, createIfMissing: true) as! Unit
                            }
                        }
                        editCategory.units = unitSet
                    }
                }
            }
        }
        
        if symbols != nil {
            for s in symbols! {
                if let pk = s[kDataPK] as? NSNumber {
                    let editSymbol = databaseManager.getEntity(kDataSymbol, withID: pk, createIfMissing: true) as! Symbol
                    
                    let delete = s[kDictDelete]?.boolValue ?? false
                    if delete {
                        databaseManager.deleteObject(editSymbol)
                    } else {
                        editSymbol.lastModified = NSDate()
                        editSymbol.name = s["name"] as? String ?? ""
                        editSymbol.onlyReal = s["onlyReal"] as? Bool ?? false
                        editSymbol.sympy = s["sympy"] as? String ?? ""
                        editSymbol.isConstant = s["isConstant"] as? Bool ?? false
                        editSymbol.constant = s["constant"] as? NSNumber ?? 0
                        editSymbol.display = s["display"] as? String ?? ""
                        
                        if let category = s["category"] as? NSNumber {
                            editSymbol.unitCategory = databaseManager.getEntity(kDataUnitCategory, withID: category, createIfMissing: true) as! UnitCategory
                        }
                        if let unit = s["constantUnit"] as? NSNumber {
                            editSymbol.unit = databaseManager.getEntity(kDataUnit, withID: unit, createIfMissing: true) as? Unit
                        }
                    }
                }
            }
        }
        
        if units != nil {
            for u in units!  {
                if let pk = u[kDataPK] as? NSNumber {
                    let editUnit = databaseManager.getEntity(kDataUnit, withID: pk, createIfMissing: true) as! Unit
                    
                    let delete = u[kDictDelete]?.boolValue ?? false
                    if delete {
                        databaseManager.deleteObject(editUnit)
                    } else {
                        editUnit.lastModified = NSDate()
                        editUnit.name = u["name"] as? String ?? ""
                        editUnit.pluralName = u["plural"] as? String ?? ""
                        editUnit.distinction = u["distinction"] as? String ?? ""
                        editUnit.multiplicand = u["multiplicand"] as? NSNumber ?? 1
                        editUnit.addend = u["addend"] as? NSNumber ?? 0
                        editUnit.isBaseUnit = u["isBaseUnit"] as? Bool ?? false
                        editUnit.display = u["units"] as? String ?? ""
                    }
                }
            }
        }
        
        if steam != nil {
            if steam!.count > 0 {
                let st = steam![0]
                if let pk = st[kDataPK] as? NSNumber {
                    let editSteam = databaseManager.getEntity(kDataSteam, withID: pk, createIfMissing: true) as! SteamTables
                    
                    let delete = st[kDictDelete]?.boolValue ?? false
                    if delete {
                        databaseManager.deleteObject(editSteam)
                    } else {
                        editSteam.lastModified = NSDate()
                        editSteam.calculations = st["calculations"] as? [String] ?? []
                        editSteam.parameters = st["parameters"] as? [String:NSNumber] ?? [:]
                        
                        var graphSet = Set<Graph>()
                        if let graphs = st["graphs"] as? [NSNumber] {
                            for g in graphs {
                                graphSet += databaseManager.getEntity(kDataGraph, withID: g, createIfMissing: true) as! Graph
                            }
                        }
                        editSteam.graphs = graphSet
                    }
                }
            }
        }
        
        databaseManager.saveContext()
        settingsManager.forceReload = false
        hud?.hide(true)
    }
    
    
    func downloadImage(identifier: String, image: Image) {
        // Might want to move this to a background thread
        Alamofire.request(.GET, kServerBaseURL+identifier, parameters: nil).response {
            (request, _, data, error) in
            
            if let imgData = data {
                image.image = imgData
                
                databaseManager.saveContext()
            } else {
                image.lastModified = NSDate(timeIntervalSince1970: 0)
            }
            if image.formulae != nil {
                image.formulae!.hasMain = image.formulae!.hasImage()
            }
            databaseManager.saveContext()
        }
    }
     
    // ================ EQUATION SOLVER =================
    
    func solveEquation(variables: [String:AnyObject], completion: SolverConnectionBlock) {
        let waitingHUD = self.getHUD("Solving Equation")
        
        Alamofire.request(.POST, kServerBaseURL+kServerEquation, parameters: variables).responseJSON {
            [weak self] response in
            let error = response.result.error
            
            if let returnedDictionary = response.result.value as? [String:AnyObject] {
                if let (success,solutions) = self?.checkEquationSuccess(returnedDictionary) {
                    completion(success: success, solutions: solutions, error: error)
                } else {
                    NSLog("ConnectionManager equation solver error: Could not read self")
                    completion(success: false, solutions: nil, error: error)
                }
            } else {
                NSLog("ConnectionManager equation solver error: Could not make dict")
                completion(success: false, solutions: nil, error: error)
            }
            waitingHUD.hide(true)
        }
    }
    
    func checkEquationSuccess(response: [String:AnyObject]) -> (success:Bool, solutions: [SolverSolution]) {
        var success = true
        if let err = response["error"] as? Bool {
            if err {
                success = false
            }
        } else {
            print("NO ERROR IN DICT")
        }
        if let n = response["number of solutions"] as? Int {
            if n == 0 {
                success = false
            }
        } else {
            print("NO NUMBER OF SOLUTIONS IN DICT")
            success = false
        }
        
        if let answers = response["answers"] as? [String:AnyObject] {
            var solutions: [SolverSolution] = []
            for key in answers.keys {
                if let answer = answers[key] as? [String:AnyObject] {
                    let index = NSString(string: key).integerValue
                    let real = answer["real"] as? Double ?? 0
                    let imag = answer["imaginary"] as? Double ?? 0
                    let complex = answer["complex"] as? Bool ?? false
                    let vars = answer["vars"] as? String ?? ""
                    let solution = SolverSolution(index: index, complex: complex, real: real, imag: imag, vars: vars)
                    solutions += [solution]
                }
            }
            // sort by index
            solutions.sortInPlace({ $0.index < $1.index })
            return (success, solutions)
        } else {
            print("COULD NOT RETRIEVE ANSWERS")
            success = false
        }
        
        return (false, [])
    }
    
    // ================ STEAM TABLES =================
    
    //call server to calculate steam values from two parameters
    //use completion handler to retrieve asynchronous networking request from Alamofire
    func getSteamResults(steamParameters: [String:Double], completion: SteamConnectionBlock) {

        Alamofire.request(.POST, kServerBaseURL+kServerSteam, parameters: steamParameters).responseJSON {
            response in
            let error = response.result.error
            
            //init variables to return
            var success = false
            var steamData: [Entity]? = nil
            
            if let returnedDictionary = response.result.value as? [String:AnyObject] {
                let (s, data) = self.checkSuccess(returnedDictionary)
                success = s
                if success { // success implies data != nil so we can forcibly unwrap
                    if let dataDict = data as? [String:Double] {
                        steamData = self.formatSteamDataFor(dataDict)
                    }
                }
            }
            completion(success: success, steamData: steamData, error: error)
        }
    }
    
    func formatSteamDataFor(returnedDictionary: [String:Double]) -> [Entity] {
        //format the data as an array of data classes for paramaters we are concerned with
        var steamDataArray: [Entity] = []
        let paramDict = SteamTables.sharedInstance().getParametersDict()
        
        for key in paramDict.keys {
            
            let entity = Entity(unit: paramDict[key]!, value: 0)
            entity.key = key
            if let returnedValue = returnedDictionary[key] { // search returned data for parameters from global array
                entity.value = returnedValue.roundTo(significantFigures: 5)
            } else {
                entity.shouldDisplayValueAsNil = true
            }
            steamDataArray += [entity] //compile array
        }
        return steamDataArray
    }
    
    
    
    // ================ FEEDBACK =================
    
    func sendFeedback(feedback: String, completion: (success:Bool, error: NSError?) -> ()) -> () {
        
        //let progressHUD = getHUD("Sending Feedback")
        
        Alamofire.request(.POST, kServerBaseURL+kServerFeedback, parameters: ["summary":feedback]).responseJSON {
            response in
            let error = response.result.error
            //init variables to return
            var success = false
            
            //if let returnedDictionary = response.result.value as? [String:AnyObject] {
                /*let (success, data: [String:AnyObject]?) = self!.checkSuccess(returnedDictionary)
                if success { // success implies data != nil so we can forcibly unwrap
                    if let dataDict = data as? [String:Double] {
                        steamData = self!.formatSteamDataFor(dataDict)
                    }
                }*/
                if error == nil {
                    success = true
                }
            //}
            completion(success: success, error: error)
        }
    }
    
    
    // ================ HUD WINDOW =================
    
    func getHUD(text: String?) -> MBProgressHUD! {
        if let window = UIApplication.sharedApplication().windows.last {
            
            let waitingHUD =  MBProgressHUD.showHUDAddedTo(window, animated: true) //MBProgressHUD(window: window)
            waitingHUD.color = UIColor.darkGrayColor()
            
            if text != nil {
                waitingHUD.labelText = text!
            }
            
            return waitingHUD
        }
        return nil
    }
}