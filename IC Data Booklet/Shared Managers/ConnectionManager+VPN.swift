//
//  ConnectionManager+VPN.swift
//  IC Data Booklet
//
//  Created by Mike on 30/06/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import NetworkExtension


// define VPN keys
let kICVPNServiceName = "ICVPNService"
let kICVPNPassword = "ICVPNPassword"
let kICVPNUsername = "ICVPNUserName"
let kICVPNUserAccount = "ICVPNUserAccount"
let kICVPNServerAddress = "vpn.imperial.ac.ak"

extension ConnectionManager {
    // ================ PERSONAL VPN =================
    // VPN FUNCTIONALITY NOT CURRENTLY WORKING - ALWAYS RETURNS ERROR 2
    func configureVPN(completion: ConnectionBlock) {
        manager.loadFromPreferencesWithCompletionHandler { loadError in
            print("load: \(loadError)")
            
            let p = NEVPNProtocol()
            
            if let username = keychainManager.vpnUsername, let passRef = keychainManager.vpnPasswordReference {
                p.username = username
                p.passwordReference = passRef
            } else {
                UIAlertView(title: "Oops!", message: "There was a problem when reading the login data. Please try again.", delegate: nil, cancelButtonTitle: "OK").show()
                completion(success: false, responseObject: nil, error: nil)
                return
            }
            
            p.serverAddress = kICVPNServerAddress
            p.disconnectOnSleep = false
            
            // Set protocol
            self.manager.`protocol` = p
            
            // Set on demand
            self.manager.onDemandEnabled = true
            var rules: [NEOnDemandRule] = []
            rules += [NEOnDemandRuleConnect()]
            self.manager.onDemandRules = rules
            
            // Set localized description
            self.manager.localizedDescription = "Imperial VPN"
            
            // Enable VPN
            self.manager.enabled = true
            
            // Save to preference
            self.manager.saveToPreferencesWithCompletionHandler({
                error in
                if error != nil {
                    UIAlertView(title: "Connection Error", message: "Unable to configure the VPN connection. Please try again or configure it manually in the Settings app.", delegate: nil, cancelButtonTitle: "OK").show()
                    completion(success: false, responseObject: nil, error: error)
                } else {
                    do {
                        try self.manager.connection.startVPNTunnel()
                    } catch let startError as NSError {
                        UIAlertView(title: "Connection Error", message: "Could not initiate the VPN connection. Please try again or configure it manually in the Settings app.", delegate: nil, cancelButtonTitle: "OK").show()
                        completion(success: false, responseObject: nil, error: startError)
                    } catch {
                        fatalError()
                    }
                    completion(success: true, responseObject: nil, error: nil)
                }
            })
        }
    }
}
