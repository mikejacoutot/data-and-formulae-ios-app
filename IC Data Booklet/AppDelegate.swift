 //
//  AppDelegate.swift
//  Temp
//
//  Created by Mike on 26/12/2014.
//  Copyright (c) 2014 Michael Jacoutot. All rights reserved.
//

import UIKit
import CoreData
 

let IS_IPAD = UIDevice.currentDevice().userInterfaceIdiom == .Pad
 
// global shared managers for convenience
let settingsManager     = SettingsManager.sharedManager
let keychainManager     = KeychainManager.sharedManager
let connectionManager   = ConnectionManager.sharedManager
let databaseManager     = DatabaseManager.sharedManager
let errorHandler        = ErrorHandler.sharedManager

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var rootContainer: RootViewContainer!
    var window: UIWindow?
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        ///(application as! QTouchposeApplication).alwaysShowTouches = true        
   
        UINavigationBar.appearance().tintColor = UIColor.blueColor()
        
        self.window?.makeKeyAndVisible()
        rootContainer = window!.rootViewController as! RootViewContainer
        
        
        let forceReload = settingsManager.forceReload
        connectionManager.syncWithDB(nil, force: forceReload)
        
        let pa = UIPageControl.appearance()
        pa.pageIndicatorTintColor = UIColor.lightGrayColor()
        pa.currentPageIndicatorTintColor = UIColor.grayColor()
        pa.backgroundColor = UIColor.clearColor()
        pa.hidesForSinglePage = true
        
        let ta = UINavigationBar.appearance()
        ta.tintColor = UIColor.blueColor()
    
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
        
        // if the user has opted not to save the vpn password, remove the reference from the keychain
        if !settingsManager.shouldSaveVPNPassword {
            keychainManager.vpnPassword = nil
        }
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "Michael-Jacoutot.Temp" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1] 
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("Temp", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("Temp.sqlite")

        // attempt to use automatic migrations
        let mOptions: [NSObject:AnyObject] = [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true]
        
        // if the database is not compatable, force a reload
        if let path = url.path {
            if NSFileManager.defaultManager().fileExistsAtPath(url.path!) {
                let existingPersistentStoreMetadata: [String: AnyObject]?
                do {
                    existingPersistentStoreMetadata = try NSPersistentStoreCoordinator.metadataForPersistentStoreOfType(NSSQLiteStoreType, URL: url)
                } catch let error as NSError {
                    existingPersistentStoreMetadata = nil
                } catch {
                    fatalError()
                }
                if let meta = existingPersistentStoreMetadata where self.managedObjectModel.isConfiguration(nil, compatibleWithStoreMetadata: meta) { /*success*/ } else {
                    // the database is not compatible so set a flag to reload all data from the server incase the device attempted to sync with the server before the app was updated
                    settingsManager.forceReload = true
                }
            }
        }
        
        do {
            try coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: mOptions)
        } catch let initError as NSError {
        
            // Check if we already have a persistent store
            if let path = url.path {
                if NSFileManager.defaultManager().fileExistsAtPath(url.path!) {
                    let existingPersistentStoreMetadata: [String: AnyObject]
                    do {
                        existingPersistentStoreMetadata = try NSPersistentStoreCoordinator.metadataForPersistentStoreOfType(NSSQLiteStoreType, URL: url)
                    } catch let error as NSError {
                        existingPersistentStoreMetadata = [:]
                        // something *really* bad has happened to the persistent store
                        NSException(name: NSInternalInconsistencyException, reason: "Failed to read metadata for persistent store \(url): \(error)", userInfo: error.userInfo).raise()
                    }
                    // try to remove the incompatible store
                    if !self.managedObjectModel.isConfiguration(nil, compatibleWithStoreMetadata: existingPersistentStoreMetadata) {
                        do {
                            try NSFileManager.defaultManager().removeItemAtURL(url)
                        } catch let error as NSError {
                            NSLog("*** Could not delete persistent store, \(error)")
                        } catch {
                            fatalError()
                        } 
                    }// else the existing persistent store is compatible with the current model
                } // else no database file yet
            }
            
            // retry loading/creating the store
            do {
                try coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: mOptions)
            } catch let error as NSError {
                NSLog("*** Could not create new persistent store, \(error)")
                abort()
            } catch {
                fatalError()
            }
        } catch {
            fatalError()
        }
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext? = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext()
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support
    func saveContext() {
        saveContext(self.managedObjectContext)
    }
    
    func saveContext(context: NSManagedObjectContext?) {
        if let moc = context {
            if moc.hasChanges {
                do {
                    try moc.save()
                } catch let error as NSError {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    NSLog("Unresolved error \(error), \(error.userInfo)")
                    abort()
                }
            }
        }
    }

}

