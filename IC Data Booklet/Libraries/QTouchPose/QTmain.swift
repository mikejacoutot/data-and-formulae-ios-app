//
//  main.swift
//  IC Data Booklet
//
//  Created by Mike on 09/06/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import UIKit

//UIApplicationMain(Process.argc, Process.unsafeArgv, NSStringFromClass(QTouchposeApplication.self), NSStringFromClass(AppDelegate.self))

/* Note: 

To enable:
* uncomment line above
* rename file to 'main.swift'
* comment out @UIApplicationMain in AppDelegate.swift
* uncomment line (application as! QTouchposeApplication).alwaysShowTouches = true in application didFinishLaunchingWithOptions

*/