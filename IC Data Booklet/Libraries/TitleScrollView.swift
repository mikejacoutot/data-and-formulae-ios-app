//
//  TitleScrollView.swift
//  Temp
//
//  Created by Mike on 27/03/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit

protocol TitleScrollViewDelegate {
    func titleScrollView(titleScrollView: TitleScrollView, shouldScrollToSelectedTitle title: String, atIndex index: Int) -> Bool
}

class TitleScrollView: UIScrollView {
    
    // ================ INITIALIZERS =================
    
    init(titles: [String]) {
        super.init(frame: CGRectZero)
        self.titles = titles
        setup()
    }
    
    init(mutableAttributedTitles: [NSMutableAttributedString]) {
        super.init(frame: CGRectZero)
        self.attributedTitles = mutableAttributedTitles
        setup()
    }
    
    init(attributedTitles: [NSAttributedString]) {
        super.init(frame: CGRectZero)
        var attrTitles: [NSMutableAttributedString] = []
        for title in attributedTitles {
            attrTitles += [NSMutableAttributedString(attributedString: title)]
        }
        self.attributedTitles = attrTitles
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // ================ VARIABLES =================
    
    var titles: [String] = [] {
        didSet {
            var attrTitles: [NSMutableAttributedString] = []
            for title in titles {
                attrTitles += [NSMutableAttributedString(string: title)]
            }
            self.attributedTitles = attrTitles
        }
    }
    
    var attributedTitles: [NSMutableAttributedString] = [] {
        didSet {
            self.configureForTitles(attributedTitles)
            self.selectTitle(currentPage, animated: false)
            pageControl?.numberOfPages = attributedTitles.count
        }
    }
    var tViews: [UILabel] = []
    var titleDelegate: TitleScrollViewDelegate?
    let labelOffset: CGFloat = IS_IPAD ? 60 : 30
    var currentPage: Int = 0 {
        didSet {
            pageControl?.currentPage = currentPage
        }
    }
    var pageControl: UIPageControl?
    
    var currentOrientation: UIDeviceOrientation = UIDevice.currentDevice().orientation
    
    // ================ VIEW CONTROL =================
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        self.userInteractionEnabled = true
        
        UIDevice.currentDevice().beginGeneratingDeviceOrientationNotifications()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "deviceOrientationDidChange:", name: UIDeviceOrientationDidChangeNotification, object: nil)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: "titleSwipedLeft:")
        swipeLeft.direction = .Left
        self.addGestureRecognizer(swipeLeft)
        let swipeRight = UISwipeGestureRecognizer(target: self, action: "titleSwipedRight:")
        swipeRight.direction = .Right
        self.addGestureRecognizer(swipeRight)
        self.selectTitle(currentPage, animated: false)
    }
    
    // ================ ORIENTATION NOTIFICATIONS =================
    
    func deviceOrientationDidChange(notification: NSNotification) {
        let orientation = UIDevice.currentDevice().orientation
        if orientation == .FaceUp || orientation == .FaceDown || orientation == .Unknown || currentOrientation == orientation {
            return
        }
        currentOrientation = orientation
        
        self.selectTitle(currentPage, animated: false)
        //self.contentInset = UIEdgeInsets(top: 0, left: self.frame.width / 2, bottom: 0, right: 0)
    }
    
    func configureForTitles(titles: [NSMutableAttributedString]) {
        if self.attributedTitles != titles {
            self.attributedTitles = titles
        }
        
        if tViews.count > titles.count {
            for var j = tViews.count-1; j >= titles.count; j-- {
                let tView = tViews.removeAtIndex(j)
                tView.removeFromSuperview()
            }
        }
        
        for i in 0..<titles.count {
            let title = titles[i]
            if let tView = tViews[¿i] {
                // if the label already exists, update it
                updateLabelForTitle(tView, title: title)
            } else {
                // otherwise create a new label
                let tView = UILabel()
                tViews += [tView]
                tView.lineBreakMode = NSLineBreakMode.ByClipping // occaisionally titles seem to extend 1-2px beyond the frame after sizeToFit()
                tView.userInteractionEnabled = true
                tView.tag = i
                
                // add gesture recognizers
                let tap = UITapGestureRecognizer(target: self, action: "titlePressed:")
                tView.addGestureRecognizer(tap)
                
                
                self.addSubview(tView)
                
                // set constraints
                tView.translatesAutoresizingMaskIntoConstraints = false
                self.addConstraint(NSLayoutConstraint(item: tView, attribute: .CenterY, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1, constant: 0))
                if i == 0 {
                    self.addConstraint(NSLayoutConstraint(item: tView, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .Leading, multiplier: 1, constant: 0))
                } else {
                    if i == 0 {
                        self.addConstraint(NSLayoutConstraint(item: tView, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .Leading, multiplier: 1, constant: 0))
                    } else {
                        self.addConstraint(NSLayoutConstraint(item: tView, attribute: .Leading, relatedBy: .Equal, toItem: tViews[i-1], attribute: .Trailing, multiplier: 1, constant: labelOffset))
                    }
                }
                updateLabelForTitle(tView, title: title)
                self.contentSize = CGSizeZero
                //self.contentSize = CGSize(width: max(tView.frame.maxX - tViews[0].frame.minX,self.frame.width + 1), height: self.frame.height)
            }
        }
        self.layoutIfNeeded()
        self.selectTitle(0, animated: false)
    }
    
    func updateLabelForTitle(label: UILabel, title: NSAttributedString) {
        label.attributedText = title
        label.sizeToFit()
        label.frame.size.width += 2
        label.updateConstraintsIfNeeded()
    }
    
    internal func titlePressed(sender: UITapGestureRecognizer) {
        if let label = sender.view as? UILabel {
            shouldSelectTitle(label)
        }
    }
    
    internal func titleSwipedLeft(sender: UISwipeGestureRecognizer) {
        if let label = tViews[¿currentPage+1] {
            shouldSelectTitle(label)
        }
    }
    
    internal func titleSwipedRight(sender: UISwipeGestureRecognizer) {
        if let label = tViews[¿currentPage-1] {
            shouldSelectTitle(label)
        }
    }
    
    private func shouldSelectTitle(label: UILabel) {
        let index = label.tag
        
        if titleDelegate?.titleScrollView(self, shouldScrollToSelectedTitle: label.text ?? "", atIndex: index) ?? true {
            self.selectTitle(index, animated: true)
        }
    }
    
    func selectTitle(index: Int, animated: Bool) {
        if self.tViews.count == 0 || self.attributedTitles.count == 0 {
            NSLog("titlescrollview cant select title ")
            return
        }
        
        let newPage: Int = {
            let max = self.tViews.count - 1
            if index < 0 {
                return 0
            } else if index >= max {
                return max
            } else {
                return index
            }
            }()
        
        //let offset = tViews[newPage].frame.midX

        if animated {
            let duration: NSTimeInterval = IS_IPAD ? 0.3 : 0.2
            UIView.animateWithDuration(duration, animations: {
                self.updateTitles(newPage)
            })
        } else {
            self.updateTitles(newPage)
            self.updateConstraintsIfNeeded()
        }
        self.currentPage = newPage
    }
    
    func updateTitles(newPage: Int) {
        let offset = tViews[newPage].frame.midX
        self.contentOffset = CGPoint(x: offset - self.frame.width/2, y: 0)
        
        for i in 0..<self.tViews.count {
            let tView = self.tViews[i]
            if i == newPage {
                tView.attributedText = tView.attributedText!.underlined(.StyleSingle)
                tView.layer.opacity = 1
            } else {
                tView.attributedText = tView.attributedText!.underlined(.StyleNone)
                tView.layer.opacity = 0.3
            }
        }
    }
}
