//
//  TransitionManager.swift
//  IC Data Booklet
//
//  Created by Mike on 11/10/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import UIKit


enum PanDirection {
    case LeftToRight
    case RightToLeft
    case TopToBottom
    case BottomToTop
}


class TransitionManager: UIPercentDrivenInteractiveTransition, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate {
    
    // ================ VARIABLES =================
    
    var presenting = false
    var interactive = false
    internal var completedInitialConfiguration = false
    
    weak internal var sourceViewController: UIViewController!
    weak internal var destinationViewController: UIViewController!
    weak internal var container: UIView!
    
    internal var exitGesturesForSourceView: [UIGestureRecognizer] = []
    internal var exitGesturesForDestinationView: [UIGestureRecognizer] = []
    
    // ================ FUNCTIONS TO OVVERIDE IN SUBCLASS =================
    
    func presentingAnimations(sourceView: UIView, destinationView: UIView, container: UIView) {
        assert(false, "A subclass of TransitionManager must provide presenting and dismissing animations!")
    }
    
    func dismissingAnimations(sourceView: UIView, destinationView: UIView, container: UIView) {
        assert(false, "A subclass of TransitionManager must provide presenting and dismissing animations!")
    }
    
    // one-off configuration called immediately before the transition to the destinationViewController begins. Override this function to apply constraints, shadows and other elements which will persist through the presentation and dismissal.
    func configureForPresentation(sourceView: UIView, destinationView: UIView, container: UIView) {}
    
    // final (de)configuration called only when the destinationViewController has been dismissed. Override this function to undo any customisation of the sourveView in configureForPresentation: that should not persist after the destinationView is dismissed.
    func configureForDismissal(sourceView: UIView, destinationView: UIView, container: UIView) {}
    
    // called at the end of any transition. Override this function to configure the transition manager for completion or cancellation of the presenting or dismissing transitions
    func completedTransition(transitionContext: UIViewControllerContextTransitioning, cancelled: Bool, presenting: Bool) {}
    
    // ================ GENERAL FUNCTIONS =================
    
    private func initialConfiguration(sourceView: UIView, destinationView: UIView, container: UIView, remove: Bool) {
        if !remove && !self.completedInitialConfiguration {
            configureForPresentation(sourceView, destinationView: destinationView, container: container)
            self.completedInitialConfiguration = true
            
        } else if remove {
            configureForDismissal(sourceView, destinationView: destinationView, container: container)
            self.completedInitialConfiguration = false
            
            // release assets
            self.destinationViewController = nil
            self.sourceViewController = nil
            self.container = nil
            //self.exitGesturesForDestinationView = []
            //self.exitGesturesForSourceView = []
        }
    }
    
    func handleInteractivePanGesture(pan: UIPanGestureRecognizer, panDirection: PanDirection, completionLength: CGFloat) {
        let referenceView: UIView
        if let view = container {
            referenceView = view
        } else if let view = pan.view?.superview {
            referenceView = view
        } else if let view = pan.view {
            referenceView = view
        } else {
            referenceView = UIApplication.sharedApplication().keyWindow!.rootViewController!.view
        }
        
        //deal with different states that the gesture recognizer sends
        switch (pan.state) {
            
        case UIGestureRecognizerState.Began:
            NSLog("Gesture \(pan) returned a state of Began! handleInteractivePanGesture is intended to manage a pan gesture handler for a transition that has already started. You should initiate the required for state = .Began and pass any other state through to this handler")
            break
            
        case UIGestureRecognizerState.Changed:
            let translation = pan.translationInView(referenceView) // get panned distance
            var t: CGFloat
            
            switch panDirection {
            case .LeftToRight: t = translation.x
            case .RightToLeft: t = -translation.x
            case .TopToBottom: t = translation.y
            case .BottomToTop: t = -translation.y
            }
            
            let percentCompletion = CGFloat(Double(t / completionLength).limitedToBounds(0, max: 1))
            
            self.updateInteractiveTransition(percentCompletion)
            break
            
        default: // .Ended, .Cancelled, .Failed ...
            // finish or cancel transition depending on translation
            self.interactive = false
            
            let velocity = pan.velocityInView(referenceView)
            let v: CGFloat
            
            switch panDirection {
            case .LeftToRight: v = velocity.x
            case .RightToLeft: v = -velocity.x
            case .TopToBottom: v = velocity.y
            case .BottomToTop: v = -velocity.y
            }
            
            if(v > 0){
                self.finishInteractiveTransition()
            }
            else {
                self.cancelInteractiveTransition()
            }
        }
        
    }
    
    // ================ UIViewControllerAnimatedTransitioning =================
    
    // animate a change from one viewcontroller to another
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        let container = transitionContext.containerView()!
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
        let fromView = transitionContext.viewForKey(UITransitionContextFromViewKey)!
        let toView = transitionContext.viewForKey(UITransitionContextToViewKey)!
        
        let sourceViewController = self.presenting ? fromViewController : toViewController
        let destinationViewController = self.presenting ? toViewController : fromViewController
        
        self.sourceViewController = sourceViewController
        self.destinationViewController = destinationViewController
        self.container = container
        
        let sourceView =  self.presenting ? fromView : toView
        let destinationView = self.presenting ? toView : fromView
        
        // add the both views to our view controller
        container.addSubview(destinationView)
        container.addSubview(sourceView)
        
        if (self.presenting){ // prepare views for presentation
            self.dismissingAnimations(sourceView, destinationView: destinationView, container: container)
            self.initialConfiguration(sourceView, destinationView: destinationView, container: container, remove: false)
        }
        
        let duration = self.transitionDuration(transitionContext)
        let options: UIViewAnimationOptions = interactive ? .CurveLinear : .CurveEaseInOut
        
        // perform the animation
        UIView.animateWithDuration(duration, delay: 0.0, options: options, animations: {
            
            if (self.presenting){
                self.presentingAnimations(sourceView, destinationView: destinationView, container: container)
            } else {
                self.dismissingAnimations(sourceView, destinationView: destinationView, container: container)
            }
            
            }, completion: { finished in
                // tell our transitionContext object that we've finished animating
                if(transitionContext.transitionWasCancelled()){
                    
                    // call endAppearanceTransition to avoid unbalanced calls errors for UINavigationControllers (and probably certain other controllers) within the view heirarchy. For some reason this is not called automatically.
            
                    sourceViewController.endAppearanceTransition()
                    
                    transitionContext.completeTransition(false)
                    
                    if self.presenting {
                        self.dismissingAnimations(sourceView, destinationView: destinationView, container: container)
                        self.initialConfiguration(sourceView, destinationView: destinationView, container: container, remove: true)
                        //self.statusBarBackground.hidden = true
                    } else {
                        self.presentingAnimations(sourceView, destinationView: destinationView, container: container)
                    }
                    self.completedTransition(transitionContext, cancelled: true, presenting: self.presenting)
                } else {
                    
                    transitionContext.completeTransition(true)
                    
                    if (self.presenting){
                        UIApplication.sharedApplication().keyWindow?.addSubview(sourceView)
                        
                        // presentation complete so add all required return gestures
                        sourceView.addGestureRecognizers(self.exitGesturesForSourceView)
                        destinationView.addGestureRecognizers(self.exitGesturesForDestinationView)
                        
                    } else {
                        // remove exit gestures
                        sourceView.removeGestureRecognizers(self.exitGesturesForSourceView)
                        destinationView.removeGestureRecognizers(self.exitGesturesForDestinationView)
                        
                        self.initialConfiguration(sourceView, destinationView: destinationView, container: container, remove: true)
                    }
                    self.completedTransition(transitionContext, cancelled: false, presenting: self.presenting)
                }
        })
        
    }
    
    
    // return how many seconds the transiton animation will take
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.3
    }
    
    // ================ UIViewControllerTransitioningDelegate =================
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.presenting = true
        return self
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.presenting = false
        return self
    }
    
    func interactionControllerForPresentation(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return self.interactive ? self : nil
    }
    
    func interactionControllerForDismissal(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return self.interactive ? self : nil
    }
    
}