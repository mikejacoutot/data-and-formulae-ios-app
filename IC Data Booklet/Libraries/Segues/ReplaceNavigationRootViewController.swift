//
//  ReplaceNavigationRootViewController.swift
//  IC Data Booklet
//
//  Created by Mike on 09/10/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation

// segue to replace the existing navigation stack of a UINavigationController

class ReplaceNavigationRootViewControllerSegue: UIStoryboardSegue {
    
    var animated = IS_IPAD
    
    override func perform() {
        if let navVC = sourceViewController as? UINavigationController {
            navVC.setViewControllers([destinationViewController], animated: animated)
            navVC.viewControllers = [destinationViewController]
        } else {
            self.showDetailViewController()
        }
    }
}