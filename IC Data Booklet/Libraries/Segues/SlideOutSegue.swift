//
//  SlideOutSegue.swift
//  IC Data Booklet
//
//  Created by Mike on 09/10/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation

enum SlideOutDirection {
    case FromRight, FromLeft
}

enum SlideOutMode {
    case MoveOffscreen, Resize
}

class SlideOutSegue: UIStoryboardSegue {
    
    var direction: SlideOutDirection = .FromLeft
    var mode: SlideOutMode = IS_IPAD ? .Resize : .MoveOffscreen
    var destinationViewWidth: CGFloat = 220
    
    weak var transitionManager: SlideOutTransitionManager!
    
    override func perform() {
        // limit offset to upper and lower bounds to ensure both views are visible
        let windowFrame = UIApplication.sharedApplication().keyWindow!.frame
        let smallestScreenWidth = min(windowFrame.width, windowFrame.height)
        let largestWidth: CGFloat = smallestScreenWidth - 55
        let smallestWidth: CGFloat = 55 // currently based on the show menu button size
        
        // set transitionmanager if a default was not already supplied
        if transitionManager == nil {
            destinationViewWidth = destinationViewWidth.limitedToBounds(smallestWidth, max: largestWidth)
            transitionManager = SlideOutTransitionManager(direction: direction, mode: mode, destinationViewWidth: destinationViewWidth)
        } else {
            // retrieve settings from pre-supplied transition manager
            transitionManager.destinationViewWidth = transitionManager.destinationViewWidth.limitedToBounds(smallestWidth, max: largestWidth)
            self.destinationViewWidth = transitionManager.destinationViewWidth
            self.direction = transitionManager.direction
            self.mode = transitionManager.mode
        }
        
        destinationViewController.transitionManager = transitionManager
        destinationViewController.transitioningDelegate = transitionManager
        
        self.presentViewController(nil)
    }
    
}


class SlideOutTransitionManager: TransitionManager {
    
    // ================ INITIALIZERS =================
    
    init(direction: SlideOutDirection, mode: SlideOutMode, destinationViewWidth: CGFloat) {
        self.direction = direction
        self.mode = mode
        self.destinationViewWidth = destinationViewWidth
        
        super.init()
        
        switch mode {
        case .MoveOffscreen:
            let exitTapGesture = UITapGestureRecognizer(target: self, action: "handleExitTap:")
            exitTapGesture.numberOfTapsRequired = 1
            exitTapGesture.cancelsTouchesInView = false
            
            let exitPanGesture = UIPanGestureRecognizer(target: self, action: "handleExitPan:")
            
            self.exitGesturesForSourceView = [exitTapGesture, exitPanGesture]
            
        case .Resize:
            let exitScreenEdgePan = ViewEdgePanGestureRecognizer(target: self, action:"handleExitPan:")
            switch direction {
            case .FromLeft: exitScreenEdgePan.edges = .Left
            case .FromRight: exitScreenEdgePan.edges = .Right
            }
            self.exitGesturesForSourceView = [exitScreenEdgePan]
        }
    }
    
    // ================ VARIABLES =================
    
    var direction: SlideOutDirection
    var mode: SlideOutMode
    
    var destinationViewWidth: CGFloat
    var destinationSlideTranslationFactor: CGFloat = 1
    
    // ================ GESTURE RECOGNIZERS =================
    
    
    func handleEnterPan(gesture: UIPanGestureRecognizer){
        
        //deal with different states that the gesture recognizer sends
        switch (gesture.state) {
            
        case UIGestureRecognizerState.Began:
            self.interactive = true
            break
            
        default: // .Changed, .Ended, .Cancelled, .Failed ...
            let panDirection: PanDirection
            switch direction {
            case .FromLeft: panDirection = .LeftToRight
            case .FromRight: panDirection = .RightToLeft
            }
            self.handleInteractivePanGesture(gesture, panDirection: panDirection, completionLength: destinationViewWidth)
        }
    }
    
    func handleExitPan(gesture: UIPanGestureRecognizer){
        switch (gesture.state) {
            
        case .Began:
            self.interactive = true
            self.destinationViewController.dismissViewControllerAnimated(true, completion: nil)
            break
            
        default: // .Changed, .Ended, .Cancelled, .Failed ...
            let panDirection: PanDirection
            switch direction {
            case .FromLeft: panDirection = .RightToLeft
            case .FromRight: panDirection = .LeftToRight
            }
            self.handleInteractivePanGesture(gesture, panDirection: panDirection, completionLength: destinationViewWidth)
        }
    }
    
    func handleExitTap(gesture: UITapGestureRecognizer!){
        self.interactive = false
        self.destinationViewController.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // ================ UIViewControllerAnimatedTransitioning =================
    
    override func presentingAnimations(sourceView: UIView, destinationView: UIView, container: UIView){
        
        switch mode {
        case .Resize:
            
            var insets = UIEdgeInsetsZero
            switch direction {
            case .FromLeft: insets.left = destinationViewWidth
            case .FromRight: insets.right = destinationViewWidth
            }
            sourceView.frame = UIEdgeInsetsInsetRect(container.frame, insets)
            for subview in sourceView.subviews {
                subview.layoutIfNeeded()
            }
            sourceView.layer.shadowPath = UIBezierPath(rect: sourceView.frame).CGPath
            
        case .MoveOffscreen:
            let sourceTranslation: CGFloat
            switch direction {
            case .FromRight: sourceTranslation = -destinationViewWidth
            case .FromLeft: sourceTranslation = destinationViewWidth
            }
            
            sourceView.transform = CGAffineTransformMakeTranslation(sourceTranslation, 0)
        }
        
        destinationView.transform = CGAffineTransformIdentity
        
        //self.statusBarBackground?.backgroundColor = destinationView.backgroundColor
    }
    
    override func dismissingAnimations(sourceView: UIView, destinationView: UIView, container: UIView){
        
        var destinationTranslation: CGFloat
        switch direction {
        case .FromRight: destinationTranslation = destinationViewWidth
        case .FromLeft: destinationTranslation = -destinationViewWidth
        }
        
        destinationTranslation *= destinationSlideTranslationFactor
        
        switch mode {
        case .Resize:
            sourceView.frame = container.frame
            for subview in sourceView.subviews {
                subview.layoutIfNeeded()
            }
            sourceView.layer.shadowPath = UIBezierPath(rect: sourceView.frame).CGPath
        case .MoveOffscreen:
            sourceView.transform = CGAffineTransformIdentity
        }
        destinationView.transform = CGAffineTransformMakeTranslation(destinationTranslation, 0)
    }
    
    override func configureForPresentation(sourceView: UIView, destinationView: UIView, container: UIView) {
        destinationView.translatesAutoresizingMaskIntoConstraints = false
        
        // get the side of the screen to which the destinationView should be pinned
        let sideAttribute: NSLayoutAttribute
        switch direction {
        case .FromRight: sideAttribute = .Trailing
        case .FromLeft: sideAttribute = .Leading
        }
        
        // constrain the destinationView to a width of 'destinationViewWidth' and pinned to the side from which it will appear
        container.addConstraint(NSLayoutConstraint(item: container, attribute: sideAttribute, relatedBy: .Equal, toItem: destinationView, attribute: sideAttribute, multiplier: 1, constant: 0))
        container.addConstraint(NSLayoutConstraint(item: destinationView, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: self.destinationViewWidth))
        container.addConstraint(NSLayoutConstraint(item: container, attribute: .Top, relatedBy: .Equal, toItem: destinationView, attribute: .Top, multiplier: 1, constant: 0))
        container.addConstraint(NSLayoutConstraint(item: container, attribute: .Bottom, relatedBy: .Equal, toItem: destinationView, attribute: .Bottom, multiplier: 1, constant: 0))
        
        // set a shadow on the sourceView to add depth and write an explicit shadowPath to improve performance
        sourceView.layer.shadowOpacity = 1
        sourceView.layer.shadowPath = UIBezierPath(rect: sourceView.frame).CGPath
    }
    
    override func configureForDismissal(sourceView: UIView, destinationView: UIView, container: UIView) {
        sourceView.layer.shadowOpacity = 0
    }
    
    
}