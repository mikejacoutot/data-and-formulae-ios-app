//
//  NavigationTransitionOperator.swift
//  
//
//  Created by Mike on 08/02/2015.
//
//

import Foundation
import UIKit

class ZoomInTransitionManager: UIPercentDrivenInteractiveTransition, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate {
    
    // ================ INITIALIZERS =================
    
    init(sourceViewController: UIViewController, forSegueWithIdentifier identifier: String, returnSegueIdentifier: String? = nil){
        super.init()
        self.setSourceViewController(sourceViewController)
        self.segueIdentifier = identifier
        self.returnSegueIdentifier = returnSegueIdentifier
        self.setOffset(defaultOffset)
    }
    
    // set method so that didSet is called during initialization
    private func setSourceViewController(sourceViewController: UIViewController) {
        self.sourceViewController = sourceViewController
    }
    
    private func setOffset(offset: CGFloat?){
        self.offset = offset == nil ? self.defaultOffset : offset!
    }
    
    // ================ VARIABLES =================
    
    var animated = true
    
    private var presenting = false
    private var interactive = false
    
    var segueIdentifier: String!
    var returnSegueIdentifier: String?
    
    // select width of previous view to show (as 'offset') depending on device idiom
    private var defaultOffset: CGFloat = UIDevice.currentDevice().userInterfaceIdiom == .Pad ? 120 : 60
    var offset: CGFloat! {
        didSet {
            if let windowFrame = UIApplication.sharedApplication().keyWindow?.frame {
                let smallestWidth = min(windowFrame.width, windowFrame.height)
                let minOffset = 0.9 * smallestWidth
                if self.offset < minOffset {
                    self.offset = minOffset
                }
            }
        }
    }
    var scaleFact: CGFloat = 0.6
    
    private var enterPanGesture: UIPanGestureRecognizer!
    private var exitPanGesture: UIScreenEdgePanGestureRecognizer!
    private var exitTapGesture: UITapGestureRecognizer!
    
    private var constraintsAdded = false
    
    var sourceViewController: UIViewController! {
        didSet {
            // setup entry pan for the sourceViewController
            self.enterPanGesture = UIPanGestureRecognizer()
            self.enterPanGesture.addTarget(self, action:"handleEnterPan:")
            self.sourceViewController.view.addGestureRecognizer(self.enterPanGesture)
            
            // configure exit tap for the sourceViewController, but don't add it until the presentation segue completes
            self.exitTapGesture = UITapGestureRecognizer()
            self.exitTapGesture.addTarget(self, action: "handleExitTap:")
            self.exitTapGesture.numberOfTapsRequired = 1
            self.exitTapGesture.cancelsTouchesInView = false
        }
    }
    
    var destinationViewController: UIViewController! {
        didSet {
            self.exitPanGesture = UIScreenEdgePanGestureRecognizer()
            self.exitPanGesture.addTarget(self, action:"handleExitPan:")
            self.exitPanGesture.edges = UIRectEdge.Left
            self.destinationViewController.view.addGestureRecognizer(self.exitPanGesture)
        }
    }
    
    // ================ GESTURE RECOGNIZERS =================
    
    func handleEnterPan(pan: UIPanGestureRecognizer){
        let translation = pan.translationInView(pan.view!) // get panned distance
        
        let fact = (pan.view!.window!.bounds.width - offset) / pan.view!.window!.bounds.width
        
        let d = fact * -translation.x / pan.view!.bounds.width // translate this into a percentage-based distance
        
        //deal with different states that the gesture recognizer sends
        switch (pan.state) {
            
        case UIGestureRecognizerState.Began:
            self.interactive = true
            self.sourceViewController.performSegueWithIdentifier(segueIdentifier, sender: self) //trigger transition
            break
            
        case UIGestureRecognizerState.Changed:
            self.updateInteractiveTransition(d)
            break
            
        default: // .Ended, .Cancelled, .Failed ...
            // finish or cancel transition depending on translation
            self.interactive = false
            if(d > 0.2){
                self.finishInteractiveTransition()
            }
            else {
                self.cancelInteractiveTransition()
            }
        }
    }
    
    func handleExitPan(pan: UIPanGestureRecognizer){
        
        let translation = pan.translationInView(pan.view!)
        
        let fact = (pan.view!.window!.bounds.width - offset) / pan.view!.window!.bounds.width
        
        let d =  fact * translation.x / pan.view!.bounds.width * 0.55
        
        switch (pan.state) {
            
        case UIGestureRecognizerState.Began:
            self.interactive = true
            if returnSegueIdentifier != nil {
                self.destinationViewController.performSegueWithIdentifier(returnSegueIdentifier!, sender: self)
            } else {
                self.destinationViewController.dismissViewControllerAnimated(true, completion: nil)
            }
            break
            
        case UIGestureRecognizerState.Changed:
            self.updateInteractiveTransition(d)
            break
            
        default: // .Ended, .Cancelled, .Failed ...
            self.interactive = false
            if(d > 0.1){
                self.finishInteractiveTransition()
            }
            else {
                self.cancelInteractiveTransition()
            }
        }
    }
    
    func handleExitTap(sender: UITapGestureRecognizer!){
        self.interactive = false
        if returnSegueIdentifier != nil {
            self.destinationViewController.performSegueWithIdentifier(returnSegueIdentifier!, sender: self)
        } else {
            self.destinationViewController.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    // ================ UIViewControllerAnimatedTransitioning =================
    
    // animate a change from one viewcontroller to another
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        let container = transitionContext.containerView()!
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
        
        let sourceViewController = self.presenting ? fromViewController : toViewController
        let destinationViewController = self.presenting ? toViewController : fromViewController
        let sourceView = sourceViewController.view
        let destinationView = destinationViewController.view
        
        // add the both views to our view controller
        container.addSubview(sourceViewController.view)
        container.addSubview(destinationViewController.view)
        
        
        if (self.presenting){ // prepare views for presentation
            self.dismissingAnimations(sourceViewController, destinationViewController: destinationViewController)
            
            self.applyConstraints(sourceView, destinationView: destinationView, container: container, remove: false)
        }
        
        let duration = self.transitionDuration(transitionContext)
        
        // perform the animation
        UIView.animateWithDuration(duration, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 0.8, options: [], animations: {
            
            if (self.presenting){
                self.presentingAnimations(sourceViewController, destinationViewController: destinationViewController, container: container)
            } else {
                self.dismissingAnimations(sourceViewController, destinationViewController: destinationViewController)
            }
            
            }, completion: { finished in
                // tell our transitionContext object that we've finished animating
                if(transitionContext.transitionWasCancelled()){
                    transitionContext.completeTransition(false)
                    
                    if self.presenting {
                        // bug: we have to manually add the view back:
                        UIApplication.sharedApplication().keyWindow?.addSubview(fromViewController.view)
                    }
                } else {
                    transitionContext.completeTransition(true)
                    
                    if (self.presenting){
                        self.destinationViewController = toViewController
                        self.sourceViewController.view.addGestureRecognizer(self.exitTapGesture)
                    } else {
                        self.animated = true
                        self.applyConstraints(sourceView, destinationView: destinationView, container: container, remove: true)
                        
                        // bug: we have to manually add the view back:
                        UIApplication.sharedApplication().keyWindow?.addSubview(toViewController.view)
                        
                        self.sourceViewController.view.removeGestureRecognizer(self.exitPanGesture)
                        //self.sourceViewController.view.removeGestureRecognizer(self.exitTapGesture)
                    }
                }
        })
        
    }
    
    func presentingAnimations(sourceViewController: UIViewController, destinationViewController: UIViewController, container: UIView){
        
        let invScaleFact = 1 / scaleFact
        let translateDistance = ((0.97) * sourceViewController.view.window!.frame.width)/2 - offset
        
        var zoomInTransform = CGAffineTransformMakeScale(invScaleFact, invScaleFact)
        zoomInTransform = CGAffineTransformTranslate(zoomInTransform, -translateDistance, 0)
        
        //sourceViewController.view.transform = zoomInTransform
        destinationViewController.view.transform = CGAffineTransformIdentity
        
        
        for subview in sourceViewController.view.subviews {
            subview.layer.opacity = 0
            subview.transform = zoomInTransform
        }
    }
    
    func dismissingAnimations(sourceViewController: UIViewController, destinationViewController: UIViewController){
        
        let w = UIApplication.sharedApplication().keyWindow!.frame.width
        
        let translateDistance = ((1 + scaleFact) * w)/2 - offset
        
        var zoomInTransform = CGAffineTransformMakeTranslation(translateDistance, 0)
        zoomInTransform = CGAffineTransformScale(zoomInTransform, scaleFact, scaleFact)
        
        //sourceViewController.view.transform = CGAffineTransformIdentity
        destinationViewController.view.transform = zoomInTransform
        
        for subview in sourceViewController.view.subviews {
            subview.layer.opacity = 1
            subview.transform = CGAffineTransformIdentity
        }
    }
    
    
    func applyConstraints(sourceView: UIView, destinationView: UIView, container: UIView, remove: Bool) {
        if !remove {
            if !self.constraintsAdded {
                
                
                self.constraintsAdded = true
            }
        } else {
            if self.constraintsAdded {
                
                
                self.constraintsAdded = false
            }
        }
    }
    
    
    
    // return how many seconds the transiton animation will take
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return self.animated ? 0.5 : 0
    }
    
    // ================ UIViewControllerTransitioningDelegate =================
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.presenting = true
        return self
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.presenting = false
        return self
    }
    
    func interactionControllerForPresentation(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return self.interactive ? self : nil
    }
    
    func interactionControllerForDismissal(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return self.interactive ? self : nil
    }
    
}