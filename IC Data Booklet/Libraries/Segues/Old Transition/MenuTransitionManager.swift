//
//  MenuTransitionManager.swift
//  Temp
//
//  Created by Mike on 04/03/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import UIKit

class MenuTransitionManager: UIPercentDrivenInteractiveTransition, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate {
    
    // ================ INITIALIZERS =================
    
    init(sourceViewController: UIViewController, forSegueWithIdentifier identifier: String, returnSegueIdentifier: String? = nil){
        super.init()
        self.setSourceViewController(sourceViewController)
        self.segueIdentifier = identifier
        self.returnSegueIdentifier = returnSegueIdentifier
        self.setOffset(defaultOffset)
        
        self.exitPanGesture = UIScreenEdgePanGestureRecognizer()
        self.exitPanGesture.addTarget(self, action:"handleExitPan:")
        self.exitPanGesture.edges = UIRectEdge.Left
        
        self.enterTapGesture = UITapGestureRecognizer()
        self.enterTapGesture.addTarget(self, action: "handleEnterTap:")
        self.enterTapGesture.numberOfTapsRequired = 1
        self.enterTapGesture.cancelsTouchesInView = false
        
        self.enterPanGesture = UIPanGestureRecognizer()
        self.enterPanGesture.addTarget(self, action:"handleEnterPan:")
    }
    
    // set methods so that didSet is called during initialization
    private func setSourceViewController(sourceViewController: UIViewController) {
        self.sourceViewController = sourceViewController
    }
    private func setOffset(offset: CGFloat?){
        self.offset = offset == nil ? self.defaultOffset : offset!
    }
    
    // ================ VARIABLES =================
    
    var animated = true
    private var cancelledAnimation = false
    
    enum presentationType: Int {
        case Present, Dismiss, Return
    }
    
    private var status: presentationType = .Present
    var interactive = false
    var segueIdentifier: String!
    var returnSegueIdentifier: String?
    
    // select width of previous view to show (as 'offset') depending on device idiom
    private var defaultOffset: CGFloat = UIDevice.currentDevice().userInterfaceIdiom == .Pad ? 120 : 60
    var offset: CGFloat! {
        didSet {
            if let windowFrame = UIApplication.sharedApplication().keyWindow?.frame {
                let smallestWidth = min(windowFrame.width, windowFrame.height)
                let minOffset = 0.9 * smallestWidth
                if self.offset < minOffset {
                    self.offset = minOffset
                }
            }
        }
    }
    var scaleFact: CGFloat = 0.6
    
    private var enterPanGesture: UIPanGestureRecognizer!
    private var exitPanGesture: UIScreenEdgePanGestureRecognizer!
    private var enterTapGesture: UITapGestureRecognizer!
    
    private var constraintsAdded = false
    
    var sourceViewController: UIViewController!
    var destinationViewController: UIViewController!
    var transitionContext: UIViewControllerContextTransitioning!
    
    func performSegueWithIdentifier(identifier: String, sender: AnyObject?) {
        // if we are already in the required transition, simply resume it, otherwise complete the current transition and immediately begin the next
        if identifier == segueIdentifier  && self.transitionContext != nil  {
            status = .Return
            self.manageAnimations(completion: nil)
        } else {
            transitionContext?.completeTransition(true)
            self.transitionContext = nil
            self.segueIdentifier = identifier
            self.destinationViewController = nil
            
            if sourceViewController.childViewControllers.count != 0 {
                sourceViewController.dismissViewControllerAnimated(true, completion: {
                    self.sourceViewController.performSegueWithIdentifier(identifier, sender: nil)
                })
            } else {
                sourceViewController.performSegueWithIdentifier(identifier, sender: nil)
            }
        }
    }
    
    
    // ================ UIViewControllerAnimatedTransitioning =================
    
    func percentAnimation(var x: CGFloat) {
        if x < 0 { x = 0 }
        if x > 1 { x = 1 }
        
        let w = UIApplication.sharedApplication().keyWindow!.frame.width
        
        let scale = 1 - (1 - x)*(1 - scaleFact)
        let trans = (1 - x) * (((1 + scaleFact) * w) / 2 - offset)
        let transform = CGAffineTransformScale(CGAffineTransformMakeTranslation(trans, 0),scale,scale)
        
        let invscale = scale / scaleFact
        let invtrans = x * (((0.97) * sourceViewController.view.window!.frame.width)/2 - offset)
        
        let invtransform = CGAffineTransformTranslate(CGAffineTransformMakeScale(invscale, invscale), -invtrans, 0) //CGAffineTransformScale(CGAffineTransformMakeTranslation(-invtrans, 0),invscale,invscale)
        
        let opacity = Float(1 - (0.9 * x))
        
        destinationViewController.view.transform = transform
        
        for subview in sourceViewController.view.subviews {
            subview.layer.opacity = opacity
            subview.transform = invtransform
        }
    }
    
    
    func presentingAnimations(){
        percentAnimation(1)
    }
    
    func dismissingAnimations(){
        percentAnimation(0)
    }
    
    // animate a change from one viewcontroller to another
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        self.transitionContext = transitionContext
        
        let container = transitionContext.containerView()!
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
        
        if self.destinationViewController == nil {
            self.destinationViewController = self.status == .Present ? toViewController : fromViewController
        }
        
        // add the both views to our view controller
        container.addSubview(sourceViewController.view)
        container.addSubview(destinationViewController.view)
        if self.status == .Present { // prepare views for presentation
            self.dismissingAnimations()
        }
        
        manageAnimations(nil, completion: nil)
    }
    
    // performs the relevant animation for the state and any required setup after completion
    func manageAnimations(var duration: NSTimeInterval? = nil, completion: (()->())?) {
        if duration == nil {
            duration = self.transitionDuration(self.transitionContext!)
        }
        
        // perform the animation
        UIView.animateWithDuration(duration!, delay: 0.0, options: .CurveEaseInOut , animations: {
            switch self.status {
            case .Dismiss: self.dismissingAnimations()
            default: self.presentingAnimations()
            }
            
            }, completion: { finished in
                completion?() // call completion handler to perform any required setup before completion functions are called
                
                // tell our transitionContext object that we've finished animating
                if self.transitionContext.transitionWasCancelled() || self.cancelledAnimation {
                    switch self.status {
                    case .Present:
                        self.transitionContext.completeTransition(false)
                        UIApplication.sharedApplication().keyWindow?.addSubview(self.sourceViewController.view)
                    case .Dismiss:
                        self.transitionContext.completeTransition(false)
                        UIApplication.sharedApplication().keyWindow?.addSubview(self.destinationViewController.view)
                        self.destinationViewController.view.removeGestureRecognizer(self.enterTapGesture)
                        self.destinationViewController.view.removeGestureRecognizer(self.enterPanGesture)
                        self.destinationViewController.view.addGestureRecognizer(self.exitPanGesture)
                    case .Return:
                        self.destinationViewController.view.removeGestureRecognizer(self.exitPanGesture)
                        self.destinationViewController.view.addGestureRecognizer(self.enterTapGesture)
                        self.destinationViewController.view.addGestureRecognizer(self.enterPanGesture)
                    }
                    
                } else {
                    switch self.status {
                    case .Present:
                        self.transitionContext.completeTransition(true)
                        self.destinationViewController.view.addGestureRecognizer(self.exitPanGesture)
                        self.destinationViewController.view.userInteractionEnabled = true
                        break
                    case .Dismiss:
                        self.destinationViewController.view.removeGestureRecognizer(self.exitPanGesture)
                        self.destinationViewController.view.addGestureRecognizer(self.enterTapGesture)
                        self.destinationViewController.view.addGestureRecognizer(self.enterPanGesture)
                        self.status = .Return
                        for subview in self.destinationViewController.view.subviews {
                            subview.userInteractionEnabled = false
                        }
                        break
                    case .Return:
                        self.destinationViewController.view.removeGestureRecognizer(self.enterTapGesture)
                        self.destinationViewController.view.removeGestureRecognizer(self.enterPanGesture)
                        self.destinationViewController.view.addGestureRecognizer(self.exitPanGesture)
                        self.transitionContext.completeTransition(false)
                        self.destinationViewController.view.userInteractionEnabled = true
                        self.status = .Dismiss
                        for subview in self.destinationViewController.view.subviews {
                            subview.userInteractionEnabled = true
                        }
                        break
                    }
                }
                // ensure user interaction is enabled (is disabled by default for a non-interactive transition, e.g button press, which has not yet completed context)
                if UIApplication.sharedApplication().isIgnoringInteractionEvents() {
                    UIApplication.sharedApplication().endIgnoringInteractionEvents()
                }
                self.cancelledAnimation = false
        })
        
    }
    
    // return how many seconds the transiton animation will take
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return self.animated ? 0.5 : 0
    }
    
    // ================ GESTURE RECOGNIZERS =================
    
    func handleEnterPan(pan: UIPanGestureRecognizer){
        let translation = pan.translationInView(pan.view!) // get panned distance
        let fact: CGFloat = 1 //(pan.view!.window!.bounds.width - offset) / pan.view!.window!.bounds.width
        let d = fact * -translation.x / pan.view!.bounds.width // translate this into a percentage-based distance
        
        //deal with different states that the gesture recognizer sends
        switch (pan.state) {
            
        case UIGestureRecognizerState.Began:
            self.interactive = true
            self.status = .Return
            break
            
        case UIGestureRecognizerState.Changed:
            self.percentAnimation(d)
            break
            
        default: // .Ended, .Cancelled, .Failed ...
            // finish or cancel transition depending on translation
            self.interactive = false
            self.cancelledAnimation = d < 0.1
            
            // get the corrected percent distance as the distance to animate depending on wether the animation is to be completed or cancelled
            let d_corr = self.cancelledAnimation ? d : (1-d)
            
            // calculate the corrected time duration to complete the animation at the current velocity, plus a small buffer to keep it smooth
            let v = pan.velocityInView(pan.view!)
            let t_corr = NSTimeInterval(abs(translation.x / v.x) + 0.1)
            
            // if the duration is greater than the standard time that would be given by the transition context for the remaining distance, take the standard time.
            let t_min = self.transitionDuration(transitionContext) * NSTimeInterval(d_corr)
            let t = min(t_corr, t_min)
            
            if !self.cancelledAnimation {
                self.manageAnimations(t, completion: nil)//self.finishInteractiveTransition()
            }
            else {
                // perform the dismissal animations but the return to the Return state as soon as animations are complete
                self.status = .Dismiss
                self.cancelledAnimation = true
                self.manageAnimations(t, completion: {
                    self.status = .Return
                })
            }
        }
    }
    
    func handleExitPan(pan: UIPanGestureRecognizer){
        
        let translation = pan.translationInView(pan.view!.window!)
        let d = translation.x  / pan.view!.window!.bounds.width// * 0.55
        
        switch (pan.state) {
        case UIGestureRecognizerState.Began:
            self.interactive = true
            if returnSegueIdentifier != nil {
                self.destinationViewController.performSegueWithIdentifier(returnSegueIdentifier!, sender: self)
            } else {
                self.destinationViewController.dismissViewControllerAnimated(true, completion: nil)
            }
            break
            
        case UIGestureRecognizerState.Changed:
            self.updateInteractiveTransition(d)
            break
            
        default: // .Ended, .Cancelled, .Failed ...
            self.interactive = false
            if(d > 0.2){
                self.finishInteractiveTransition()
            } else {
                self.cancelInteractiveTransition()
            }
        }
    }
    
    func handleEnterTap(sender: UITapGestureRecognizer!){
        self.interactive = false
        self.status = .Return
        self.manageAnimations(completion: nil)
    }
    
    // ================ UIViewControllerTransitioningDelegate =================
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.status = .Present
        return self
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.status = .Dismiss
        return self
    }
    
    func interactionControllerForPresentation(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return self.interactive ? self : nil
    }
    
    func interactionControllerForDismissal(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return self.interactive ? self : nil
    }
    
}