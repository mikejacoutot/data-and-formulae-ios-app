//
//  PopOutTransitionOperator.swift
//  NavTest
//
//  Created by Mike on 21/02/2015.
//  Copyright (c) 2015 Mike. All rights reserved.
//

import UIKit

class PopOutTransitionOperator: NSObject, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate {
   
    var isPresenting : Bool = true
    var returnTapRecognizer: UITapGestureRecognizer?
    var returnPanRecognizer: UIPanGestureRecognizer?
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.5
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        if isPresenting{
            presentNavigation(transitionContext)
        } else {
            dismissNavigation(transitionContext)
        }
    }
    
    func presentNavigation(transitionContext: UIViewControllerContextTransitioning) {
        
        let container = transitionContext.containerView()!
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)
        let fromView = fromViewController!.view
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)
        let toView = toViewController!.view
    
        /*// 1 - Create transparent background view (to dim the background) and constrain it to the window
        let x = max(fromView.frame.width, fromView.frame.height)
        fromView.maskView = UIView(frame: CGRect(origin: fromView.frame.origin, size: CGSize(width: x, height: x)))
        fromView.maskView?.layer.backgroundColor = UIColor.grayColor().CGColor
        fromView.maskView?.layer.opacity = 1*/
        
        // 2 - Add the pop out view to the container and constrain it
        container.addSubview(toView)
        toView.translatesAutoresizingMaskIntoConstraints = false
        
        // lower-priority (preferred) size constraints
        let sizeThatFits = toView.frame.size
        let preferredSize = toViewController!.preferredContentSize
        let bestHeight = preferredSize.height == 0 ? sizeThatFits.height : preferredSize.height
        let heightConstraint = NSLayoutConstraint(item: toView, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: bestHeight)
        heightConstraint.priority = 800
        container.addConstraint(heightConstraint)
        let bestWidth = preferredSize.width == 0 ? sizeThatFits.width : preferredSize.width
        let widthConstraint = NSLayoutConstraint(item: toView, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: bestWidth)
        widthConstraint.priority = 800
        container.addConstraint(widthConstraint)
        
        // required centering and padding constraints
        container.addConstraint(NSLayoutConstraint(item: toView, attribute: .CenterX, relatedBy: .Equal, toItem: container, attribute: .CenterX, multiplier: 1, constant: 0))
        container.addConstraint(NSLayoutConstraint(item: toView, attribute: .CenterY, relatedBy: .Equal, toItem: container, attribute: .CenterY, multiplier: 1, constant: 0))
        
        container.addConstraint(NSLayoutConstraint(item: toView, attribute: .Leading, relatedBy: .GreaterThanOrEqual, toItem: container, attribute: .Leading, multiplier: 1, constant: 20))
        container.addConstraint(NSLayoutConstraint(item: toView, attribute: .Top, relatedBy: .GreaterThanOrEqual, toItem: container, attribute: .Top, multiplier: 1, constant: 20))

        toView.roundToRadius(10) // round pop out window
        
        // 3 - add gesture recognizers to dismiss the view
        returnTapRecognizer = UITapGestureRecognizer(target: toViewController!, action: "dismissViewControllerForTapOutside:")
        returnTapRecognizer!.numberOfTapsRequired = 1
        returnTapRecognizer!.cancelsTouchesInView = false
        toView.window?.addGestureRecognizer(returnTapRecognizer!)
        
        returnPanRecognizer = UIPanGestureRecognizer(target: toViewController!, action: "dismissViewControllerAnimated(true, completion:nil)")
        //returnPa
        
        // 4 - set up transform
        let offsetTransform = CGAffineTransformMakeTranslation(0, (toView.frame.height+fromView.frame.height)/2)
        
        toView.transform = offsetTransform
        
        let duration = self.transitionDuration(transitionContext)
        
        // 5 - perform animated transforms
        UIView.animateWithDuration(duration, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: [], animations: {

            toView.transform = CGAffineTransformIdentity
            fromView.alpha = 0.7//fromView.maskView?.layer.opacity = 0.7
            
            }, completion: { finished in
                transitionContext.completeTransition(true)
        })
    }
    
    func dismissNavigation(transitionContext: UIViewControllerContextTransitioning) {
        
        //let container = transitionContext.containerView()!
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)
        let fromView = fromViewController!.view
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)
        let toView = toViewController!.view
        
        let offsetTransform = CGAffineTransformMakeTranslation(0, (toView.frame.height+fromView.frame.height)/2)
        
        let duration = self.transitionDuration(transitionContext)
        
        UIView.animateWithDuration(duration * 0.8, delay: 0.0, options: [], animations: {

            fromView.transform = offsetTransform
            toView.alpha = 1
            
            }, completion: { finished in
                transitionContext.completeTransition(true)
                self.returnTapRecognizer?.view?.removeGestureRecognizer(self.returnTapRecognizer!)
                toView.maskView = nil
        })
    }
    
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        self.isPresenting = true
        return self
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        self.isPresenting = false
        return self
    }
    
}


