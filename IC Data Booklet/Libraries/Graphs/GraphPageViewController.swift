//
//  FormulaGraphViewController.swift
//  Temp
//
//  Created by Mike on 19/03/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit

class GraphPageViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource, GraphButtonDelegate {
    
    init(graphs: [Graph], transitionStyle style: UIPageViewControllerTransitionStyle, navigationOrientation: UIPageViewControllerNavigationOrientation, options: [String : AnyObject]?) {
        super.init(transitionStyle: style, navigationOrientation: navigationOrientation, options: options)
        self.graphs = graphs
    }
    
    init(graphs: [Graph]) {
        super.init(transitionStyle: .Scroll, navigationOrientation: .Horizontal, options: nil)
        self.graphs = graphs
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    // ================ VARIABLES =================

    var currentIndex : Int = 0
    var graphs: [Graph] = []
    
    var savedDataPoints: [[Entity]] = [] {
        didSet {
            for page in self.childViewControllers as! [GraphViewController] {
                page.dataArray = savedDataPoints
            }
        }
    }
 
    // ================ VIEW CONTROLLER =================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let firstGraphController = self.viewControllerAtIndex(0) {
            let viewControllers = [firstGraphController]
            self.view.backgroundColor = UIColor.whiteColor()

            self.delegate = self
            self.dataSource = self
            
            self.setViewControllers(viewControllers, direction: .Forward, animated: true, completion: {done in })
            self.view.frame = self.view.bounds
            self.didMoveToParentViewController(self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // ================ PAGE VIEW CONTROLLER DATA SOURCE =================
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! GraphViewController).index
        
        if (index == 0) || (index == NSNotFound) { return nil }
        
        index--
        return viewControllerAtIndex(index)
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! GraphViewController).index
        if index == NSNotFound {return nil}
        index++
        if (index == self.graphs.count) {return nil}
        
        return viewControllerAtIndex(index)
    }
    
    func viewControllerAtIndex(index: Int) -> GraphViewController? {
        if self.graphs.count == 0 || index >= self.graphs.count { return nil }
        
        // Create a new view controller and pass suitable data.
        let graphController = GraphViewController(graph: graphs[index])
        graphController.dataArray = savedDataPoints
        graphController.index = index
        graphController.delegate = self // override button delegate to configure all graphs simultaneously
        currentIndex = index
        
        return graphController
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return self.graphs.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    // ================ GRAPH VIEW CONTROLLER DELEGATE =================
    
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let vcs = pageViewController.viewControllers ?? []
        
        if vcs.count > 0 {
            let vc = vcs[0] as? GraphViewController
            currentIndex = vc?.index ?? 0
        }
    }
    
    // ================ GRAPH VIEW CONTROLLER DELEGATE =================
    
    func graphButton(shouldDeleteDataAtIndex index: Int) {
        self.savedDataPoints[index] = []
    }

}
