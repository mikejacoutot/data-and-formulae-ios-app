//
//  GraphViewController+GestureRecognizers.swift
//  IC Data Booklet
//
//  Created by Mike on 21/09/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import CorePlot

extension GraphViewController {
    
    // ================ GESTURE RECOGNIZERS =================
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if graphPinchGesture == nil || graphTwoFingerDragGesture == nil {
            return false
        } else if gestureRecognizer == graphPinchGesture && otherGestureRecognizer == graphTwoFingerDragGesture {
            return true
        } else if gestureRecognizer == graphTwoFingerDragGesture && otherGestureRecognizer == graphPinchGesture {
            return true
        }
        return false
    }
    
    // ================ DOUBLE TAP ZOOM OUT =================
    
    func handleDoubleTap(gesture: UITapGestureRecognizer!) {
        
        // test if tap is within the graph image (true) or on the axes (false)
        let isWithinImage = imageViewContainer.pointInside(gesture.locationInView(imageViewContainer), withEvent: nil)
        // test if the graph is currently less tha half zoomed
        let isUnderHalfZoom = self.defaultPlotSpace.xRange.length.doubleValue > minScaleFactor
        
        if isWithinImage && isUnderHalfZoom {
            zoomToMaximum(gesture)
        } else {
            zoomToDefault()
        }
    }
    
    func zoomToMaximum(gesture: UITapGestureRecognizer) {
        let duration: CGFloat = 0.3
        
        // location appears to be based on original (untransformed) size so take the imageViewContainer size
        let size = imageViewContainer.frame.size
        let p = gesture.locationInView(imageView)
        
        // fractional location on graph image
        let pt = CGPoint(x: p.x/size.width, y: 1 - p.y/size.height)
        
        // convert to length
        let length = CGFloat(minScaleFactor)
        let xRange = CPTPlotRange(location: pt.x - length/2, length: length)
        let yRange = CPTPlotRange(location: pt.y - length/2, length: length)
        
        CPTAnimation.animate(defaultPlotSpace, property: "xRange", fromPlotRange: defaultPlotSpace.xRange, toPlotRange: xRange, duration: duration)
        CPTAnimation.animate(defaultPlotSpace, property: "yRange", fromPlotRange: defaultPlotSpace.yRange, toPlotRange: yRange, duration: duration)
    }
    
    func zoomToDefault() {
        let duration: CGFloat = 0.3
        
        CPTAnimation.animate(defaultPlotSpace, property: "xRange", fromPlotRange: defaultPlotSpace.xRange, toPlotRange: defaultPlotSpace.globalXRange!, duration: duration)
        CPTAnimation.animate(defaultPlotSpace, property: "yRange", fromPlotRange: defaultPlotSpace.yRange, toPlotRange: defaultPlotSpace.globalYRange!, duration: duration)
    }
    
    // ================ TWO FINGER DRAG/PAN =================
    
    // custom implementation for a two finger gesture to control the graph
    // default coreplot gestures are lacking in the ability to work with a UIPageViewController and correctly handle logarithmic axes
    func handleTwoFingerDrag(gesture: UIPanGestureRecognizer) {
        
        // read translation
        let t = gesture.translationInView(view)
        let size = imageView.frame.size
        
        // read current fractional range
        var fx = defaultPlotSpace.xRange.dafRange(false)
        var fy = defaultPlotSpace.yRange.dafRange(false)
        
        fx = fx.shifted(-t.x/size.width)
        fy = fy.shifted(t.y/size.height)
        
        // apply new ranges
        defaultPlotSpace.xRange = fx.CPTRange
        defaultPlotSpace.yRange = fy.CPTRange
        
        // reset translation
        gesture.setTranslation(CGPointZero, inView: view)
    }
    
    // ================ TWO FINGER PINCH =================
    
    // custom implementation for a pinch gesture as CorePlot does not handle logarithmic axes properly at the moment
    func handlePinch(gesture: UIPinchGestureRecognizer) {
        if self.graphValidation.success {
            
            // read scale 
            let scale = Double(1/gesture.scale)
            
            // get fractional ranges from current plotspace ranges
            var fx = defaultPlotSpace.xRange.dafRange(true)
            var fy = defaultPlotSpace.yRange.dafRange(true)
            
            // apply new scale to each fractional range (note the scale should be equal for each coordinate)
            let newLength = max(scale * fx.length, minScaleFactor)
            fx.length = newLength
            fy.length = newLength
            
            /*// crop to bounds to slow the approach to maximum magnification near the bounds
            fx = fx.limitedToBounds(0, max: 1)
            fy = fy.limitedToBounds(0, max: 1)*/
            
            // apply new ranges
            defaultPlotSpace.xRange = fx.CPTRange
            defaultPlotSpace.yRange = fy.CPTRange
            
            // reset scale
            gesture.scale = 1
        }
    }
    
}
