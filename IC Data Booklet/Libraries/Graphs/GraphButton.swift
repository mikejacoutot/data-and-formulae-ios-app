//
//  SteamGraphButton.swift
//  Temp
//
//  Created by Mike on 03/01/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit
import QuartzCore

protocol GraphButtonDelegate {
    func graphButton(shouldDeleteDataAtIndex index: Int)
}

class GraphButton: UIButton {
    
    // ================ INITIALIZERS =================
    
    convenience init(entities: [Entity], index: Int, delegate: GraphButtonDelegate?){
        
        
        let buttonFrame = CGRect(x: 0, y: 0, width: 35, height: 35)
        self.init(frame: buttonFrame)

        self.entities = entities
        self.delegate = delegate
        self.index = index
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialConfiguration()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialConfiguration()
    }
    
    private func initialConfiguration() {
        // add target to call the popup data table on init
        self.addTarget(self, action: "callDataTable:", forControlEvents: .TouchUpInside)
    }
    
    // ================ VARIABLES =================
    
    var delegate: GraphButtonDelegate?
    
    var index: Int = 0 {
        didSet {
            self.setTitle("\(index + 1)", forState: .Normal)
        }
    }
    
    var entities: [Entity] = []
    
    
    // Button generation parameters
    let colors: (top:UIColor,bottom:UIColor) = (UIColor.greenColor(),UIColor(red: 64/255, green: 145/255, blue: 84/255, alpha: 1.0))
    let border: (color:UIColor,width:CGFloat,radius:CGFloat) = (UIColor.grayColor(),2,20)
    let innerGlow: (color:UIColor,width:CGFloat,offset:CGFloat) = (UIColor(white: 1.0, alpha: 0.5),1,2)
    
    // ================ GENERAL FUNCTIONS =================
    
    func callDataTable(sender: GraphButton) {
        let dataTable = PopoverController.dataTable(entities, popoverAnchor: self, cancelHandler: nil, deleteHandler: {
            alert in
            self.delegate?.graphButton(shouldDeleteDataAtIndex: self.index)
        })
        
        dataTable.show()
    }
    
    // ================ CUSTOM DRAWING =================
    
    override func drawRect(rect: CGRect) {
        // General Declarations
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let context = UIGraphicsGetCurrentContext()
        let width = self.frame.width / self.transform.a
        let height = self.frame.height / self.transform.a
        
        // Gradient Declarations
        let gradientColors = [colors.top.CGColor,colors.bottom.CGColor]
        let gradient = CGGradientCreateWithColors(colorSpace, gradientColors, nil)
        
        //CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)(gradientColors), NULL);
        
        let highlightedGradientColors = [colors.bottom.CGColor,colors.top.CGColor]
        let highlightedGradient = CGGradientCreateWithColors(colorSpace, highlightedGradientColors, nil)
        
        // Draw rounded rectangle bezier path
        let roundedRectanglePath = UIBezierPath(roundedRect: CGRect(x: 0,y: 0,width: width,height: height), cornerRadius: border.radius)
        
        // Use the bezier as a clipping path
        roundedRectanglePath.addClip()
        
        // Use one of the two gradients depending on the state of the button
        let background = self.highlighted ? highlightedGradient : gradient
        
        // Draw gradient within the path
        CGContextDrawLinearGradient(context, background, CGPoint(x: width/2, y: 0), CGPoint(x: width/2, y: height), [])
        
        // Draw Inner Glow
        var innerRadius = border.radius-innerGlow.offset
        if innerRadius < 0 { innerRadius = 0 }
        
        let innerGlowRect = UIBezierPath(roundedRect: CGRect(x: innerGlow.offset, y: innerGlow.offset, width: width - innerGlow.offset*2, height: height - innerGlow.offset*2), cornerRadius: innerRadius)
        innerGlow.color.setStroke()
        innerGlowRect.lineWidth = innerGlow.width
        innerGlowRect.stroke()
        
        // Draw border
        border.color.setStroke()
        roundedRectanglePath.lineWidth = border.width
        roundedRectanglePath.stroke()
    }
    
    // ================ GESTURE RECOGNIZERS =================
    
    // redraw rect if touches begin/end
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        self.setNeedsDisplay()
    }

    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesEnded(touches, withEvent: event)
        self.setNeedsDisplay()
    }
}
