//
//  AxisStyles.swift
//  IC Data Booklet
//
//  Created by Mike on 27/07/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import CorePlot

struct AxisStyles {
    var title: CPTTextStyle
    var line: CPTLineStyle
    var text: CPTTextStyle
    var tickLine: CPTLineStyle
    var gridLine: CPTLineStyle
}

extension AxisStyles {
    
    var axisLabelFont: UIFont! {
        return UIFont(name: text.fontName!, size: text.fontSize)
    }
    
    static var Default: AxisStyles {
        let axisTitleStyle = CPTMutableTextStyle()
        axisTitleStyle.color = CPTColor.blackColor()
        axisTitleStyle.fontName = "Helvetica-Bold"
        axisTitleStyle.fontSize = 12
        let axisLineStyle = CPTMutableLineStyle()
        axisLineStyle.lineWidth = 2
        axisLineStyle.lineColor = CPTColor.blackColor()
        let axisTextStyle = CPTMutableTextStyle()
        axisTextStyle.color = CPTColor.blackColor()
        axisTextStyle.fontName = "Helvetica-Bold"
        axisTextStyle.fontSize = 11
        let tickLineStyle = CPTMutableLineStyle()
        tickLineStyle.lineColor = CPTColor.blackColor()
        tickLineStyle.lineWidth = 2
        let gridLineStyle = CPTMutableLineStyle()
        gridLineStyle.lineColor = CPTColor.blackColor()
        gridLineStyle.lineWidth = 1
        
        return AxisStyles(title: axisTitleStyle, line: axisLineStyle, text: axisTextStyle, tickLine: tickLineStyle, gridLine: gridLineStyle)
    }
    
    static var None: AxisStyles {
        let axisTitleStyle = CPTMutableTextStyle()
        axisTitleStyle.color = CPTColor.clearColor()
        axisTitleStyle.fontName = "Helvetica-Bold"
        axisTitleStyle.fontSize = 1
        let axisLineStyle = CPTMutableLineStyle()
        axisLineStyle.lineWidth = 1
        axisLineStyle.lineColor = CPTColor.clearColor()
        let axisTextStyle = CPTMutableTextStyle()
        axisTextStyle.color = CPTColor.clearColor()
        axisTextStyle.fontName = "Helvetica-Bold"
        axisTextStyle.fontSize = 1
        let tickLineStyle = CPTMutableLineStyle()
        tickLineStyle.lineColor = CPTColor.clearColor()
        tickLineStyle.lineWidth = 1
        let gridLineStyle = CPTMutableLineStyle()
        gridLineStyle.lineColor = CPTColor.clearColor()
        gridLineStyle.lineWidth = 1
        
        return AxisStyles(title: axisTitleStyle, line: axisLineStyle, text: axisTextStyle, tickLine: tickLineStyle, gridLine: gridLineStyle)
    }
}