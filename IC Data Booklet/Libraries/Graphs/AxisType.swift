//
//  AxisType.swift
//  IC Data Booklet
//
//  Created by Mike on 27/07/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import CorePlot

enum AxisType: Int {
    case X = 0
    case Y = 1
    case SecondaryY = 2
}


extension AxisType {
    var coordinate: CPTCoordinate? {
        switch self {
        case .X:
            return .X
        default:
            return .Y
        }
    }
    
    static func typeForCoordinate(coordinate: CPTCoordinate) -> AxisType? {
        switch coordinate {
        case .X:
            return .X
        case .Y:
            return .Y
        default:
            return nil
        }
    }
}