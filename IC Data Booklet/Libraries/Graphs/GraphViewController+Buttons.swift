//
//  GraphViewController+Methods.swift
//  IC Data Booklet
//
//  Created by Mike on 18/06/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import UIKit

extension GraphViewController {
    
    // ================ DATA POINT BUTTON SETUP =================
    
    //adds a button to the graph for the given dataset and adds its data to the array
    func addDataPointFor(data: [Entity], atIndex: Int) {
        
        if let xUnit = graphValidation.xAxis?.unit, let yUnit = graphValidation.yAxis?.unit {
            
            //check that data has been provided for each axis
            var xVal = 0.0, yVal = 0.0
            var xExists = false, yExists = false
            for entity in data {
                if entity.unit == xUnit {
                    xExists = true
                    xVal = entity.convertValue(toUnit: xUnit)
                }
                if entity.unit == yUnit {
                    yExists = true
                    yVal = entity.convertValue(toUnit: yUnit)
                }
            }
            
            //find the position of the datapoint as a fraction of the x and y scale
            if xExists && yExists {
                self.addButton(atIndex, xVal: xVal, yVal: yVal)
            }
        }
    }
    
    func addButton(atIndex: Int, xVal: Double, yVal: Double) {
        
        if !(graphValidation?.success ?? false) {
            return
        }
        
        let x = graphValidation.xAxis!
        let y = graphValidation.yAxis!
        
        let xRange = DAFRange(min: x.min.doubleValue, max: x.max.doubleValue, logarithmic: x.logarithmic.boolValue)
        let yRange = DAFRange(min: y.min.doubleValue, max: y.max.doubleValue, logarithmic: y.logarithmic.boolValue)
        
        let standardRange = DAFRange(min: 0, max: 1, logarithmic: false)
        let xPos = standardRange.interpolate(fromRange: xRange, withValue: xVal, limitToRange: true) ?? 0
        let yPos = standardRange.interpolate(fromRange: yRange, withValue: yVal, limitToRange: true) ?? 0
        
        //add button to view
        let buttonFrame = CGRect(x: 0, y: 0, width: 35, height: 35)
        let button = GraphButton(entities: dataArray[atIndex], index: atIndex, delegate: self)
        imageView.addSubview(button)
        
        button.index = atIndex
        
        //constrain the button to the given position on the graph
        button.translatesAutoresizingMaskIntoConstraints = false
        imageView.addConstraint(NSLayoutConstraint(item: button, attribute: .CenterX, relatedBy: .Equal, toItem: imageView!, attribute: .Trailing, multiplier: CGFloat(xPos), constant: 0))
        imageView.addConstraint(NSLayoutConstraint(item: button, attribute: .CenterY, relatedBy: .Equal, toItem: imageView!, attribute: .Bottom, multiplier: CGFloat(1-yPos), constant: 0))
        
        button.constrainToSize(buttonFrame.size)
        /*
        imageView.updateConstraintsIfNeeded()
        imageView.layoutIfNeeded()
        button.layoutIfNeeded()
        button.updateConstraints()*/
    }
    
    func setDataPoints() {
        if self.imageView != nil {
            // remove existing buttons
            if let views = self.imageView?.subviews {
                for view in views {
                    view.removeFromSuperview()
                }
            }
            
            // add new buttons
            var i = 0
            for data in dataArray {
                //if data != [] {
                    self.addDataPointFor(data, atIndex: i)
                    i++
                //}
            }
        }
    }

    // ================ GRAPH BUTTON DELEGATE =================
    
    func graphButton(shouldDeleteDataAtIndex index: Int) {
        self.dataArray[index] = []
    }
}
