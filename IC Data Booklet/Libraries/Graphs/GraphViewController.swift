//
//  SteamGraphItemController.swift
//  Temp
//
//  Created by Mike on 02/01/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit
import CorePlot

enum TickType {
    case Major
    case Minor
}

class GraphViewController: UIViewController, UIGestureRecognizerDelegate, CPTPlotDataSource, CPTPlotSpaceDelegate, DAFViewDelegate, GraphButtonDelegate {
    
    // ================ INITIALIZERS =================
    
    init(graph: Graph) {
        super.init(nibName: nil, bundle: nil)
        self.graph = graph
        self.graphValidation = graph.validateGraph()
        
        // initially set self as the delegate (GraphButtonDelegate) to handle button presses unless a GraphPageViewController or other controller takes over
        self.delegate = self
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // ================ VARIABLES =================

    let minScaleFactor = IS_IPAD ? 0.5 : 0.333
    
    var graph: Graph! {
        didSet {
            graphValidation = graph.validateGraph()
        }
    }
    var graphValidation: GraphValidation!
    
    var hostView = CPTGraphHostingView()
    
    var imageViewContainer: UIView!
    var imageView: UIImageView!
    var imageViewConstraints: [NSLayoutConstraint] = []
    
    var defaultPlotSpace: CPTXYPlotSpace!
    var primaryPlotSpace: CPTXYPlotSpace!
    var secondaryPlotSpace: CPTXYPlotSpace?
    
    var graphPinchGesture: UIPinchGestureRecognizer!
    var graphTwoFingerDragGesture: UIPanGestureRecognizer!
    
    var index: Int = 0
    var dataArray: [[Entity]] = [] {
        didSet {
            if self.imageView != nil {
                setDataPoints()
            }
        }
    }
    
    var delegate: GraphButtonDelegate?
   
    // ================ VIEW CONTROLLER =================
    
    override func loadView() {
        // override view with custom view with delegates to enable hitTest overrides for graph buttons
        let frame = UIScreen.mainScreen().applicationFrame
        let dafView = DAFView(frame: frame)
        dafView.delegate = self
        self.view = dafView
        self.view.autoresizingMask = [.FlexibleHeight, .FlexibleWidth]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if graph != nil {
            //create graph axes
            self.configureHost()
            self.configureGraph()
            self.configureAxes()
            
            // add a graph imageView at the bottom of the z tree
            self.configureGraphImage()
            
            // add buttons for existing saved data points
            setDataPoints()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        // recalculate title line breaks to fit screen width and reposition the graph and image accordingly 
        // CorePlot does not currently handle line breaks in the title automatically
        formatGraphTitle(forWidth: size.width)
        setImageViewConstraints()
    }
    
    
    /*override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // refresh imageView transform as the image does not rotate properly when zoomed
        let fx = defaultPlotSpace.xRange.dafRange(false)
        let fy = defaultPlotSpace.yRange.dafRange(false)
        setImageViewTransform(forFractionalRange: fx, forCoordinate: .X)
        setImageViewTransform(forFractionalRange: fy, forCoordinate: .Y)
    }*/
    
    // ================ DAF VIEW DELEGATE =================
    
    func dafView(view: DAFView, overrideHitTestForPoint point: CGPoint, withEvent event: UIEvent) -> UIView? {

        // check if the hit corresponds to a button
        for subview in imageView.subviews {
            if let button = subview as? GraphButton {
                let p = button.convertPoint(point, fromView: view)
                if button.pointInside(p, withEvent: event) {
                    return button
                }
            }
        }
        // otherwise return nil for default
        return nil
    }
    
    // ================ CPT GRAPH VIEW DATA SOURCE =================
    
    func numberOfRecordsForPlot(plot: CPTPlot) -> UInt {
        return 4
    }
    
    func numberForPlot(plot: CPTPlot, field fieldEnum: UInt, recordIndex idx: UInt) -> AnyObject? {
        return idx + 1
    }
    
    // ================ CPT PLOT SPACE DELEGATE =================
    
    func plotSpace(space: CPTPlotSpace, willChangePlotRangeTo newRange: CPTPlotRange, forCoordinate coordinate: CPTCoordinate) -> CPTPlotRange? {
        
        // ignore any calls not from the defaultPlotSpace (controller)
        if space != defaultPlotSpace {
            return newRange
        }
        
        // get fractional range from default plot space and correct for max magnification and bounds
        let fractionalRange = newRange.dafRange(false) //.applyMinimumLength(minScaleFactor).shiftedToBounds(0, max: 1)
        
        // apply image view transform
        setImageViewTransform(forFractionalRange: fractionalRange, forCoordinate: coordinate)
        
        // shift primary plot space coordinate appropriately
        switch coordinate {
        case .X:
            if let axis = graphValidation.xAxis {
                primaryPlotSpace.xRange = axis.rangeFromFractionalRange(fractionalRange)
            }
        case .Y:
            if let axis = graphValidation.yAxis {
                primaryPlotSpace.yRange = axis.rangeFromFractionalRange(fractionalRange)
            }
        default:
            // graph not configred properly so return default
            return newRange
        }
        
        // shift the secondary y axis for Y coordinates if one is used
        if secondaryPlotSpace != nil && coordinate == .Y, let y2 = graphValidation.secondaryYAxis {
            secondaryPlotSpace!.yRange = y2.rangeFromFractionalRange(fractionalRange)
        }
        
        return fractionalRange.CPTRange
    }
    
    func setImageViewTransform(forFractionalRange range: DAFRange, forCoordinate coordinate: CPTCoordinate) {
        if imageView == nil { return }
        let scale = CGFloat(1/range.length)
        let offset = CGFloat(range.min)
        let factor = (scale-1)/2
        
        let w = imageViewContainer.frame.width
        let h = imageViewContainer.frame.height
        
        // calculate scale and translate components in x and y directions
        var tx,ty,sx,sy: CGFloat!
        switch coordinate {
        case .X:
            tx = -w * (scale * offset - factor)
            ty = imageView.transform.ty
            sx = scale
            sy = imageView.transform.d
        case .Y:
            ty = h * (scale * offset - factor)
            tx = imageView.transform.tx
            sy = scale
            sx = imageView.transform.a
        default: return // incorrectly configured
        }
        
        // translate and scale component accordingly
        var transform = CGAffineTransformMakeTranslation(tx, ty)
        transform = CGAffineTransformScale(transform, sx, sy)
        
        imageView.transform = transform
        
        // maintain button size by applying an inverse scale transform
        for subview in imageView.subviews {
            if let button = subview as? GraphButton {
                let invScale = 1/scale
                button.transform = CGAffineTransformMakeScale(invScale, invScale)
            }
        }
    }
    
}
