//
//  GraphViewController+Configurations.swift
//  IC Data Booklet
//
//  Created by Mike on 21/09/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import CorePlot

extension GraphViewController {
    
    // ================ HOST VIEW =================
    
    func configureHost() {
        // add hostView as subView
        self.view.addSubview(hostView)
        hostView.userInteractionEnabled = true
        
        //constrain hostView to view frame
        hostView.translatesAutoresizingMaskIntoConstraints = false
        view.constrainSubview(hostView, toView: view, withInsets: nil)
        
        // add and configure gesture recognizers
        self.graphPinchGesture = UIPinchGestureRecognizer(target: self, action: "handlePinch:")
        graphPinchGesture.delegate = self
        hostView.addGestureRecognizer(graphPinchGesture)
        
        self.graphTwoFingerDragGesture = UIPanGestureRecognizer(target: self, action: "handleTwoFingerDrag:")
        graphTwoFingerDragGesture.minimumNumberOfTouches = 2
        graphTwoFingerDragGesture.delegate = self
        hostView.addGestureRecognizer(graphTwoFingerDragGesture)
        
        let doubleTapGesture = UITapGestureRecognizer(target: self, action: "handleDoubleTap:")
        doubleTapGesture.numberOfTapsRequired = 2
        hostView.addGestureRecognizer(doubleTapGesture)
    }
    
    // ================ GRAPH =================
    
    func configureGraph() {
        // 1 - Create graph
        let hostedGraph = CPTXYGraph(frame: self.hostView.bounds)
        //graph.applyTheme(CPTTheme(named: kCPTDarkGradientTheme))
        self.hostView.hostedGraph = hostedGraph
        
        // 2 - Create and set text style
        let titleStyle = CPTMutableTextStyle()
        titleStyle.color = CPTColor.blackColor()
        titleStyle.fontName = "Helvetica-Bold"
        titleStyle.fontSize = 16
        titleStyle.lineBreakMode = NSLineBreakMode.ByWordWrapping
        titleStyle.textAlignment = .Center
        hostedGraph.titleTextStyle = titleStyle
        hostedGraph.titlePlotAreaFrameAnchor = CPTRectAnchor.Top
        
        // 3 - Add title (formatted for word wrapping)
        let width = view.frame.width
        formatGraphTitle(forWidth: width)
        
        // 4 - Set padding for graph
        hostedGraph.paddingLeft = 10
        hostedGraph.paddingBottom = 10
        hostedGraph.paddingRight = 10
        
        // 5 - Set padding for plot area
        hostedGraph.plotAreaFrame!.paddingLeft = 40
        hostedGraph.plotAreaFrame!.paddingBottom = 40
        hostedGraph.plotAreaFrame!.paddingTop = 20
        hostedGraph.plotAreaFrame!.paddingRight = graphValidation.hasSecondaryAxis ? 40 : 10
        
        // 6 - Disable user interactions for plot space as CorePlot does not currently handle logarithmic axes properly
        hostedGraph.defaultPlotSpace!.allowsUserInteraction = false
        hostedGraph.defaultPlotSpace!.delegate = self
    }
    
    func formatGraphTitle(forWidth width: CGFloat) {
        if self.graph != nil && hostView.hostedGraph != nil {
            let style = hostView.hostedGraph!.titleTextStyle
            if let font = UIFont(name: style!.fontName!, size: style!.fontSize) {
                
                // get title formatted with new line characters for compatability with CorePlot
                let titleWithFormat = graph.name.formatLinesForWidth(width, font: font)
                hostView.hostedGraph!.title = titleWithFormat.string
                
                // layout graph and axes to compensate for any height change
                let textHeight = font.pointSize * CGFloat(titleWithFormat.lines)
                hostView.hostedGraph!.paddingTop = 10 + textHeight
                hostView.hostedGraph!.titleDisplacement = CGPointMake(0, textHeight)
            }
        }
    }
    
    // ================ AXES =================
    
    func configureAxes() {
        
        // 1 - Check validation success -> contains X and Y axes
        if graphValidation.success, let xAxis = graphValidation.xAxis, let yAxis = graphValidation.yAxis {
            
            let axisSet = self.hostView.hostedGraph!.axisSet as! CPTXYAxisSet
            defaultPlotSpace = self.hostView.hostedGraph!.defaultPlotSpace as? CPTXYPlotSpace
            
            // create x and y axes
            if axisSet.axes!.count >= 2 && defaultPlotSpace != nil {
                
                // create default axes with coordinates (0,1) to handle gesture recognizers are CorePlot does not currently handle logarithmic axes correctly
                let defaultX = axisSet.axes![0] as! CPTXYAxis
                let defaultY = axisSet.axes![1] as! CPTXYAxis
                configureAxis(defaultX, range: CPTPlotRange(location: 0, length: 1), inPosition: .X, withStyles: .None, inPlotSpace: defaultPlotSpace)
                configureAxis(defaultY, range: CPTPlotRange(location: 0, length: 1), inPosition: .Y, withStyles: .None, inPlotSpace: defaultPlotSpace)
                
                // create primary plot space for primary graph axes
                self.primaryPlotSpace = CPTXYPlotSpace()
                let x = CPTXYAxis(frame: hostView.frame)
                let y = CPTXYAxis(frame: hostView.frame)
                configureAxis(x, usingGraphAxis: xAxis, withStyles: .Default, inPlotSpace: primaryPlotSpace)
                configureAxis(y, usingGraphAxis: yAxis, withStyles: .Default, inPlotSpace: primaryPlotSpace)
                
                // add plotspace to graph
                hostView.hostedGraph!.addPlotSpace(primaryPlotSpace)
                var axes = [defaultX,defaultY,x,y]
                
                // configure secondary y-axis on the secondary plot space as necessary
                if let secondYAxis = graphValidation.secondaryYAxis {
                    
                    self.secondaryPlotSpace = CPTXYPlotSpace()
                    
                    let x2 = CPTXYAxis(frame: hostView.frame)
                    let y2 = CPTXYAxis(frame: hostView.frame)
                    configureAxis(x2, range: xAxis.CPTRange, inPosition: .X, withStyles: .None, inPlotSpace: secondaryPlotSpace!)
                    configureAxis(y2, usingGraphAxis: secondYAxis, withStyles: .Default, inPlotSpace: secondaryPlotSpace!)
                    
                    hostView.hostedGraph!.addPlotSpace(secondaryPlotSpace!)
                    axes += [x2,y2]
                }
                
                // configure axes
                axisSet.axes = axes
            }
        } else {
            NSLog("VALIDATION FAILED!")
        }
    }
    
    func configureAxis(axis: CPTXYAxis, usingGraphAxis graphAxis: GraphAxis, withStyles styles: AxisStyles, inPlotSpace plotSpace: CPTXYPlotSpace) {
        
        let position = graphAxis.location ?? .X
        configureAxis(axis, range: graphAxis.CPTRange, inPosition: position, withStyles: styles, inPlotSpace: plotSpace)
        
        // configure axis styles
        let titleFont = UIFont(name: styles.title.fontName!, size: styles.title.fontSize)
        axis.attributedTitle = graphAxis.titleForAxis(forFont: titleFont)
        
        // configure axis layout and constraints
        axis.orthogonalPosition = graphAxis.min
        
        // get axis range and grid increments
        let max = graphAxis.max.doubleValue
        let min = graphAxis.min.doubleValue
        let major = graphAxis.majorTick.doubleValue
        let minor = graphAxis.minorTick.doubleValue
        let log = graphAxis.logarithmic.boolValue
        
        switch position {
        case .X:
            plotSpace.xScaleType = log ? .Log : .Linear
        case .Y, .SecondaryY:
            plotSpace.yScaleType = log ? .Log : .Linear
        }
        
        // set coordinate
        axis.coordinate = graphAxis.coordinate
        
        // add min and max labels
        self.addPointToAxis(axis, forValue: min, tickType: .Major)
        self.addPointToAxis(axis, forValue: max, tickType: .Major)
        
        let iMin = log ? valueToLogScale(min) : min
        let iMax = log ? valueToLogScale(max) : max
        
        // add major ticks
        for var i = iMin.roundToMultiple(ofNumber: major, type: .Up); i < iMax; i += major {
            if log && i % 10 < 1 && major <= 1 { i++ }
            let j = log ? valueFromLogScale(i) : i
            
            self.addPointToAxis(axis, forValue: j, tickType: .Major)
        }
        
        // add minor ticks
        for var i = iMin.roundToMultiple(ofNumber: minor, type: .Up); i < iMax; i += minor {
            if log && i % 10 < 1 && minor <= 1 { i++ }
            let j = log ? valueFromLogScale(i) : i
            
            let mod = abs(i % major)
            let isMajor = mod <= major * 0.01 || mod >= major * 0.99
            
            if !isMajor {
                self.addPointToAxis(axis, forValue: j, tickType: .Minor)
            }
        }
    }
    
    func configureAxis(axis: CPTXYAxis, range:CPTPlotRange, inPosition position: AxisType, withStyles styles: AxisStyles, inPlotSpace plotSpace: CPTXYPlotSpace) {
        
        axis.titleTextStyle = styles.title
        axis.axisLineStyle = styles.line
        axis.labelingPolicy = CPTAxisLabelingPolicy.None
        axis.labelTextStyle = styles.text
        axis.majorTickLineStyle = styles.line
        axis.majorTickLength = 4
        axis.minorTickLength = IS_IPAD ? 4 : 2
        
        switch position {
        case .X:
            plotSpace.globalXRange = range
            plotSpace.xRange = range
        case .Y, .SecondaryY:
            plotSpace.globalYRange = range
            plotSpace.yRange = range
        }
        
        // set position-dependent variables
        switch position {
        case .X:
            axis.titleOffset = 22
            axis.labelOffset = 0
            axis.tickLabelDirection = .Negative
            axis.tickDirection = .Negative
            axis.axisConstraints = CPTConstraints(lowerOffset: 0)
        case .Y:
            axis.titleOffset = 26
            axis.labelOffset = 0
            axis.tickLabelDirection = .Negative
            axis.tickDirection = .Negative
            axis.axisConstraints = CPTConstraints(lowerOffset: 0)
        case .SecondaryY:
            axis.titleOffset = 26
            axis.labelOffset = 0
            axis.tickLabelDirection = .Positive
            axis.tickDirection = .Positive
            axis.axisConstraints = CPTConstraints(upperOffset: 0)
        }
        
        //set display range
        axis.plotSpace = plotSpace
    }
    
    func valueToLogScale(val: Double) -> Double {
        let exp = floor(log10(val))
        let ratio = val/pow(10,exp)
        
        return 10*exp + ratio
    }
    
    func valueFromLogScale(val: Double) -> Double {
        var exp = floor(val / 10)
        var ratio = val % 10
        
        if ratio <= 0 {
            if ratio == 0 {
                exp -= 1
            }
            ratio += 10
        } else if ratio < 1 {
            ratio = 1
        }
        let result = pow(10,exp) * ratio
        return result
    }
    
    func addPointToAxis(axis: CPTAxis, forValue value: Double, tickType: TickType) {
        
        switch tickType {
        case .Minor:
            axis.minorTickLocations!.insert(value)
        case .Major:
            axis.majorTickLocations!.insert(value)
            
            // initiate label and location
            let text = value.roundTo(significantFigures: 4).stringValue()
            let label = CPTAxisLabel(text: text, textStyle: axis.labelTextStyle)
            label.tickLocation = value
            
            // set offset from axis
            label.offset = axis.majorTickLength + axis.labelOffset
            
            // add to the labels set
            axis.axisLabels!.insert(label)
        }
    }
    
    // ================ IMAGE VIEW =================
    
    func configureGraphImage() {
        // create image view container view
        self.imageViewContainer = UIView()
        self.view.insertSubview(imageViewContainer, belowSubview: hostView)
        imageViewContainer.clipsToBounds = true
        
        // add constraints to container
        setImageViewConstraints()
        
        // create image view
        if let image = self.graph?.getGraphImage() {
            imageView = UIImageView(image: image)
        } else {
            NSLog("Could not load graph image")
            imageView = UIImageView()
        }
        self.imageViewContainer.insertSubview(imageView, atIndex: 0)
        imageView.userInteractionEnabled = true
        
        // add imageview constraints
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageViewContainer.constrainSubview(imageView, toView: imageViewContainer, withInsets: nil)
        
        
        self.view.gestureRecognizers = imageView?.gestureRecognizers
    }
    
    func setImageViewConstraints() {
        // remove any existing constraints
        if imageViewConstraints.count > 0 {
            view.removeConstraints(imageViewConstraints)
        }
        
        // calculate insets based on graph padding
        let host = hostView.hostedGraph!
        let insets = UIEdgeInsets(top: host.paddingTop + host.plotAreaFrame!.paddingTop,
            left: host.paddingLeft + host.plotAreaFrame!.paddingLeft,
            bottom: host.paddingBottom + host.plotAreaFrame!.paddingBottom,
            right: host.paddingRight + host.plotAreaFrame!.paddingRight)
        
        // set new constraints
        imageViewContainer.translatesAutoresizingMaskIntoConstraints = false
        imageViewConstraints = view.constrainSubview(imageViewContainer, toView: view, withInsets: insets)
    }
    
}