//
//  GraphValidation.swift
//  IC Data Booklet
//
//  Created by Mike on 27/07/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation

struct GraphValidation {
    var success: Bool
    var xAxis: GraphAxis?
    var yAxis: GraphAxis?
    var secondaryYAxis: GraphAxis?
}

extension GraphValidation {
    var hasSecondaryAxis: Bool {
        return secondaryYAxis != nil
    }
}