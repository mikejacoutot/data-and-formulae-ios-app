//
//  TypeAliases.swift
//  IC Data Booklet
//
//  Created by Mike on 27/09/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation

typealias AlertActionBlock = ((UIAlertAction!) -> Void)!

typealias SteamConnectionBlock = (success:Bool, steamData:[Entity]?, error: NSError?) -> ()
typealias ConnectionBlock = (success:Bool, responseObject:AnyObject?, error: NSError?) -> ()
typealias SolverConnectionBlock = (success:Bool, solutions:[SolverSolution]?, error: NSError?) -> ()
