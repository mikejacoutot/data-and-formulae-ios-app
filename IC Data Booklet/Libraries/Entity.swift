//
//  SteamData.swift
//  Temp
//
//  Created by Mike on 30/12/2014.
//  Copyright (c) 2014 Michael Jacoutot. All rights reserved.
//

import Foundation
import UIKit

class Entity: Equatable {
    
    // ================ INITIALIZERS =================
    
    init(unit: Unit, realValue: Double, imaginaryValue: Double) {
        self.unit = unit
        self.value = realValue
        self.imaginaryValue = imaginaryValue
    }
    
    convenience init(unit: Unit, value: Double) {
        self.init(unit: unit, realValue: value, imaginaryValue: 0)
        self.isImaginary = false
    }
    
    convenience init(unit: Unit) {
        self.init(unit: unit, realValue: 0, imaginaryValue: 0)
    }
    
    init(symbol: Symbol, realValue: Double, imaginaryValue: Double) {
        self.symbol = symbol
        if let symbolUnit = symbol.unit {
            self.unit = symbolUnit
        } else {
            self.unit = symbol.unitCategory.getBaseUnit()
        }
        self.value = realValue
        self.imaginaryValue = imaginaryValue
        self.isImaginary = !symbol.onlyReal.boolValue
    }
    
    convenience init(symbol: Symbol, value: Double) {
        self.init(symbol: symbol, realValue: value, imaginaryValue: 0)
    }
    
    convenience init(symbol: Symbol) {
        self.init(symbol: symbol, realValue: 0, imaginaryValue: 0)
    }
    
    init(symbol: Symbol, unit: Unit, realValue: Double, imaginaryValue: Double) {
        self.symbol = symbol
        self.unit = unit
        self.value = realValue
        self.imaginaryValue = imaginaryValue
        self.isImaginary = !symbol.onlyReal.boolValue
    } 
    
    convenience init(symbol: Symbol, unit: Unit, value: Double) {
        self.init(symbol: symbol, unit: unit, realValue: value, imaginaryValue: 0)
    }
    
    convenience init(symbol: Symbol, unit: Unit) {
        self.init(symbol: symbol, unit: unit, realValue: 0, imaginaryValue: 0)
    }
    
    // ================ CLASS FUNCTIONS =================
    
    class func initializeEntitiesFromSymbols(symbols: [Symbol]) -> [Entity] {
        
        var entities: [Entity] = []
        for symbol in symbols {
            entities += [Entity(symbol: symbol)]
        }
        return entities
    }
    
    // ================ VARIABLES =================
    
    var value: Double = 0 {
        didSet {
            if value != 0 {
                self.shouldDisplayValueAsNil = false
            }
        }
    }
    var imaginaryValue: Double = 0 {
        didSet {
            if imaginaryValue != 0 {
                self.shouldDisplayValueAsNil = false
            }
        }
    }
    var unit: Unit!
    var symbol: Symbol?
    var isImaginary = true
    var shouldDisplayValueAsNil = true
    var key = ""
    var shouldSolve = false
    
    // ================ METHODS =================
    
    func convertToUnit(unit: Unit) {
        if self.unit == nil {
            self.unit = unit
            self.value = unit.convertValue(valueFromBaseUnit: self.value)
        } else if unit.category == self.unit.category {
            self.value = self.unit.convertValue(self.value, toUnit: unit)
            self.unit = unit
        }
    }
    
    func convertValue(toUnit unit: Unit) -> Double {
        return self.unit.convertValue(value, toUnit: unit)
    }
    
    func sympyValue() -> String {
        if symbol?.onlyReal.boolValue ?? true {
            if unit != nil {
                return "\(unit.convertValue(valueToBaseUnit: value))"
            } else {
                return "\(value)"
            }
        } else {
            return "\(value)+\(imaginaryValue)*I"
        }
    }
    
    func getNameString(forFont font: UIFont!, short: Bool = false) -> NSMutableAttributedString {
        if self.symbol != nil {
            // configure variable label
            var display = symbol!.display.parseSuperAndSubscript(font)
            if symbol!.name != symbol!.display && !symbol!.name.isWhiteSpace() && !short {
                display += " - " + symbol!.name
            }
            return display
        } else if self.unit != nil {
            return NSMutableAttributedString(string: unit.category.name)
        } else {
            return NSMutableAttributedString()
        }
    }
}

// ================ EQUATABLE PROTOCOL IMPLEMENTATION =================

func == (lhs: Entity, rhs: Entity) -> Bool {
    let convertedLhsReal = lhs.unit.convertValue(lhs.value, toUnit: rhs.unit)
    let convertedLhsImag = lhs.unit.convertValue(lhs.imaginaryValue, toUnit: rhs.unit)
    
    return rhs.value == convertedLhsReal && rhs.imaginaryValue == convertedLhsImag && lhs.unit.category == rhs.unit.category
}
