//
//  ViewEdgePanGestureRecognizer.swift
//  IC Data Booklet
//
//  Created by Mike on 12/10/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit

enum ViewEdgePanFirstTouchArea: UInt8 {
    case None   = 0
    case Left   = 1
    case Right  = 2
    case Bottom = 4
    case Top    = 8
}


class ViewEdgePanGestureRecognizer: UIPanGestureRecognizer {
    
    var edges: UIRectEdge = .All {
        didSet {
            configureEdgeInsets(forEdges: edges, withInset: edgeInset)
        }
    }
    
    var edgeInset: CGFloat = 50 {
        didSet {
            configureEdgeInsets(forEdges: edges, withInset: edgeInset)
        }
    }
    
    
    var edgeInsets = UIEdgeInsetsZero
    var firstTouchLocation = CGPointZero
    var firstTouchArea: ViewEdgePanFirstTouchArea = .None
    
    func configureEdgeInsets(forEdges edges: UIRectEdge, withInset: CGFloat) {
        var insets = UIEdgeInsetsZero
        
        // currently cannot use or operators to test swift enums
        
        if edges.intersect(.Right) == .Right {
            insets.right = edgeInset
        }
        if edges.intersect(.Left) == .Left {
            insets.left = edgeInset
        }
        if edges.intersect(.Top) == .Top {
            insets.top = edgeInset
        }
        if edges.intersect(.Bottom) == .Bottom {
            insets.bottom = edgeInset
        }
        
        self.edgeInsets = insets
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent) {
        super.touchesBegan(touches, withEvent: event)
        
        if let view = self.view where touches.count == event.allTouches()?.count {
            
            let location = self.locationInView(view)
            configureEdgeInsets(forEdges: edges, withInset: edgeInset)
            
            let isTouchingEdge = CGRectContainsPoint(view.bounds, location) && !CGRectContainsPoint(UIEdgeInsetsInsetRect(view.bounds, self.edgeInsets), location)
            
            if isTouchingEdge {
                self.firstTouchLocation = location
                firstTouchArea = .None
            } else {
                self.state = .Failed
            }
        }
    }
    
    override func reset() {
        super.reset()
        firstTouchLocation = CGPointZero
        firstTouchArea = .None
    }
    
    override func canPreventGestureRecognizer(preventedGestureRecognizer: UIGestureRecognizer) -> Bool {
        return firstTouchLocation != CGPointZero
    }
    
    override func canBePreventedByGestureRecognizer(preventingGestureRecognizer: UIGestureRecognizer) -> Bool {
        return firstTouchLocation == CGPointZero
    }
}