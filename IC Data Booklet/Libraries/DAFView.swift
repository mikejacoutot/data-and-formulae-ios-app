//
//  GraphViewContainer.swift
//  Temp
//
//  Created by Mike on 25/03/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit
import CorePlot

@objc
protocol DAFViewDelegate {
    optional func dafView(view: DAFView, touchesBegan touches: Set<NSObject>, withEvent event: UIEvent)
    optional func dafView(view: DAFView, touchesMoved touches: Set<NSObject>, withEvent event: UIEvent)
    optional func dafView(view: DAFView, touchesEnded touches: Set<NSObject>, withEvent event: UIEvent)
    optional func dafView(view: DAFView, overrideHitTestForPoint point: CGPoint, withEvent event: UIEvent) -> UIView?
}

class DAFView: UIView {

    var delegate: DAFViewDelegate?
    var plotSpace: CPTXYPlotSpace?
    var imageView: UIImageView?
    
    override func hitTest(point: CGPoint, withEvent event: UIEvent!) -> UIView? {
        if let view = delegate?.dafView?(self, overrideHitTestForPoint: point, withEvent: event) {
            return view
        }
        
        return super.hitTest(point, withEvent: event)
    }
}
