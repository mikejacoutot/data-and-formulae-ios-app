//
//  String+Methods.swift
//  Temp
//
//  Created by Mike on 24/03/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import UIKit

// ================= NS MUTABLE ATTRIBURED STRING =================

extension NSMutableAttributedString {
    convenience init(string: String, font: UIFont) {
        self.init(string: string)
        self.addAttribute(NSFontAttributeName, value: font, range: NSMakeRange(0, NSString(string: string).length))
    }
    
    func isWhiteSpace() -> Bool {
        return self.string.isWhiteSpace()
    }
    
    func underline(style: NSUnderlineStyle) {
        self.addAttribute(NSUnderlineStyleAttributeName, value: style.rawValue, range: NSRange(location: 0, length: self.length))
    }
}

// ================= NS ATTRIBURED STRING =================

extension NSAttributedString {
    func mutableString() -> NSMutableAttributedString {
        return NSMutableAttributedString(attributedString: self)
    }
    
    func underlined(style: NSUnderlineStyle) -> NSAttributedString {
        let str = NSMutableAttributedString(attributedString: self)
        str.underline(style)
        return str
    }
}



// ================= STRING =================

private enum ScriptType: Int {
    case Normal,Super,Sub,Persist,End
}

private let scriptKeys: [String:ScriptType] = ["^":.Super,"_":.Sub,"{":.Persist,"}":.End]
private var persist = false

extension String {
    
    func dataValue() -> NSData! {
        return NSString(string: self).dataUsingEncoding(NSUTF8StringEncoding)
    }
    
    func expectedHeight(forWidth width: CGFloat, usingFont font: UIFont) -> CGFloat {
        let size = CGSize(width: width, height: CGFloat.max)
        return NSString(string: self).boundingRectWithSize(size, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName:font], context: NSStringDrawingContext()).height
    }
    
    func expectedWidth(usingFont font: UIFont) -> CGFloat {
        let size = CGSize(width: CGFloat.max, height: CGFloat.max)
        return NSString(string: self).boundingRectWithSize(size, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName:font], context: NSStringDrawingContext()).width
    }
    
    var words: [String] {
        return self.characters.split(allowEmptySlices: false, isSeparator: {$0 == " "}).map { String($0) }
    }
    
    // manual word wrapping function needed for CorePlot titles (dont currently have an option to word wrap)
    func formatLinesForWidth(width: CGFloat, font: UIFont) -> (string: String, lines: Int) {
        self.expectedWidth(usingFont: font)
        if width >= self.expectedWidth(usingFont: font) {
            return (self, 1)
        }
        
        var words = self.words
        var lines: [String] = []
        
        while words.count > 0 {
            var lineComplete = false
            
            var line = words.removeAtIndex(0)
            if words.count > 0 {
                while !lineComplete {
                    // check if adding the next word will push the string over the width limit
                    let newLine = line + " " + words[0]
                    if newLine.expectedWidth(usingFont: font) > width {
                        // true -> add line to array and trigger next line
                        lines += [line]
                        lineComplete = true
                    } else {
                        // false -> append word to line
                        line += " " + words.removeAtIndex(0)
                
                        // if the last word was just used, trigger an end
                        if words.count == 0 {
                            lines += [line]
                            lineComplete = true
                        }
                    }
                }
            } else {
                // if the line was created from the last word, add the line to the array (loop will end)
                lines += [line]
            }
        }
        // return lines separated by newline char. Coreplot will wrap titles for new lines
        let str = lines.joinWithSeparator("\n")
        return (str, lines.count)
    }
    
    func getAllOccurancesOfSubstring(substring: String) -> [NSRange] {
        let attrStr = NSMutableAttributedString(string: self)
        let inputLength = attrStr.string.characters.count
        let searchLength = substring.characters.count
        var range = NSRange(location: 0, length: attrStr.length)
        
        var ranges: [NSRange] = []
        while (range.location != NSNotFound) {
            range = (attrStr.string as NSString).rangeOfString(substring, options: [], range: range)
            if (range.location != NSNotFound) {
                ranges += [NSRange(location: range.location, length: searchLength)]
                range = NSRange(location: range.location + range.length, length: inputLength - (range.location + range.length))
            }
        }
        return ranges
    }
    
    func hasSuperscriptCharacters() -> Bool {
        let superscriptStarts = self.getAllOccurancesOfSubstring("^")
        for start in superscriptStarts {
            let nextIndex: Int = start.location + start.length
            let nextCharacter: String = self[nextIndex]
            
            if !nextCharacter.isWhiteSpace() {
                if nextCharacter == "{" {
                    // check the superscript range
                    let attrStr = NSMutableAttributedString(string: self)
                    let startIndex = nextIndex + 1
                    let remainingRange = NSRange(location: startIndex, length: self.characters.count - startIndex)
                    let endRange = (attrStr.string as NSString).rangeOfString("}", options: [], range: remainingRange)
                    
                    var searchRange: NSRange
                    if endRange.location != NSNotFound {
                        searchRange = NSRange(location: startIndex, length: endRange.location - 1)
                    } else {
                        searchRange = remainingRange
                    }
                    
                    let searchString: String = self[searchRange]
                    if !searchString.isWhiteSpace() {
                        return true
                    }
                } else {
                    // if it is a generic, nonblank character immediately proceeding the superscript indicator, return true
                    return true
                }
            }
        }
        return false
    }
    
    /*func multiplyExistingExponents(byExponent: Int) -> Bool {
        if byExponent == 0 || byExponent == 1 {
            return false
        }
        // set up variables in case of ranges
        var attrStr = NSMutableAttributedString(string: self)
        var inputLength = countElements(attrStr.string)
        var addedCharacters = 0
        
        // get all instances where a superscript indicator has occured
        let superscriptStarts = self.getAllOccurancesOfSubstring("^")
        
        for start in superscriptStarts {
            // get the character immediately after the indicator
            let nextIndex: Int = start.location + start.length
            let nextCharacter: String = self[nextIndex]
            
            // ignore it if it is blank
            if !nextCharacter.isWhiteSpace() {
                if nextCharacter == "{" {
                    // if it is the start of a superscript range, get the resulting string
                    let startIndex = nextIndex + 1
                    let remainingRange = NSRange(location: startIndex, length: inputLength - startIndex)
                    let endRange = (attrStr.string as NSString).rangeOfString("}", options: nil, range: remainingRange)
                    var searchRange: NSRange
                    if endRange.location != NSNotFound {
                        searchRange = NSRange(location: startIndex, length: endRange.location - 1)
                    } else {
                        searchRange = remainingRange
                    }
                    let searchString: String = self[searchRange]
                    if !searchString.isWhiteSpace() {
                        let currentValue = NSString(string: searchString).integerValue
                    }
                } else {
                    // if it is a generic, nonblank character immediately proceeding the superscript indicator, return true
                    return true
                }
            }
        }
        return false
    }*/
    
    func parseSuperAndSubscript(var font: UIFont! = nil) -> NSMutableAttributedString {
        if font == nil {
            font = UIFont.systemFontOfSize(UIFont.systemFontSize())
        }
        
        var previousType: ScriptType = .Normal
        var outputString = NSMutableAttributedString()
        
        for character in self.characters {
            let char = String(character)
            
            // if it is an identifier character, set the flag for the next character, otherwise append with the appropriate script
            if let scriptType = scriptKeys[char] {
                switch scriptType {
                case .Persist:
                    persist = true
                    break
                case .End:
                    previousType = .Normal
                    persist = false
                    break
                default:
                    previousType = scriptType
                    break
                }
            } else {
                switch previousType {
                case .Super:
                    outputString += char.getSuperscript(font)
                    break
                    
                case .Sub:
                    outputString += char.getSubscript(font)
                    break
                    
                default:
                    outputString += char
                    break
                }
                if !persist {
                    previousType = .Normal
                }
            }
        }
        return outputString
    }

    func isWhiteSpace() -> Bool {
        if self.isEmpty { return true}
        let whitespace = NSCharacterSet.whitespaceAndNewlineCharacterSet()
        return self.stringByTrimmingCharactersInSet(whitespace).isEmpty
    }
    
    func getSuperscript(baseFont: UIFont? = nil) -> NSMutableAttributedString {
        return self.superscriptOrSubsript(baseFont, value: 1)
    }
    
    func getSubscript(baseFont: UIFont? = nil) -> NSMutableAttributedString {
        return self.superscriptOrSubsript(baseFont, value: -1)
    }
    
    private func superscriptOrSubsript(var baseFont: UIFont! = nil, value: Int) -> NSMutableAttributedString {
        if baseFont == nil {
            baseFont = UIFont(name: "Helvetica", size: 12)
        }
        // create a new font at 70% the original size
        let superFont = UIFont(name: baseFont.fontName, size: baseFont.pointSize*0.7)!
        // create an attributed string with the small font
        let superString = NSMutableAttributedString(string: self, font: superFont)
        // apply sub/superscript across full length
        superString.addAttribute(String(kCTSuperscriptAttributeName), value: value, range: NSMakeRange(0, superString.length))
        return superString
    }
    
    subscript (i: Int) -> Character {
        return self[self.startIndex.advancedBy(i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        return substringWithRange(Range(start: startIndex.advancedBy(r.startIndex), end: startIndex.advancedBy(r.endIndex)))
    }
    
    subscript (r: NSRange) -> String {
        let endIndex = r.location + r.length
        let range = r.location...endIndex
        return self[range]
    }
    
}



// ================= OPERATORS =================

func + (left: NSMutableAttributedString, right: NSMutableAttributedString) -> NSMutableAttributedString {
    let copyLeft = NSMutableAttributedString(attributedString: left)
    copyLeft.appendAttributedString(right)
    return copyLeft
}

func + (left: NSMutableAttributedString, right: NSAttributedString) -> NSMutableAttributedString {
    let copyLeft = NSMutableAttributedString(attributedString: left)
    copyLeft.appendAttributedString(right)
    return copyLeft
}

func + (left: NSMutableAttributedString, right: String) -> NSMutableAttributedString {
    let copyLeft = NSMutableAttributedString(attributedString: left)
    copyLeft.appendAttributedString(NSMutableAttributedString(string: right))
    return copyLeft
}

func += (inout left: NSMutableAttributedString, right: NSMutableAttributedString) {
    left = left + right
}

func += (inout left: NSMutableAttributedString, right: NSAttributedString) {
    left = left + right
}

func += (inout left: NSMutableAttributedString, right: String) {
    left = left + right
}


