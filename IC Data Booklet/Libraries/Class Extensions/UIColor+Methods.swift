//
//  UIColor+Methods.swift
//  Test
//
//  Created by Mike on 11/12/2014.
//  Copyright (c) 2014 Mike. All rights reserved.
//

import UIKit
import ObjectiveC


let backgroundColor = UIColor.freshDarkGray()
let foregroundColor = UIColor.concreteColor()
let detailColor = UIColor.freshGreen()


extension UIColor {
    
    // Resistor Colours
    class func violetColor() -> UIColor {
        return UIColor(red: 148/255, green: 0, blue: 211/255, alpha: 1.0)
    }
    class func goldColor() -> UIColor {
        return UIColor(red: 252/255, green: 194/255, blue: 0, alpha: 1.0)
    }
    class func silverColor() -> UIColor {
        return UIColor(red: 192/255, green: 192/255, blue: 192/255, alpha: 1.0)
    }
    class func beigeColor() -> UIColor {
        return UIColor(red: 220/255, green: 220/255, blue: 180/255, alpha: 1.0)
    }
    class func concreteColor() -> UIColor {
        return UIColor(red: 149/255, green: 165/255, blue: 166/255, alpha: 1.0)
    }
    class func freshGreen() -> UIColor {
        return UIColor(red: 0, green: 166/255, blue: 82/255, alpha: 1.0)
    }
    class func freshDarkGray() -> UIColor {
        return UIColor(red: 47/255, green: 38/255, blue: 55/255, alpha: 1.0)
    }
}