//
//  UIImage+Methods.swift
//  Temp
//
//  Created by Mike on 24/03/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    func colorImage(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, self.scale)
        let c = UIGraphicsGetCurrentContext()
        self.drawInRect(rect)
        CGContextSetFillColorWithColor(c, color.CGColor)
        CGContextSetBlendMode(c, CGBlendMode.SourceAtop)
        CGContextFillRect(c, rect)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result
    }
    
    func changeColor(fromColor: UIColor, toColor: UIColor) -> UIImage? {
        
        let originalImage = self.CGImage
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        CGImageGetWidth(originalImage)
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.PremultipliedFirst.rawValue)
        let bitmapContext = CGBitmapContextCreate(nil, CGImageGetWidth(originalImage), CGImageGetHeight(originalImage), 8, CGImageGetWidth(originalImage)*4, colorSpace, bitmapInfo.rawValue)
        
        let w = CGFloat(CGBitmapContextGetWidth(bitmapContext))
        let h = CGFloat(CGBitmapContextGetHeight(bitmapContext))
        let rect = CGRect(x: 0, y: 0, width: w, height: h)
        
        CGContextDrawImage(bitmapContext, rect, originalImage)
        let data = UnsafeMutablePointer<UInt8>(CGBitmapContextGetData(bitmapContext))
        let numComponents = 4
        let bytesInContext = Int(CGBitmapContextGetHeight(bitmapContext) * CGBitmapContextGetBytesPerRow(bitmapContext))
        
        var fromRed, fromBlue, fromGreen, fromAlpha: CGFloat!
        // get RGB values of fromColor
        let fromCountComponents = CGColorGetNumberOfComponents(fromColor.CGColor)
        if fromCountComponents == 4 {
            let fromComponents = CGColorGetComponents(fromColor.CGColor)
            fromRed = fromComponents[0]
            fromGreen = fromComponents[1]
            fromBlue = fromComponents[2]
            fromAlpha = fromComponents[3]
        } else if fromCountComponents == 2 {
            let fromComponents = CGColorGetComponents(fromColor.CGColor)
            fromRed = fromComponents[0]
            fromGreen = fromComponents[0]
            fromBlue = fromComponents[0]
            fromAlpha = fromComponents[1]
        }
        
        var toRed, toBlue, toGreen, toAlpha: CGFloat!
        // get RGB values of toColor
        let toCountComponents = CGColorGetNumberOfComponents(toColor.CGColor)
        if toCountComponents == 4 {
            let toComponents = CGColorGetComponents(toColor.CGColor)
            toRed = toComponents[0]
            toGreen = toComponents[1]
            toBlue = toComponents[2]
            toAlpha = toComponents[3]
        } else if toCountComponents == 2 {
            let toComponents = CGColorGetComponents(toColor.CGColor)
            toRed = toComponents[0]
            toGreen = toComponents[0]
            toBlue = toComponents[0]
            toAlpha = toComponents[1]
        }
        
        var redIn, greenIn,blueIn,alphaIn: CGFloat!
        // iterate through each pixel
       
        if toRed != nil && fromRed != nil {
            
            for (var i = 0; i < bytesInContext; i += numComponents) {
                // get pixel RGB
                redIn = CGFloat(data[i])/255
                greenIn = CGFloat(data[i+1])/255
                blueIn = CGFloat(data[i+2])/255
                alphaIn = CGFloat(data[i+3])/255
                
                // test against fromColor
                if redIn == fromRed && blueIn == fromBlue && greenIn == fromGreen && fromAlpha == alphaIn {
                    // if the image matches, change the pixel color to toColor
                    data[i] = UInt8(toRed*255)
                    data[i+1] = UInt8(toGreen*255)
                    data[i+2] = UInt8(toBlue*255)
                    data[i+3] = UInt8(toAlpha*255)
                }
            }
            let scale = w / self.size.width
            if let img = CGBitmapContextCreateImage(bitmapContext) {
                return UIImage(CGImage: img, scale: scale, orientation: UIImageOrientation.Up)
            }
        }
        return nil
    }
}