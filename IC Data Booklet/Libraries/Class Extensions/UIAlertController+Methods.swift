//
//  UIAlertController+Methods.swift
//  IC Data Booklet
//
//  Created by Mike on 29/09/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation

extension UIAlertController {
    
    func show() {
        if var controller = UIApplication.sharedApplication().keyWindow?.rootViewController {
            while controller.presentedViewController != nil {
                controller = controller.presentedViewController!
            }
            controller.presentViewController(self, animated: true, completion: nil)
        }
    }
    
}