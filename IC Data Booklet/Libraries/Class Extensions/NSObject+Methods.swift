//
//  NSObject+Methods.swift
//  IC Data Booklet
//
//  Created by Mike on 17/10/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation

extension NSObject {
    
    class func swizzleMethod(forClass: AnyClass, originalSelector: Selector, replacementSelector: Selector) -> Bool {
        var originalMethod: Method?
        var swizzledMethod: Method?
        
        originalMethod = class_getInstanceMethod(forClass, originalSelector)
        swizzledMethod = class_getInstanceMethod(forClass, replacementSelector)
        
        if originalMethod != nil && swizzledMethod != nil {
            method_exchangeImplementations(originalMethod!, swizzledMethod!)
            return true
        }
        return false
    }
    
    class func swizzleStaticMethod(forClass: AnyClass, originalSelector: Selector, replacementSelector: Selector) -> Bool {
        var originalMethod: Method?
        var swizzledMethod: Method?
        
        originalMethod = class_getClassMethod(forClass, originalSelector)
        swizzledMethod = class_getClassMethod(forClass, replacementSelector)
        
        if originalMethod != nil && swizzledMethod != nil {
            method_exchangeImplementations(originalMethod!, swizzledMethod!)
            return true
        }
        return false
    }
    
}