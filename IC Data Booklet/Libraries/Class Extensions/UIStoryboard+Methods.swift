//
//  UIStoryboard+Methods.swift
//  IC Data Booklet
//
//  Created by Mike on 27/09/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    
    class var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
    
}
