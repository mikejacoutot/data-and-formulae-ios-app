//
//  UIViewController+Methods.swift
//  Temp
//
//  Created by Mike on 24/03/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import UIKit
import ObjectiveC

var TransitionManagerAssociationKey: UInt8 = 2

extension UIViewController {
    
    @IBAction func showMenu() {
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            self.view.endEditing(true)
            appDelegate.rootContainer.showNavigationMenu()
        }
    }
    
    var transitionManager: TransitionManager? {
        get {
            return objc_getAssociatedObject(self, &TransitionManagerAssociationKey) as? TransitionManager
        }
        set(newValue) {
            objc_setAssociatedObject(self, &TransitionManagerAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    func dismissViewControllerForTapOutside(sender: UITapGestureRecognizer) {
        if sender.state == UIGestureRecognizerState.Ended {
            let location = sender.locationInView(nil) //Passing nil gives us coordinates in the window
            
            //Then we convert the tap's location into the local view's coordinate system, and test to see if it's in or outside. If outside, dismiss the view.
            if sender.view != nil {
                if !self.view.pointInside(self.view.convertPoint(location, fromCoordinateSpace: sender.view!), withEvent: nil) {
                    sender.view!.removeGestureRecognizer(sender)
                    self.dismissViewControllerAnimated(true, completion: nil)
                }
            }
        }
    }
    
    func registerKeyboardNotifications()  {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWasShown:", name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillBeShown:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillBeHidden:", name: UIKeyboardWillHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWasHidden:", name: UIKeyboardDidHideNotification, object: nil)
    }
    
    func deregisterKeyboardNotifications () -> Void {
        let center:  NSNotificationCenter = NSNotificationCenter.defaultCenter()
        center.removeObserver(self, name: UIKeyboardDidHideNotification, object: nil)
        center.removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    
    func keyboardWasShown(notification: NSNotification) {
    }
    
    func keyboardWillBeShown(notification: NSNotification) {
    }
    
    func keyboardWillBeHidden(notification: NSNotification) {
    }
    
    func keyboardWasHidden(notification: NSNotification) {
    }
    
}