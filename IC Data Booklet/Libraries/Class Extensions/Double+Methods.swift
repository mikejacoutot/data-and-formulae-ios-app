//
//  Double+Methods.swift
//  Temp
//
//  Created by Mike on 14/01/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import UIKit

enum RoundingType {
    case Up
    case Down
    case Nearest
}



extension Double {
    
    func limitedToBounds(min: Double, max: Double) -> Double {
        if self < min {
            return min
        } else if self > max {
            return max
        } else {
            return self
        }
    }
    
    // returns the value of the double as a string, ignoring the .0 postfix when the double represents an integer
    func stringValue() -> String {
        if abs(self % 1.0) <= 0.00000000001 && self < Double(Int.max) {
            return "\(Int(self))"
        }
        return "\(self)"
    }
    
    func attributedStringWithExponentBase10(significantFigures SF:Int, forFont font:UIFont = UIFont(name: "Helvetica", size: 12)!) -> NSMutableAttributedString {
        
        //return zero for a zero value or invalid SF value
        if self.isZero || SF <= 0 {
            return NSMutableAttributedString(string: "0", font: font)
        }
        
        // make a working copy of the double value and find the "integer" exponent
        var value = abs(self)
        let power = floor(log10(value))
        
        // return cases which do not require an exponent
        if power < 0 {
            // fractional cases
            if power < 0 && abs(power) < Double(SF) {
                // for low negative powers just round the number
                if power > -3 {
                    return NSMutableAttributedString(string: "\(self.roundTo(significantFigures: SF))", font: font)
                // if there are fewer than SF digits just return the number
                } else if value * pow(10, Double(SF)) % 1 == 0 {
                    return NSMutableAttributedString(string: "\(self)", font: font)
                }
            }
        // fewer than SF digits after the decimal point just round the number
        } else if power <= Double(SF - 1) {
            return NSMutableAttributedString(string: self.roundTo(significantFigures: SF).stringValue(), font: font)
        }
        
        // get multiplicand
        value /= pow(10,power)
        if self.isSignMinus {
            value *= -1
        }
        value = value.roundTo(significantFigures: SF)
        
        let returnString = NSMutableAttributedString(string: value.stringValue() + "×10", font: font)
        
        let superString = power.stringValue().getSuperscript(font) //superscript exponent
        
        
        returnString.appendAttributedString(superString)
        return returnString
    }
    
    func roundTo(significantFigures SF:Int) -> Double {
        //neglect 0 cases as power -> -inf
        if self.isZero || SF <= 0  { return self }
        
        var number = abs(self) // take absolute val
        
        let power = floor(log10(number)) //find "integer" power of number
        let scaleFactor = pow(10,Double(SF - 1) - power) // find resultant value of 10^x needed to round
        number = round(number*scaleFactor)/scaleFactor // round number
        
        //reapply negative sign if needed
        if self.isSignMinus { number *= -1 }
        return number
    }
    
    
    
    func roundToMultiple(ofNumber number: Double, type : RoundingType ) -> Double {
        if number == 0 || self == 0 { return 0 }
        
        var val = self / number
        
        switch type {
        case .Up: val = ceil(val)
        case .Down: val = floor(val)
        case .Nearest: val = round(val)
        }
        val = val * number
        return val
    }
    
}