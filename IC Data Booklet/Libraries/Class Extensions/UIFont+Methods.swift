//
//  UIFont+Methods.swift
//  Temp
//
//  Created by Mike on 24/03/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    class func systemFont() -> UIFont {
        return UIFont.systemFontOfSize(UIFont.systemFontSize())
    }
}