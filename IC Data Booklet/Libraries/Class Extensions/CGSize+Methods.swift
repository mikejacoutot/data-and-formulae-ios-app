//
//  CGSize+Methods.swift
//  Temp
//
//  Created by Mike on 24/03/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import CoreGraphics

func + (left: CGRect, right: CGRect) -> CGRect {
    let w = left.width + right.width
    let h = left.height + right.height
    let x = left.origin.x + right.origin.x
    let y = left.origin.y + right.origin.y
    return CGRect(x: x, y: y, width: w, height: h)
}

func - (left: CGRect, right: CGRect) -> CGRect {
    let w = left.width - right.width
    let h = left.height - right.height
    let x = left.origin.x - right.origin.x
    let y = left.origin.y - right.origin.y
    return CGRect(x: x, y: y, width: w, height: h)
}

func + (left: CGSize, right: CGSize) -> CGSize {
    let w = left.width + right.width
    let h = left.height + right.height
    return CGSize(width: w, height: h)
}

func - (left: CGSize, right: CGSize) -> CGSize {
    let w = left.width - right.width
    let h = left.height - right.height
    return CGSize(width: w, height: h)
}

func * (left: CGSize, right: CGSize) -> CGSize {
    let w = left.width * right.width
    let h = left.height * right.height
    return CGSize(width: w, height: h)
}

func / (left: CGSize, right: CGSize) -> CGSize {
    let w = left.width / right.width
    let h = left.height / right.height
    return CGSize(width: w, height: h)
}

func * (left: CGSize, right: CGFloat) -> CGSize {
    let w = left.width * right
    let h = left.height * right
    return CGSize(width: w, height: h)
}

func / (left: CGSize, right: CGFloat) -> CGSize {
    let w = left.width / right
    let h = left.height / right
    return CGSize(width: w, height: h)
}

func * (left: CGSize, right: Double) -> CGSize {
    return left * CGFloat(right)
}

func / (left: CGSize, right: Double) -> CGSize {
    return left / CGFloat(right)
}

/*func + (left: CGRect, right: CGSize) -> CGRect {
let w = left.width + right.width
let h = left.height + right.height
let s = CGSize(width: w, height: h)
return CGRect(origin: left.origin, size: s)
}

func - (left: CGRect, right: CGSize) -> CGRect {
let w = left.width - right.width
let h = left.height - right.height
let s = CGSize(width: w, height: h)
return CGRect(origin: left.origin, size: s)
}*/