//
//  Set+Methods.swift
//  IC Data Booklet
//
//  Created by Mike on 24/06/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation

// Convenience append operator for set like those for array and dict

func +<T> (left: Set<T>, right: Set<T>) -> Set<T> {
    return left.union(right)
}

func +<T> (var left: Set<T>, right: T) -> Set<T> {
    left.insert(right)
    return left
}

func +=<T> (inout left: Set<T>, right: Set<T>) {
    left.unionInPlace(right)
}

func +=<T> (inout left: Set<T>, right: T) {
    left.insert(right)
}

func -<T> (left: Set<T>, right: Set<T>) -> Set<T> {
    return left.subtract(right)
}

func -<T> (var left: Set<T>, right: T) -> Set<T> {
    left.remove(right)
    return left
}

func -=<T> (inout left: Set<T>, right: Set<T>) {
    left.subtractInPlace(right)
}

func -=<T> (inout left: Set<T>, right: T){
    left.remove(right)
}