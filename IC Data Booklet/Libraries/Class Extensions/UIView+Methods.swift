//
//  UIView+Methods.swift
//  Temp
//
//  Created by Mike on 30/12/2014.
//  Copyright (c) 2014 Michael Jacoutot. All rights reserved.
//

import Foundation
import UIKit


extension UIView {
    
    func addGestureRecognizers(gestureRecognizers: [UIGestureRecognizer]) {
        for gestureRecognizer in gestureRecognizers {
            self.addGestureRecognizer(gestureRecognizer)
        }
    }
    
    func removeGestureRecognizers(gestureRecognizers: [UIGestureRecognizer]) {
        for gestureRecognizer in gestureRecognizers {
            self.removeGestureRecognizer(gestureRecognizer)
        }
    }
    
    func roundFullHeight() {
        self.roundToRadius(self.frame.size.height/2)
    }
    
    func roundToRadius(radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
    
    func constrainSubview(subview: UIView!, toView otherView: UIView!, withInsets insets: UIEdgeInsets?) -> [NSLayoutConstraint] {
        if subview == nil || otherView == nil {
            return []
        }
        
        // default zero insets if nil is provided
        let viewInsets = insets ?? UIEdgeInsetsZero
        
        var constraints: [NSLayoutConstraint] = []
        constraints += [NSLayoutConstraint(item: subview, attribute: .Leading, relatedBy: .Equal, toItem: otherView, attribute: .Leading, multiplier: 1, constant: viewInsets.left)]
        constraints += [NSLayoutConstraint(item: subview, attribute: .Trailing, relatedBy: .Equal, toItem: otherView, attribute: .Trailing, multiplier: 1, constant: -viewInsets.right)]
        constraints += [NSLayoutConstraint(item: subview, attribute: .Top, relatedBy: .Equal, toItem: otherView, attribute: .Top, multiplier: 1, constant: viewInsets.top)]
        constraints += [NSLayoutConstraint(item: subview, attribute: .Bottom, relatedBy: .Equal, toItem: otherView, attribute: .Bottom, multiplier: 1, constant: -viewInsets.bottom)]
        
        self.addConstraints(constraints)
        return constraints
    }
    
    func constrainToSize(size: CGSize) -> [NSLayoutConstraint] {
        var constraints: [NSLayoutConstraint] = []
        constraints += [NSLayoutConstraint(item: self, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: size.width)]
        constraints += [NSLayoutConstraint(item: self, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: size.height)]
        
        self.addConstraints(constraints)
        return constraints
    }
}