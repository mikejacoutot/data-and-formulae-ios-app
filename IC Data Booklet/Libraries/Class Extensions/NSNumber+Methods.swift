//
//  NSNumber+Methods.swift
//  Temp
//
//  Created by Mike on 24/03/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation

extension NSNumber: Comparable {
}

public func ==(lhs: NSNumber, rhs: NSNumber) -> Bool {
    return lhs.compare(rhs) == NSComparisonResult.OrderedSame
}

public func <(lhs: NSNumber, rhs: NSNumber) -> Bool {
    return lhs.compare(rhs) == NSComparisonResult.OrderedAscending
}

public func >(lhs: NSNumber, rhs: NSNumber) -> Bool {
    return lhs.compare(rhs) == NSComparisonResult.OrderedDescending
}

public func <=(lhs: NSNumber, rhs: NSNumber) -> Bool {
    return !(lhs > rhs)
}

public func >=(lhs: NSNumber, rhs: NSNumber) -> Bool {
    return !(lhs < rhs)
}


func + (left: NSNumber, right: NSNumber) -> NSNumber {
    return NSNumber(double: left.doubleValue + right.doubleValue)
}

func - (left: NSNumber, right: NSNumber) -> NSNumber {
    return NSNumber(double: left.doubleValue - right.doubleValue)
}

func * (left: NSNumber, right: NSNumber) -> NSNumber {
    return NSNumber(double: left.doubleValue * right.doubleValue)
}

func / (left: NSNumber, right: NSNumber) -> NSNumber {
    return NSNumber(double: left.doubleValue / right.doubleValue)
}


func += (inout left: NSNumber, right: NSNumber) {
    left = left + right
}

func -= (inout left: NSNumber, right: NSNumber) {
    left = left - right
}

func *= (inout left: NSNumber, right: NSNumber) {
    left = left * right
}

func /= (inout left: NSNumber, right: NSNumber) {
    left = left / right
}

func min(x: NSNumber, y: NSNumber) -> NSNumber {
    if x > y {
        return y
    } else {
        return x
    }
}

func max(x: NSNumber, y: NSNumber) -> NSNumber {
    if y < x {
        return y
    } else {
        return x
    }
}



