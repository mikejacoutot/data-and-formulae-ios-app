//
//  UIStoryboardSegue+Methods.swift
//  IC Data Booklet
//
//  Created by Mike on 09/10/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation

extension UIStoryboardSegue {
    
    // default push function to perform a basic segue if the custom segue cannot be performed
    func showDetailViewController() {
        sourceViewController.showDetailViewController(destinationViewController, sender: nil)
    }
    
    func showViewController() {
        sourceViewController.showViewController(destinationViewController, sender: nil)
    }
    
    func presentViewController(completion: (() -> Void)?) {
        sourceViewController.presentViewController(destinationViewController, animated: true, completion: completion)
    }
    
}