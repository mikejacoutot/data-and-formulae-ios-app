//
//  UINavigationBar+Methods.swift
//  IC Data Booklet
//
//  Created by Mike on 12/10/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

/*
A modification to layoutSubviews() to fix the title label jumping when the UINavigationController's frame is changed during an animation.

Code translated to Swift from original source: http://stackoverflow.com/questions/4758082/uinavigationcontroller-title-jumps-in-resize-animation

Method swizzling required to override an instance method within a class extension when using Swift, which would otherwise default the method in the class during startup (causing lots of crazy view-related issues). The replacement is performed within the AppDelegate by calling the patch function.
*/

// CURRENTLY NOT IMPLEMENTED AS OTHER, LESS INTRUSIVE SOLUTIONS HAVE BEEN FOUND

extension UINavigationBar {
    
    class func _patchLayoutIssues() {
        NSObject.swizzleMethod(UINavigationBar.self, originalSelector: "layoutSubviews", replacementSelector: "_layoutSubviews_titleFixed")
    }
    
    func _layoutSubviews_titleFixed() {
        
        // methods are exchanged so call THIS selector to execute the original method!!
        _layoutSubviews_titleFixed()

        for subview in self.subviews {
            if _stdlib_getDemangledTypeName(subview) == "UINavigationItemView" {
                (subview ).autoresizingMask = [.FlexibleLeftMargin, .FlexibleRightMargin]
            } else if subview.isKindOfClass(UIButton) {
                if subview.center.x < self.center.x/2 {
                    (subview as! UIButton).autoresizingMask = .FlexibleRightMargin
                } else {
                    (subview as! UIButton).autoresizingMask = .FlexibleLeftMargin
                }
            }
        }
    }
}

    /*override func layoutSubviews() {
        super.layoutSubviews()
        
        if IS_IPAD {
            var titleView: UIView!
            var buttonWidths: CGFloat = 0
            for subview in self.subviews as! [UIView] {
                if subview.isKindOfClass(NSClassFromString("UINavigationItemView")) {
                    // look for the title view, an instance of the private UINavigationItemView class
                    if titleView != nil {
                        // Exit here - if there is more than one title, probably doing a pop
                        return
                    }
                    titleView = subview
                } else if subview.isKindOfClass(UIView) || subview.isKindOfClass(NSClassFromString("UINavigationButton")) {
                    // look for a bar button with custom view (UIView) or an ordinary bar button (UINavigationButton)
                    buttonWidths += subview.frame.size.width
                    // is it RHS?
                    if subview.frame.origin.x > subview.frame.size.width {
                        subview.autoresizingMask = .FlexibleLeftMargin
                    }
                    else {
                        subview.autoresizingMask = .FlexibleRightMargin
                    }
                } else if subview.isKindOfClass(NSClassFromString("UINavigationItemButtonView")) {
                    // pretty sure this is always the back button
                    buttonWidths += subview.frame.size.width
                    subview.autoresizingMask = .FlexibleRightMargin
                }
            }
            
            if titleView != nil {
                buttonWidths += 20 // seems to be apple indentation
                
                var rect = titleView.frame
                if rect.size.width > self.frame.size.width - buttonWidths {
                    rect.size.width = self.frame.size.width - buttonWidths
                    titleView.frame = rect
                }
                
                titleView.center = self.center
            }
        }
    }*/


