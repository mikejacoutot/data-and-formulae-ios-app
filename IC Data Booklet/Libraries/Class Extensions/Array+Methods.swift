//
//  Array+Methods.swift
//  Temp
//
//  Created by Mike on 27/03/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation

struct OptionalIndex: IntegerLiteralConvertible {
    var i: Int
    
    init(integerLiteral value: IntegerLiteralType) {
        self.i = value
    }
    
    init(i: Int) {
        self.i = i
    }
}

extension Int {
    init(optionalIndex: OptionalIndex) {
        self = optionalIndex.i
    }
}

prefix operator ¿ {}

prefix func ¿ (index: Int) -> OptionalIndex {
    return OptionalIndex(integerLiteral: index)
}

extension Array {
    func get(optionalIndex: Int) -> Element? {
        if 0 <= optionalIndex && optionalIndex < count {
            return self[optionalIndex]
        } else {
            return nil
        }
    }
    
    subscript(index : OptionalIndex) -> Element? {
        return self.get(index.i)
    }
}

func + (left: OptionalIndex, right: Int) -> OptionalIndex {
    return OptionalIndex(i: left.i + right)
}
func - (left: OptionalIndex, right: Int) -> OptionalIndex {
    return OptionalIndex(i: left.i - right)
}
func * (left: OptionalIndex, right: Int) -> OptionalIndex {
    return OptionalIndex(i: left.i * right)
}
func / (left: OptionalIndex, right: Int) -> OptionalIndex {
    return OptionalIndex(i: left.i / right)
}

func + (left: OptionalIndex, right: OptionalIndex) -> OptionalIndex {
    return OptionalIndex(i: left.i + right.i)
}
func - (left: OptionalIndex, right: OptionalIndex) -> OptionalIndex {
    return OptionalIndex(i: left.i - right.i)
}
func * (left: OptionalIndex, right: OptionalIndex) -> OptionalIndex {
    return OptionalIndex(i: left.i * right.i)
}
func / (left: OptionalIndex, right: OptionalIndex) -> OptionalIndex {
    return OptionalIndex(i: left.i / right.i)
}

func += (inout left: OptionalIndex, right: Int) {
    left = left + right
}
func -= (inout left: OptionalIndex, right: Int) {
    left = left - right
}
func *= (inout left: OptionalIndex, right: Int) {
    left = left * right
}
func /= (inout left: OptionalIndex, right: Int) {
    left = left / right
}

func += (inout left: OptionalIndex, right: OptionalIndex) {
    left = left + right
}
func -= (inout left: OptionalIndex, right: OptionalIndex) {
    left = left - right
}
func *= (inout left: OptionalIndex, right: OptionalIndex) {
    left = left * right
}
func /= (inout left: OptionalIndex, right: OptionalIndex) {
    left = left / right
}

/*func + (left: Int, right: OptionalIndex) -> Int {
    return left + right.i
}
func - (left: Int, right: OptionalIndex) -> Int {
    return left - right.i
}
func * (left: Int, right: OptionalIndex) -> Int {
    return left * right.i
}
func / (left: Int, right: OptionalIndex) -> Int {
    return left / right.i
}

func += (inout left: Int, right: OptionalIndex) {
    left = left + right
}
func -= (inout left: Int, right: OptionalIndex) {
    left = left - right
}
func *= (inout left: Int, right: OptionalIndex) {
    left = left * right
}
func /= (inout left: Int, right: OptionalIndex) {
    left = left / right
}*/

