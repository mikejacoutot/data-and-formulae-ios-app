//
//  CGFloat+Methods.swift
//  IC Data Booklet
//
//  Created by Mike on 12/10/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation

extension CGFloat {
    func limitedToBounds(min: CGFloat, max: CGFloat) -> CGFloat {
        return CGFloat(Double(self).limitedToBounds(Double(min), max: Double(max)))
    }
}