//
//  NSFetchedResultsController+Methods.swift
//  IC Data Booklet
//
//  Created by Mike on 18/10/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import CoreData

extension NSFetchedResultsController {
    
    func setNewPredicate(predicate: NSPredicate) {
        // clear the cache if one is used as the results will otherwise not change
        if let cache = self.cacheName {
            NSFetchedResultsController.deleteCacheWithName(cache)
        }
        
        // set new predicate
        self.fetchRequest.predicate = predicate
        
        // re-execute the fetch request
        var error: NSError? = nil
        do {
            try self.performFetch()
        } catch let error1 as NSError {
            error = error1
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(error), \(error?.userInfo)")
            abort()
        }
    }
    
}