//
//  PopoverController.swift
//  IC Data Booklet
//
//  Created by Mike on 29/09/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import UIKit

class PopoverController: UIAlertController {
    
    // THIS METHOD USES A PRIVATE API TO SET THE CONTENT CONTROLLER OF THE UIALERTCONTROLLER. CAN LEAD TO REJECTION FROM THE APP STORE SO ATTEMPT TO FIND A BETTER SOLUTION. PERHAPS JUST ADDING IT AS A CHILD VIEW CONTROLLER.
    
    // ================= INITIALISERS ================
    
    class func controller(title: String?, message: String?, contentController: UIViewController, popoverAnchor: AnyObject?) -> UIAlertController {
        // Display as an ActionSheet (popover) on the iPad or Alert (modal pop up box) on the iPhone
        let preferredStyle: UIAlertControllerStyle = IS_IPAD ? .ActionSheet : .Alert
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        
        // set contentViewController
        alertController.setValue(contentController, forKey: "contentViewController")
        
        // set popover controller source anchor point if a popover presentation style is used and a source provided
        if let anchor: AnyObject = popoverAnchor, let popoverController = alertController.popoverPresentationController {
            if let barButtonItem = anchor as? UIBarButtonItem {
                popoverController.barButtonItem = barButtonItem
            } else if let sourceView = anchor as? UIView {
                popoverController.sourceView = sourceView
            } else if let sourceRect = anchor as? CGRect {
                popoverController.sourceRect = sourceRect
            } else {
                NSLog("Pop out controller popoverAnchor type is not supported! \(popoverAnchor)")
            }
        }
        
        return alertController
    }
    
    // ================= CLASS FUNCTIONS ================
    
    class func unitConverter(delegate: UnitConverterDelegate, dataSource: UnitConverterDataSource, popoverAnchor: AnyObject?, cancelHandler: AlertActionBlock, saveHandler: AlertActionBlock) -> UIAlertController {
        
        
        // load and configure the SteamTableViewController
        let unitConverter = UIStoryboard.mainStoryboard.instantiateViewControllerWithIdentifier("UnitConverterViewController") as! UnitConverterViewController
        unitConverter.delegate = delegate
        unitConverter.dataSource = dataSource
        unitConverter.hidesSwapButton = true
        
        let alertController = PopoverController.controller(nil, message: nil, contentController: unitConverter, popoverAnchor: popoverAnchor)
        
        // add cancel and delete actions and send through the handlers
        let saveAction = UIAlertAction(title: "Swap", style: .Default, handler: saveHandler)
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: cancelHandler)
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        return alertController
    }

    // class function to display the table as a pop up item in a UIAlertController
    class func dataTable(entities: [Entity], popoverAnchor: AnyObject?, cancelHandler: AlertActionBlock, deleteHandler: AlertActionBlock) -> UIAlertController {
        
        // load and configure the SteamTableViewController
        let dataTable = UIStoryboard.mainStoryboard.instantiateViewControllerWithIdentifier("SteamTableViewController") as! SteamTableViewController
        dataTable.entities = entities
        dataTable.canEditCells = false
        
        let alertController = PopoverController.controller(nil, message: nil, contentController: dataTable, popoverAnchor: popoverAnchor)
        
        // add cancel and delete actions and send through the handlers
        let deleteAction = UIAlertAction(title: "Delete", style: .Destructive, handler: deleteHandler)
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: cancelHandler)
        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)
        
        return alertController
    }
    
}
