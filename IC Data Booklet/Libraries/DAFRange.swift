//
//  DAFRange.swift
//  IC Data Booklet
//
//  Created by Mike on 27/06/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import CorePlot

struct DAFRange {
    var min: Double
    var max: Double
    var logarithmic: Bool
}

extension DAFRange {
    var CPTRange: CPTPlotRange {
        return CPTPlotRange(location: NSNumber(double: min), length: NSNumber(double: max-min))
    }
    
    var length: Double {
        get {
            return max-min
        }
        set {
            // set length to required value by moving it symetrically
            // TODO: CONFIGURE FOR LOGARITHMIC RANGE
            let outset = self.length - newValue
            self.min += outset/2
            self.max -= outset/2
        }
    }
    
    func shifted(value: CGFloat) -> DAFRange {
        return shifted(Double(value))
    }
    
    func shifted(value: Double) -> DAFRange {
        return DAFRange(min: self.min + value, max: self.max + value, logarithmic: self.logarithmic)
    }
    
    func applyMinimumLength(minLength: Double) -> DAFRange {
        // check that the magnification is less than or equal to the maximum
        if self.length < minLength {
            var r = DAFRange(min: min, max: max, logarithmic: logarithmic)
            r.length = minLength
            return r
            /*// limit range to required magnification
            let outset = self.length - minLength
            return DAFRange(min: self.min + outset/2, max: self.max - outset/2, logarithmic: self.logarithmic)*/
        }
        return self
    }
    
    func limitedToBounds(min: Double, max: Double) -> DAFRange {
        var r = self
        
        if r.max > max {
            r.max = max
        }
        if r.min < min {
            r.min = min
        }
        return r
    }
    
    func shiftedToBounds(min: Double, max: Double) -> DAFRange {
        var r = self
        // there should never be a case where max <= min. Incase something goes very wrong, set max = min + a very small amount to ensure the length is not 0, which may break the plotspace
        if r.max <= r.min {
            r.max = r.min * 1.0001
        }
        
        // limit to bounds by maintaining length if possible. Note: min() and max() functions are not currently working. Due to the added nsnumber functions?
        if r.max > max {
            r.min = r.min-(r.max-max)
            if r.min < min {r.min = min}
            r.max = max
        }
        if r.min < min {
            r.max = r.max+(min-r.min)
            if r.max > max { r.max = max }
            r.min = min
        }
        return r
    }
    
    func interpolate(fromRange range: DAFRange, withValue value: Double, limitToRange: Bool) -> Double? {
        
        // check logs are calculable if logarithmic ranges included
        if self.logarithmic && !(self.min > 0 && self.max > 0) {
            return nil
        }
        if range.logarithmic && !(value > 0 && range.min > 0 && range.max > 0) {
            return nil
        }
        
        // take logs if needed
        let fromValue = range.logarithmic ? log(value) : value
        let fromRange = range.logarithmic ? log(range) : range
        let toRange = self.logarithmic ? log(self) : self
        
        // calculate answer
        let ynum = fromValue - fromRange.min
        let yden = fromRange.max - fromRange.min
        let xden = toRange.max - toRange.min
        
        var ans = toRange.min + ynum*xden/yden
        
        if self.logarithmic {
            ans = exp(ans)
        }

        // limit to range if needed
        if limitToRange {
            if ans < self.min { ans = self.min }
            if ans > self.max { ans = self.max }
        }
        return ans
    }
}

func log(range: DAFRange) -> DAFRange {
    return DAFRange(min: log(range.min), max: log(range.max), logarithmic: false)
}

func exp(range: DAFRange) -> DAFRange {
    return DAFRange(min: exp(range.min), max: exp(range.max), logarithmic: true)
}

extension CPTPlotRange {
    func dafRange(logarithmic: Bool) -> DAFRange {
        return DAFRange(min: location.doubleValue, max: (location+length).doubleValue, logarithmic: logarithmic)
    }
}