//
//  Image+Methods.swift
//  Temp
//
//  Created by Mike on 06/03/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import UIKit

extension Image {
    
    func getImage() -> UIImage? {
        if let image = UIImage(data: self.image) {
            return image
        } else {
            NSLog("Image failed to be created! \(self)")
            // image could not be built from data so flag to be redownloaded
            self.lastModified = NSDate(timeIntervalSince1970: 0)
            return nil
        }
    }
}