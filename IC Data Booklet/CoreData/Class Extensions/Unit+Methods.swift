//
//  CoreDate+Methods.swift
//  Temp
//
//  Created by Mike on 05/01/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import CoreData
import UIKit

let polynomialNameDict: [Int:String] = [0:"", 1:"", 2:"square ", 3:"cubic ", 4:"quartic ", 5:"quintic "]


extension Unit {
    
    class func findIndex(units: [Unit], unit: Unit) -> Int? {
        // function which extends the default find function to search by pk so that different but equal instances are considered 
        // (using bounds-checked safe indexing prefix operator ¿ - see Array+Methods.swift)
        if let foundUnit = units.filter({ return $0.pk == unit.pk })[¿0] {
            return units.indexOf(foundUnit)
        }
        return nil
    }
    
    func convertValue(value: Double, toUnit: Unit) -> Double {
        let baseUnitValue = self.convertValue(valueToBaseUnit: value)
        let toUnitValue = toUnit.convertValue(valueFromBaseUnit: baseUnitValue)
        return toUnitValue
    }
    
    func convertValue(valueToBaseUnit value: Double) -> Double {
        return (value - self.addend.doubleValue) * self.multiplicand.doubleValue
    }
    
    func convertValue(valueFromBaseUnit value: Double) -> Double {
        return (value / self.multiplicand.doubleValue) + self.addend.doubleValue
    }
    
    func hasDisplay() -> Bool {
        return !self.display.isWhiteSpace()
    }
    
    func getDisplayString(forFont font: UIFont? = nil, appendFullName: Bool = false, plural: Bool = false) -> NSMutableAttributedString {
        if hasDisplay() {
            let displayString = NSMutableAttributedString(attributedString: self.display.parseSuperAndSubscript(font))
            if appendFullName {
                return NSMutableAttributedString(string: self.getName(plural)) + " [" + displayString + "]"
            } else {
                return displayString
            }
        } else {
            if font != nil {
                return NSMutableAttributedString(string: self.getName(true), font: font!)
            } else {
                return NSMutableAttributedString(string: self.getName(true))
            }
        }
    }
    
    func displayHasSuperscriptCharacters() -> Bool {
        return self.getBaseDisplayString(false).hasSuperscriptCharacters()
    }
    
    func getBaseDisplayString(plural: Bool) -> String {
        if hasDisplay() {
            return self.display
        } else {
            return self.getName(plural)
        }
    }
    
    func getName(plural: Bool) -> String {
        var name: String = ""
        if plural {
            // can use + character as first element as a shorthand to addend to the singular name
            if self.pluralName.isWhiteSpace() {
                name = self.name
            } else if self.pluralName[0] == "+" {
                name = self.name + self.pluralName[1..<self.pluralName.characters.count]
            } else {
                name = self.pluralName
            }
        } else {
            name = self.name
        }
        
        if !self.distinction.isWhiteSpace() {
            let distinctionString = " (" + self.distinction + ")"
            name += distinctionString
        }
        return name
    }
    
    
    func makeCompoundUnit(fromCompound compound: CompoundCategory, forCategory category: UnitCategory) -> NSError? {
        
        // group any repeated units
        var components: [CompoundCategoryComponent] = []
        for component in compound.components {
            if component.unit == nil {
                components += [component]
            } else {
                let foundComponents = components.filter({
                    if $0.unit == nil {
                        return false
                    } else {
                        return $0.category.pk == component.category.pk && $0.unit!.pk == component.unit!.pk
                    }
                })
                if let foundComponent = foundComponents[¿0] {
                    let i = components.indexOf(foundComponent)!
                    let exponent = foundComponent.exponent + component.exponent
                    components[i] = CompoundCategoryComponent(category: component.category, exponent: exponent, unit: component.unit)
                } else {
                    components += [component]
                }
            }
        }
        
        // initialise unit parameters
        var name = ""
        var plural = ""
        var display = ""
        var multiplicand: Double = 1
        let addend: Double = 0
        
        var hadPlural = false
        for i in 0..<components.count {
            if let unit = components[i].unit {
                let exponent =  components[i].exponent
                
                if exponent != 0 {
                    let isLast = i == components.count-1
                    
                    // pluralise last non-negative-exponent unit
                    let pluralName: String = {
                        if hadPlural { return unit.getName(false) }
                        
                        if !isLast {
                            let nextExp = components[i+1].exponent
                            if nextExp < 0 {
                                hadPlural = true
                                return unit.getName(true)
                            } else {
                                return unit.getName(false)
                            }
                        } else {
                            hadPlural = true
                            return unit.getName(true)
                        }
                        }()
                    
                    // add necesary extensions to plural and non-plural names
                    let poly = polynomialNameDict[abs(exponent)]!
                    let space = i == 0 ? "" : " "
                    let prefix = exponent < 0 ? "per " : ""
                    name += space + prefix + poly + unit.getName(false)
                    plural += space + prefix + poly + pluralName
                    
                    // format display string
                    display += unit.getBaseDisplayString(true)
                    if exponent == 1 {
                        if !isLast {
                            display += "·"
                        }
                    } else if exponent != 0 {
                        display += "^{\(exponent) }"
                    }
                    
                    // format conversion factors
                    multiplicand *= pow(unit.multiplicand as Double, Double(exponent))
                    // only care for absolute units so neglect addend
                }
            } else {
                //databaseManager.deleteObject(self)
                //return NSError(domain: DatabaseManagerErrorDomain, code: 0, userInfo: ["message":"no unit provided for compound at index: \(i)!"])
            }
        }
        self.name = name
        self.pluralName = plural
        self.distinction = ""
        self.display = display
        self.multiplicand = multiplicand
        self.addend = addend
        self.isBaseUnit = false
        self.category = category
        self.isCompound = true
        
        return nil
    }
}


// Equatable protocol implementation
func == (lhs: Unit, rhs: Unit) -> Bool {
    return lhs.pk == rhs.pk
}

