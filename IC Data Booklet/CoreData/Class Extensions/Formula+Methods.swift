//
//  Formula+Methods.swift
//  Temp
//
//  Created by Mike on 06/03/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import UIKit

extension Formula {
    
    func getImage() -> UIImage? {
        // try to get the main image
        for i in self.images {
            if let img = i as? Image {
                if img.main.boolValue {
                    return img.getImage()
                }
            }
        }
        // if no image is set to main, return the first available image
        for i in self.images {
            if let img = i as? Image {
                    return img.getImage()
            }
        }
        // else return nil
        return nil
    }
    
    func hasImage() -> Bool {
        for i in self.images.allObjects as! [Image] {
            if i.main.boolValue { return true }
        }
        return false
    }
}