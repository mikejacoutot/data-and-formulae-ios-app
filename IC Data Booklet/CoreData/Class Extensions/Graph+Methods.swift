//
//  Graph+Methods.swift
//  Temp
//
//  Created by Mike on 06/03/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import UIKit
import CorePlot

extension Graph {
    
    func validateGraph() -> GraphValidation {
        var validation = GraphValidation(success: false, xAxis: nil, yAxis: nil, secondaryYAxis: nil)
        
        // if it is a table, fail the validation
        if self.isTable.boolValue {
            return validation
        }
        
        // sort axes, discard any potential duplicate axes
        for axis in self.axes.allObjects as! [GraphAxis] {
            if let loc = axis.location {
                if axis.CPTRange.length > 0 {
                    switch loc {
                    case .X:
                        if validation.xAxis == nil {
                                validation.xAxis = axis
                        }
                    case .Y:
                        if validation.yAxis == nil {
                            validation.yAxis = axis
                        }
                    case .SecondaryY:
                        if validation.secondaryYAxis == nil {
                            validation.secondaryYAxis = axis
                        }
                    }
                }
            }
        }
        
        // check that there is both an x and a y axis
        if validation.xAxis != nil && validation.yAxis != nil {
            validation.success = true
        }
        
        return validation
    }
    
    var xAxis: GraphAxis? {
        return axisForCoordinate(AxisType.X)
    }
    
    var yAxis: GraphAxis? {
        return axisForCoordinate(AxisType.Y)
    }
    
    var secondaryYAxis: GraphAxis? {
        return axisForCoordinate(AxisType.SecondaryY)
    }
    
    func axisForCoordinate(coordinate: AxisType) -> GraphAxis? {
        for axis in self.axes.allObjects as! [GraphAxis] {
            if axis.location == coordinate {
                return axis
            }
        }
        return nil
    }
    
    func axisForCoordinate(coordinate: CPTCoordinate) -> GraphAxis? {
        if let type = AxisType.typeForCoordinate(coordinate) {
            return axisForCoordinate(type)
        }
        return nil
    }
    
    // Note to self: Do not name a function getXxxx() where Xxxx is a property of the class, as it interferes with the CoreData Obj-C getXxxx() selector, ie class.xxxx!! getImage() is not a suitable name for this function!!
    func getGraphImage() -> UIImage? {
        if let image = self.image.getImage() {
            return image
        }
        print("returning nil!")
        return nil
    }

}