//
//  GraphAxis+Methods.swift
//  IC Data Booklet
//
//  Created by Mike on 12/06/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import CorePlot

extension GraphAxis {
    
    var CPTRange: CPTPlotRange {
        return CPTPlotRange(location: self.min, length: self.max-self.min)
    }
    
    var dafRange: DAFRange {
        return DAFRange(min: self.min.doubleValue, max: self.max.doubleValue, logarithmic: self.logarithmic.boolValue)
    }
    
    var location: AxisType! {
        return AxisType(rawValue: self.type.integerValue)
    }
    
    var coordinate: CPTCoordinate {
        if location == AxisType.X {
            return .X
        } else { // Y, SecondaryY
            return .Y
        }
    }

    func titleForAxis(forFont font: UIFont?) -> NSMutableAttributedString {
        let hasTitle = !self.title.isEmpty && !(self.title == "???")
        let baseTitle = hasTitle ? self.title : self.unit.category.name
        var title = baseTitle.parseSuperAndSubscript(font)
        
        let hasUnit = !self.unit.getBaseDisplayString(false).isWhiteSpace()
        if hasUnit {
            title += " ("
            title += self.unit.getDisplayString(forFont: font, appendFullName: false, plural: false)
            title += ")"
        }
        return title
    }
    
    func validateRange(range: CPTPlotRange, maxMagnification: Double?) -> CPTPlotRange {
        
        var fractionalRange = self.fractionalRangeFromRange(range)

        // limit range to required magnification by shifting the max value as necessary
        if maxMagnification != nil {
            fractionalRange.applyMinimumLength(1/maxMagnification!)
        }
        
        // limit to bounds
        fractionalRange = fractionalRange.shiftedToBounds(0, max: 1)
        
        return self.rangeFromFractionalRange(fractionalRange)
    }

    // == functions to convert between a true range and a range expressed as a fraction of the boundaries of the axis ==
    func fractionalRangeFromRange(range: CPTPlotRange) -> DAFRange {
        
        let rangeMin = range.minLimit.doubleValue
        let rangeMax = range.maxLimit.doubleValue
        
        let bounds = self.dafRange
        let fractBounds = DAFRange(min: 0, max: 1, logarithmic: false)
        
        let fractMin = fractBounds.interpolate(fromRange: bounds, withValue: rangeMin, limitToRange: true) ?? 0
        let fractMax = fractBounds.interpolate(fromRange: bounds, withValue: rangeMax, limitToRange: true) ?? 1
        
        return DAFRange(min: fractMin, max: fractMax, logarithmic: false)
    }
    
    func rangeFromFractionalRange(fractionalRange: DAFRange) -> CPTPlotRange {
        
        let bounds = self.dafRange
        let fractBounds = DAFRange(min: 0, max: 1, logarithmic: false)
        
        let rangeMin = bounds.interpolate(fromRange: fractBounds, withValue: fractionalRange.min, limitToRange: true) ?? self.min.doubleValue
        var rangeMax = bounds.interpolate(fromRange: fractBounds, withValue: fractionalRange.max, limitToRange: true) ?? self.max.doubleValue
        
        // fix tiny rounding errors due to taking logs and exponents which can cause the maximum value to never be called
        if self.logarithmic.boolValue && rangeMax > 0.9999*self.max {
            rangeMax = self.max.doubleValue
        }
        
        return CPTPlotRange(location: NSNumber(double: rangeMin), length: NSNumber(double: rangeMax-rangeMin))
    }
}