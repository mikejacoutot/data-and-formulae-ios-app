//
//  UnitCategory+Methods.swift
//  Temp
//
//  Created by Mike on 06/03/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import UIKit

struct CompoundCategoryComponent: Equatable {
    var category: UnitCategory
    var exponent: Int
    var unit: Unit?
    
    func getTitle(font: UIFont?) -> NSMutableAttributedString {
        var string = NSMutableAttributedString(string: category.name)
        if exponent != 0 && exponent != 1 {
            let exp = "\(exponent)"
            string += exp.getSuperscript(font)
        }
        return string
    }
}

func == (lhs: CompoundCategoryComponent, rhs: CompoundCategoryComponent) -> Bool {
    if lhs.category.pk == rhs.category.pk && lhs.exponent == rhs.exponent {
        if lhs.unit == nil && rhs.unit == nil {
            return true
        } else if lhs.unit != nil && rhs.unit != nil {
            return lhs.unit!.pk == rhs.unit!.pk
        } else {
            return false
        }
    } else {
        return false
    }
}

struct CompoundCategory {
    var category: UnitCategory
    var components: [CompoundCategoryComponent]
    
    func getTitles(font: UIFont?) -> NSMutableAttributedString {
        var titles = NSMutableAttributedString()
        for component in components {
            if !titles.string.isEmpty {
                titles += " · "
            }
            titles += component.getTitle(font)
        }
        return titles
    }
}

extension UnitCategory {
    
    func getUnits(sortedBy descriptors: [NSSortDescriptor]?, withPredicate predicate: NSPredicate?) -> [Unit] {
        let set = predicate == nil ? units : units.filteredSetUsingPredicate(predicate!)
        if descriptors == nil {
            return set.allObjects as? [Unit] ?? []
        } else {
            return set.sortedArrayUsingDescriptors(descriptors!) as? [Unit] ?? []
        }
    }
    
    // default getUnits(), sorted by name
    func getUnits(compound: Bool) -> [Unit] {
        let nameSort = NSSortDescriptor(key: "name", ascending: true)
        let pred = NSPredicate(format: "isCompound = false", argumentArray: nil)
        return getUnits(sortedBy: [nameSort], withPredicate: compound ? nil : pred)
    }
    
    func getBaseUnit() -> Unit? {
        let us = units.allObjects as? [Unit] ?? []
        for unit in us {
            if unit.isBaseUnit.boolValue {
                return unit
            }
        }
        if us.count > 0 {
            us[0]
        }
        return nil
    }
    
    func getCompoundCategories() -> [CompoundCategory] {
        if self.hasCompound() {
            let compounds = self.compoundCategories as? [[[Int]]] ?? []
            
            var output: [CompoundCategory] = []
            for compound in compounds {
                var components: [CompoundCategoryComponent] = []
                for component in compound {
                    if component.count == 2 {
                        let category = databaseManager.getEntity(kDataUnitCategory, withID: component[0], createIfMissing: true) as! UnitCategory
                        components += [CompoundCategoryComponent(category: category, exponent: component[1], unit: category.getBaseUnit())]
                    } else {
                        // flag for redownload
                        self.lastModified = NSDate(timeIntervalSince1970: 0)
                    }
                }
                output += [CompoundCategory(category: self, components: components)]
            }
            return output
        }
        print("compound categories failed to load!")
        self.lastModified = NSDate(timeIntervalSince1970: 0)
        return []
    }
    
    func hasCompound() -> Bool {
        if let compounds = self.compoundCategories as? [[[Int]]] {
            return compounds.count > 0
        }
        return false
    }
}


func == (lhs: UnitCategory, rhs: UnitCategory) -> Bool {
    return lhs.pk == rhs.pk
}