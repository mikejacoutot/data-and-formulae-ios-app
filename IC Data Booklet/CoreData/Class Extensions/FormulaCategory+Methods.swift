//
//  FormulaCategory+Methods.swift
//  Temp
//
//  Created by Mike on 21/03/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import CoreGraphics
import UIKit

extension FormulaCategory {
    
    func getAliases() -> [String] {
        if let aliases = self.aliases as? [String] {
            return aliases
        } else {
            print("couldnt get aliases for \(self)!")
            // flag for redownload
            self.lastModified = NSDate(timeIntervalSince1970: 0)
            return []
        }
    }
    
    func nameForSize(size: CGSize, var font: UIFont? = nil) -> String {
        font = font ?? UIFont.systemFont()
        let names = [self.name] + getAliases()

        for name in names {
            let textSize = NSString(string: name).sizeWithAttributes([NSFontAttributeName:font!])
            if textSize.width <= size.width {
                return name
            }
        }

        return names[0]
    }
    
}