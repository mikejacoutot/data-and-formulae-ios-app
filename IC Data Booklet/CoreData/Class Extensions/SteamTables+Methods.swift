//
//  SteamTables+Methods.swift
//  Temp
//
//  Created by Mike on 06/03/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation

private var steamTablesSharedInstance: SteamTables!

extension SteamTables {
    
    class func sharedInstance() -> SteamTables {
        if steamTablesSharedInstance == nil {
            steamTablesSharedInstance = databaseManager.getSteamTablesData()
        }
        return steamTablesSharedInstance
    }
    
    func getCalculationsArray() -> [[String]] {
        let calcs = self.calculations as? [String] ?? []
        
        var calculations: [[String]] = []
        for calc in calcs {
            let vars = calc.componentsSeparatedByString(",")
            calculations += [vars]
        }
        return calculations
    }
    
    func getParametersDict() -> [String:Unit] {
        let params = self.parameters as? [String:NSNumber] ?? [:]
        
        var parameters: [String:Unit] = [:]
        for key in params.keys {
            let ID = params[key]!
            if let unit = databaseManager.getEntity(kDataUnit, withID: ID, createIfMissing: false) as? Unit {
                parameters[key] = unit
            }
        }
        return parameters
    }
}

