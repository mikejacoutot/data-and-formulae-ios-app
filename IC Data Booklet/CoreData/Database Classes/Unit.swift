//
//  Unit.swift
//  Temp
//
//  Created by Mike on 04/01/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import CoreData

@objc(Unit)
class Unit: Meta {

    @NSManaged var addend: NSNumber
    @NSManaged var isCompound: NSNumber
    @NSManaged var isBeingCreated: NSNumber
    @NSManaged var display: String
    @NSManaged var distinction: String
    @NSManaged var isBaseUnit: NSNumber
    @NSManaged var multiplicand: NSNumber
    @NSManaged var name: String
    @NSManaged var pluralName: String
    @NSManaged var category: UnitCategory
    @NSManaged var graph_xs: NSSet
    @NSManaged var graph_ys: NSSet
    @NSManaged var axes: NSSet
}
