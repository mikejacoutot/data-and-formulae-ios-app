//
//  SteamTables.swift
//  Temp
//
//  Created by Mike on 28/02/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import CoreData

@objc(SteamTables)
class SteamTables: Meta {

    @NSManaged var calculations: AnyObject
    @NSManaged var parameters: AnyObject
    @NSManaged var graphs: NSSet

}
