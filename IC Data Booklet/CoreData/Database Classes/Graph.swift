//
//  Graph.swift
//  Temp
//
//  Created by Mike on 28/02/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import CoreData

@objc(Graph)
class Graph: Meta {

    @NSManaged var name: String
    @NSManaged var isTable: NSNumber
    @NSManaged var formula: Formula?
    @NSManaged var image: Image
    @NSManaged var steamtable: SteamTables?
    @NSManaged var axes: NSSet

}
