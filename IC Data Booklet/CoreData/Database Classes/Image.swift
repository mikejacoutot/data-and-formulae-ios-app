//
//  Image.swift
//  Temp
//
//  Created by Mike on 04/01/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import CoreData

@objc(Image)
class Image: Meta {

    @NSManaged var image: NSData
    @NSManaged var main: NSNumber
    @NSManaged var formulae: Formula?
    @NSManaged var graph: Graph?
    @NSManaged var name: String

}
