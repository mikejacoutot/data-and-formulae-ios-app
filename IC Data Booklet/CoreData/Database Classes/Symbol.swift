//
//  Symbol.swift
//  Temp
//
//  Created by Mike on 04/01/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import CoreData

@objc(Symbol)
class Symbol: Meta {

    @NSManaged var constant: NSNumber
    @NSManaged var display: String
    @NSManaged var isConstant: NSNumber
    @NSManaged var name: String
    @NSManaged var onlyReal: NSNumber
    @NSManaged var sympy: String
    @NSManaged var equation: NSSet
    @NSManaged var nomenclature: NSSet
    @NSManaged var unitCategory: UnitCategory
    @NSManaged var unit: Unit?
    
}
