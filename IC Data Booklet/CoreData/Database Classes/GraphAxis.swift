//
//  GraphAxis.swift
//  
//
//  Created by Mike on 12/06/2015.
//
//

import Foundation
import CoreData

@objc(GraphAxis)
class GraphAxis: Meta {

    @NSManaged var title: String
    @NSManaged var min: NSNumber
    @NSManaged var max: NSNumber
    @NSManaged var majorTick: NSNumber
    @NSManaged var minorTick: NSNumber
    @NSManaged var logarithmic: NSNumber
    @NSManaged var type: NSNumber
    @NSManaged var unit: Unit
    @NSManaged var graph: Graph

}
