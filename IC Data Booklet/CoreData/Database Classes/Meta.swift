//
//  Meta.swift
//  Temp
//
//  Created by Mike on 04/01/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import CoreData

@objc(Meta)
class Meta: NSManagedObject {

    @NSManaged var lastModified: NSDate
    @NSManaged var pk: NSNumber

}
