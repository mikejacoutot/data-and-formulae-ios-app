//
//  Formula.swift
//  Temp
//
//  Created by Mike on 04/01/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import CoreData

@objc(Formula)
class Formula: Meta {

    @NSManaged var summary: String
    @NSManaged var hasMain: NSNumber
    @NSManaged var category: FormulaCategory
    @NSManaged var legal: String
    @NSManaged var name: String
    @NSManaged var images: NSSet
    @NSManaged var symbols: NSSet
    @NSManaged var graphs: NSSet
    @NSManaged var nomenclatureSymbols: NSSet
    @NSManaged var tags: String

}
