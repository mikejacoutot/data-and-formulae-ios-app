//
//  LogItem.swift
//  Temp
//
//  Created by Mike on 26/12/2014.
//  Copyright (c) 2014 Michael Jacoutot. All rights reserved.
//

import Foundation
import CoreData

class LogItem: NSManagedObject {

    @NSManaged var title: String
    @NSManaged var itemText: String

}
