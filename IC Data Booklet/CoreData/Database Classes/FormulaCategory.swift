//
//  Temp.swift
//  Temp
//
//  Created by Mike on 21/03/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import CoreData

@objc(FormulaCategory)
class FormulaCategory: Meta {

    @NSManaged var aliases: AnyObject
    @NSManaged var name: String
    @NSManaged var formulae: NSSet

}
