//
//  UnitCategory.swift
//  Temp
//
//  Created by Mike on 04/01/2015.
//  Copyright (c) 2015 Michael Jacoutot. All rights reserved.
//

import Foundation
import CoreData

@objc(UnitCategory)
class UnitCategory: Meta {

    @NSManaged var name: String
    @NSManaged var isDimensionless: NSNumber
    @NSManaged var hidden: NSNumber
    @NSManaged var compoundCategories: AnyObject?
    @NSManaged var symbols: NSSet
    @NSManaged var units: NSSet

}
